<?php $this->load->view('merchant_layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('merchant_layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('merchant_layout/header') ?>

        <main class="content">
        <div class="container-fluid p-0">
            <div class="row removable">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title mb-0"><?= $title ?></h5>
                        </div>
                        <div class="card-body">
                            <?= $this->session->flashdata('message'); ?>

                            <form id="" action="<?= base_url('dashboard/merchant_check_balance') ?>" method="get">
                                <div class="input-group w-100 d-flex justify-content center ">

                                    <div class="form-outline">
                                        <input type="text" autocomplete="off" name="keyword" value="<?= $this->input->get('keyword') ?>" class="form-control w-100" placeholder="Masukan NIS atau Nama">
                                    </div>
                                    <button class="btn btn-primary">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </form>

                            <?php if ($this->input->get('keyword')) :  ?>
                                <?php if (count($student) !== 0) : ?>
                                    <div class="card mt-5">
                                        <div class="card-body">
                                            <div class="container-fluid">
                                                <img src="<?= base_url('assets/img/uploads/' . $student['foto']) ?>" class="img-fluid img-thumbnail mx-auto d-block overflow-hidden" style="height:100px; width:100px" alt="">
                                                <div class="row mt-2 mb-2">
                                                    <div class="col-md-1"><strong>Nama</strong></div>
                                                    <div class="col"><?= $student['nama'] ?></div>
                                                </div>
                                                <div class="row mt-2 mb-2">
                                                    <div class="col-md-1"><strong>NIS</strong></div>
                                                    <div class="col"><?= $student['nis'] ?></div>
                                                </div>
                                                <div class="row mt-2 mb-2">
                                                    <div class="col-md-1"><strong>Saldo</strong></div>
                                                    <div class="col"><?= "Rp " . number_format($student['saldo'],0,',','.') ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php else : ?>
                                    <div class="alert alert-danger mt-5">
                                        Nama atau NIS tidak ditemukan.
                                    </div>
                                <?php endif ?>
                            <?php endif ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
        <?php $this->load->view('merchant_layout/footer') ?>
    </div>
</div>

<?php $this->load->view('merchant_layout/foot') ?>

