<?php $this->load->view('merchant_layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('merchant_layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('merchant_layout/header') ?>

        <main class="content">

            <div class="container-fluid p-0">
                <div class="row removable">
                    <div class="card">

                    </div>
                </div>
                <div class="row removable">
                    <div class="album bg-light">
                        <div class="container">
                            <?php if (count($products) == 0) : ?>
                                <div class="card">
                                    <div class="card-body">
                                        <b class="text-center">
                                            Produk masih kosong.
                                        </b>
                                    </div>
                                </div>
                            <?php else : ?>
                                <div class="row row-cols-1  row-cols-md-3 g-3">
                                    <?php foreach ($products as $product) : ?>
                                        <div class="col">
                                            <div class="card shadow-sm">
                                                <img src="<?= base_url('../assets/img/uploads/' . $product['gambar']) ?>" class="img-fluid img-thumbnail mx-auto d-block text-center overflow-hidden" style="height:200px;" alt="">
                                                <div class="card-body">
                                                    <h5 class="text-center"><?= $product['nama'] ?></h5>
                                                    <h6 class="card-text text-muted"><?= $product['kategori'] ?></h6>
                                                    <p class="card-text"><?= "Rp " . number_format($product['harga_jual'], 0, ',', '.') ?></p>
                                                    <div class="d-grid gap-2">
                                                        <div class="text-center">
                                                            <form action="<?= base_url('carts/store') ?>" method="post">
                                                                <input type="hidden" name="id_product" value="<?= $product['id_product'] ?>">
                                                                <input type="hidden" name="jumlah" value="1">
                                                                <button class="btn btn-dark text-center" type="submit"> <i class="fa fa-cart-plus"></i> Masukan keranjang</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach ?>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <?php $this->load->view('merchant_layout/footer') ?>
    </div>
</div>

<?php $this->load->view('merchant_layout/foot') ?>