<?php $this->load->view('merchant_layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('merchant_layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('merchant_layout/header') ?>

        <main class="content">
            <div class="container-fluid p-0">
                <div class="row removable">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title mb-0"><?= $title ?></h5>
                            </div>
                            <div class="card-body">
                                <?= $this->session->flashdata('message'); ?>

                                <?php echo form_open_multipart('products/merchant_update'); ?>
                                <div class="img-preview d-flex">
                                    <img src="<?= base_url('../assets/img/uploads/' . $product['gambar']) ?>" id="gmbr" class="img-fluid img-thumbnail mx-auto d-block text-center" style="height:200px" alt="">
                                </div>
                                <input type="hidden" class="form-control" name="id_product" value="<?= $product['id_product'] ?>">

                                <div class="form-group">
                                    <label for="gambar">Gambar</label>
                                    <input type="file" id="gambar" class="form-control" size="20" name="gambar" id="gambar" aria-describedby="gambar" placeholder="Masukan Foto Produk">
                                    <span class="text-danger">
                                        <?= form_error('gambar') ?>
                                    </span>
                                </div>

                                <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <input type="text" class="form-control" value="<?= $product['nama'] ?>" name="nama" id="nama" aria-describedby="nama" placeholder="Masukan Nama" required>
                                    <span class="text-danger">
                                        <?= form_error('nama') ?>
                                    </span>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label for="stok">Stok</label>
                                            <input type="number" class="form-control" value="<?= $product['stok'] ?>" name="stok" id="stok" aria-describedby="stok" placeholder="Masukan Nama" required>
                                            <span class="text-danger">
                                                <?= form_error('stok') ?>
                                            </span>
                                        </div>
                                        <div class="col">
                                            <label for="category">Kategori</label>
                                            <select name="category" class="form-control" id="category" required>
                                                <option selected disabled>-- Pilih Kategori --</option>
                                                <?php foreach ($categories as $category) : ?>
                                                    <option <?= ($product['id_category'] == $category['id_category'] ? "selected" : "") ?> value="<?= $category['id_category'] ?>"><?= $category['kategori'] ?></option>
                                                <?php endforeach ?>
                                            </select>
                                            <span class="text-danger">
                                                <?= form_error('kategori') ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label for="harga_modal">Harga Modal Satuan</label>
                                            <input type="number" value="<?= $product['harga_modal'] ?>" class="form-control" name="harga_modal" id="harga_modal" aria-describedby="harga_modal" placeholder="Harga Modal" required>

                                            <span class="text-danger">
                                                <?= form_error('harga_modal') ?>
                                            </span>
                                        </div>
                                        <div class="col">
                                            <label for="harga_jual">Harga Jual Satuan</label>
                                            <input type="number" name="harga_jual" value="<?= $product['harga_jual'] ?>" required class="form-control" placeholder="Harga Jual" aria-label="Harga Jual">
                                            <span class="text-danger">
                                                <?= form_error('harga_jual') ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                                <?= form_close() ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <?php $this->load->view('merchant_layout/footer') ?>
    </div>
</div>

<?php $this->load->view('merchant_layout/foot') ?>