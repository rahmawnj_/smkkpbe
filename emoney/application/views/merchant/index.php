<?php $this->load->view('merchant_layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('merchant_layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('merchant_layout/header') ?>

        <main class="content">
            <div class="container-fluid p-0">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="row removable">
                            <div class="col-sm-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col mt-0">
                                                <h5 class="card-title">Produk</h5>
                                            </div>

                                            <div class="col-auto">
                                                <div class="stat text-primary">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-truck align-middle">
                                                        <rect x="1" y="3" width="15" height="13"></rect>
                                                        <polygon points="16 8 20 8 23 11 23 16 16 16 16 8"></polygon>
                                                        <circle cx="5.5" cy="18.5" r="2.5"></circle>
                                                        <circle cx="18.5" cy="18.5" r="2.5"></circle>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                        <h1 class="mt-1 mb-3"><?= count($products) ?></h1>
                                        <!-- <div class="mb-0">
                                        <span class="text-danger"> <i class="mdi mdi-arrow-bottom-right"></i> -3.65% </span>
                                        <span class="text-muted">Since last week</span>
                                    </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col mt-0">
                                                <h5 class="card-title">Transaksi</h5>
                                            </div>

                                            <div class="col-auto">
                                                <div class="stat text-primary">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users align-middle">
                                                        <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                                        <circle cx="9" cy="7" r="4"></circle>
                                                        <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                                                        <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                        <h1 class="mt-1 mb-3"><?= count($trnsctn) ?></h1>
                                        <!-- <div class="mb-0">
                                        <span class="text-success"> <i class="mdi mdi-arrow-bottom-right"></i> 5.25% </span>
                                        <span class="text-muted">Since last week</span>
                                    </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col mt-0">
                                                <h5 class="card-title">Keuntungan</h5>
                                            </div>

                                            <div class="col-auto">
                                                <div class="stat text-primary">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign align-middle">
                                                        <line x1="12" y1="1" x2="12" y2="23"></line>
                                                        <path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                        <h1 class="mt-1 mb-3">
                                            <?=
                                            "Rp " . number_format($profit, 0, ',', '.');
                                            ?>
                                        </h1>
                                        <!-- <div class="mb-0">
                                        <span class="text-success"> <i class="mdi mdi-arrow-bottom-right"></i> 6.65% </span>
                                        <span class="text-muted">Since last week</span>
                                    </div> -->
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-12 col-md-6 col-xl d-none d-xxl-flex">
                        <div class="card flex-fill">
                            <div class="card-body py-4">
                                <div class="float-end text-danger">
                                    -1.26%
                                </div>
                                <h4 class="mb-2">XMR/BTC</h4>
                                <div class="mb-1">
                                    <strong>
                                        0.00700518
                                    </strong>
                                    $61.80
                                </div>
                                <div>
                                    Volume: 28,567 BTC
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-6 col-xxl-8 d-flex">
                        <div class="card flex-fill">
                            <div class="card-header">
                                <h5 class="card-title">Keuntungan</h5>
                                <h6 class="card-subtitle text-muted">7 hari terakhir.</h6>
                            </div>
                            <div class="card-body">
                                <div class="chart w-100">
                                    <div id="apexcharts-area"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 col-xxl-4 d-flex">
                        <div class="card flex-fill w-100">
                            <div class="card-header">
                                <div class="card-actions float-end">
                                    <div class="dropdown show">
                                        <a href="#" data-bs-toggle="dropdown" data-bs-display="static">
                                            <i class="align-middle" data-feather="more-horizontal"></i>
                                        </a>

                                        <div class="dropdown-menu dropdown-menu-end">
                                            <a class="dropdown-item" href="#">Action</a>
                                            <a class="dropdown-item" href="#">Another action</a>
                                            <a class="dropdown-item" href="#">Something else here</a>
                                        </div>
                                    </div>
                                </div>
                                <h5 class="card-title mb-0">Produk</h5>
                            </div>
                            <div class="card-body d-flex">
                                <div class="align-self-center w-100">
                                    <div class="py-3">
                                        <div class="chart chart-xs">
                                            <canvas id="product-category-merchant"></canvas>
                                        </div>
                                    </div>

                                    <table class="table mb-0">
                                        <tbody>
                                            <?php foreach ($categories as $category) : ?>
                                                <tr>
                                                    <td> <?= $category['category'] ?></td>
                                                    <td class="text-end"><?= $category['jmlh'] ?> Produk</td>
                                                </tr>

                                            <?php endforeach ?>

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </main>

        <?php $this->load->view('merchant_layout/footer') ?>
    </div>
</div>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        var categories = JSON.parse('<?= $ctgr ?>')
        var kategori = []
        var jumlah = []
        categories.forEach(e => {
            kategori.push(e.category)
            jumlah.push(e.jmlh)
        })
        new Chart(document.getElementById("product-category-merchant"), {
            type: "pie",
            data: {
                labels: kategori,
                datasets: [{
                    data: jumlah,
                    backgroundColor: [
                        window.theme.primary,
                        window.theme.warning,
                        window.theme.danger,
                        "#E8EAED"
                    ],
                    borderWidth: 5,
                    borderColor: window.theme.white
                }]
            },
            options: {
                responsive: !window.MSInputMethodContext,
                maintainAspectRatio: false,
                legend: {
                    display: false
                },
                cutoutPercentage: 70
            }
        });
    });
</script>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        var transactions = JSON.parse('<?= $trans ?>')
        var profits = []
        var dates = []
        transactions.forEach(e => {
            profits.push(e.profit)
            dates.push(e.date)
        });
        var options = {
            chart: {
                height: 350,
                type: "area",
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: "smooth"
            },
            series: [{
                name: "Keuntungan",
                data: profits
            }],
            xaxis: {
                type: "date",
                categories: dates,
            },
            tooltip: {
                x: {
                    format: "dd/MM/yy"
                },
            }
        }
        var chart = new ApexCharts(
            document.querySelector("#apexcharts-area"),
            options
        );
        chart.render();
    });
</script>
<?php $this->load->view('merchant_layout/foot') ?>