<?php $this->load->view('merchant_layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('merchant_layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('merchant_layout/header') ?>

        <main class="content">
            <div class="container-fluid p-0">
                <div class="row removable">
                    <div class="col-lg-12">
                        <div class="card flex-fill">
                            <div class="card-header d-flex justify-content-between">
                                <h5 class="card-title mb-0"><?= $title ?></h5>
                            </div>
                            <div class="card-body">
                                <div class="btn-group mb-3 btn-group-sm">
                                    <a href="<?= base_url('order_transactions/merchant_export_excel') ?>" class="btn btn-primary"> <i class="fa fa-export"></i> Export</a>
                                </div>
                                <div class="table-responsive">

                                    <table id="table" style="width: 100%;" class="table table-hover my-0 ">

                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nama</th>
                                                <th>Profit</th>
                                                <th>Status</th>
                                                <th>Metode Pembayaran</th>
                                                <th>Tanggal</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 0;
                                            foreach ($order_transactions as $ordertransaction) : ?>
                                                <tr>
                                                    <td><?= ++$no; ?></td>
                                                    <td><?= $ordertransaction['nama'] ?></td>
                                                    <td><?= $ordertransaction['profit'] ?></td>
                                                    <td><?= ($ordertransaction['status'] == 'sukses') ? '<span class="badge bg-success">' . $ordertransaction['status'] . '</span>' : '<span class="badge bg-danger">' . $ordertransaction['status'] . '</span>' ?></td>
                                                    <td><?= $ordertransaction['metode'] ?></td>

                                                    <td><?= $ordertransaction['created_at'] ?></td>
                                                    <td>
                                                        <a class="fas fa-eye btn bg-primary btn-sm text-white" href="<?= base_url('merchant/order_transactions/view/' . $ordertransaction['id_order_transaction']) ?>"></a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </main>

        <?php $this->load->view('merchant_layout/footer') ?>
    </div>
</div>

<?php $this->load->view('merchant_layout/foot') ?>