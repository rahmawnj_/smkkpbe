<?php $this->load->view('merchant_layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('merchant_layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('merchant_layout/header') ?>

        <main class="content">
            <div class="container-fluid p-0">
                <div class="row removable">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title mb-0"><?= $title ?></h5>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">

                                    <table class="table table-striped" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Produk</th>
                                                <th></th>
                                                <th>Harga</th>
                                                <th>Jumlah</th>
                                                <th>Total</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($carts as $cart) : ?>
                                                <tr id="cart-item-<?php echo $cart["id_cart"]; ?>">
                                                    <td>
                                                        <img src="<?= base_url('assets/img/uploads/' . $cart['gambar']) ?>" height="70" width="70" class="img-responsive" />

                                                    </td>
                                                    <td><?= $cart['nama'] ?></td>
                                                    <td> <?=
                                                            "Rp " . number_format($cart['harga_jual'], 0, ',', '.');
                                                            ?>
                                                    </td>
                                                    <td>
                                                        <input type="hidden" id="id-product-<?php echo $cart["id_cart"]; ?>" value="<?= $cart['id_product'] ?>">
                                                        <div class="input-group input-group-sm">
                                                            <button class="btn btn-dark" onClick="increment_quantity(<?php echo $cart["id_cart"]; ?>)">+</button>
                                                            <input disabled type="text" size="1" class="input-quantity" id="input-quantity-<?php echo $cart["id_cart"]; ?>" value="<?php echo $cart["jumlah"]; ?>">
                                                            <button class="btn btn-dark" onClick="decrement_quantity(<?php echo $cart["id_cart"]; ?>)">-</button>
                                                        </div>
                                                    </td>
                                                    <td id="total-<?= $cart['id_cart'] ?>"><?= "Rp " . number_format($cart['harga_jual'] * $cart['jumlah'], 0, ',', '.')  ?></td>
                                                    <td>
                                                        <!-- <a href="<?= base_url('carts/delete/' . $cart['id_cart']); ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a> -->
                                                        <span href="" id="remove-item" onClick="remove_item(<?php echo $cart["id_cart"]; ?>)" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></span>
                                                    </td>
                                                </tr>
                                            <?php endforeach ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>

                                                <td colspan="4" class="hidden-xs text-end">
                                                    Total :
                                                    <strong id="total_harga">
                                                        <?php
                                                        $ci = &get_instance();
                                                        $ci->load->model('cart_model');
                                                        $carts = $ci->cart_model->get_cart('carts.id_merchant', $this->session->userdata('id_merchant'));
                                                        $jumlah_harga = 0;
                                                        foreach ($carts as $cart) {
                                                            $jumlah_harga += $cart['harga_jual'] * $cart['jumlah'];
                                                        }
                                                        echo "Rp " . number_format($jumlah_harga, 0, ',', '.');
                                                        ?>
                                                    </strong>
                                                </td>
                                                <td class="hidden-xs text-end"><a href="<?= base_url('order_transactions/create') ?>" class="btn btn-success btn-block">Checkout <i class="fa fa-angle-right"></i></a></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <?php $this->load->view('merchant_layout/footer') ?>
    </div>
</div>
<script>
    function remove_item(id_cart) {
        $.get("<?= base_url('carts/delete_json/') ?>" + id_cart, function(data, status) {
            const cart_item = document.getElementById('cart-item-' + id_cart);
            cart_item.style.display = "none";
            const cart_item_nav = document.getElementById('cart-item-nav-' + id_cart);
            cart_item_nav.style.display = "none";
            $.get("<?= base_url('carts/view') ?>", function(data, status) {
                var data = JSON.parse(data)
                document.getElementById("total_harga").innerHTML = data.jumlah_harga
                document.getElementById("total_harga_nav").innerHTML = data.jumlah_harga
                document.getElementById("nav_total").innerHTML = data.jumlah_items
            })
        })

    }

    function increment_quantity(id_cart) {
        $.get("<?= base_url('data/stock/') ?>" + document.getElementById("id-product-" + id_cart).value, function(data, status) {
            var data = JSON.parse(data)
            if ((parseInt(document.getElementById("input-quantity-" + id_cart).value) + 1) <= data) {
                var jumlah = parseInt(document.getElementById("input-quantity-" + id_cart).value) + 1
                $.post("<?= base_url('carts/update') ?>", {
                    id_cart: id_cart,
                    jumlah: jumlah
                }, function(data, status) {
                    var data = JSON.parse(data)
                    document.getElementById("input-quantity-" + id_cart).value = data.jumlah
                    document.getElementById("total_harga").innerHTML = data.jumlah_harga
                    document.getElementById("total_harga_nav").innerHTML = data.jumlah_harga
                    document.getElementById("total-" + id_cart).innerHTML = data.total
                    document.getElementById("nav_total").innerHTML = data.total_item
                    document.getElementById("nav_total_pill").innerHTML = data.total_item
                    document.getElementById("total-items-" + id_cart).innerHTML = data.jumlah
                })
            }
        });


    }

    function decrement_quantity(id_cart) {
        if ((parseInt(document.getElementById("input-quantity-" + id_cart).value) - 1) > 0) {
            var jumlah = parseInt(document.getElementById("input-quantity-" + id_cart).value) - 1
            $.post("<?= base_url('carts/update') ?>", {
                id_cart: id_cart,
                jumlah: jumlah
            }, function(data, status) {
                var data = JSON.parse(data)
                document.getElementById("input-quantity-" + id_cart).value = data.jumlah
                document.getElementById("total_harga").innerHTML = data.jumlah_harga
                document.getElementById("total_harga_nav").innerHTML = data.jumlah_harga
                document.getElementById("total-" + id_cart).innerHTML = data.total
                document.getElementById("nav_total").innerHTML = data.total_item
                document.getElementById("nav_total_pill").innerHTML = data.total_item
                document.getElementById("total-items-" + id_cart).innerHTML = data.jumlah
            })
        }

    }
</script>
<?php $this->load->view('merchant_layout/foot') ?>