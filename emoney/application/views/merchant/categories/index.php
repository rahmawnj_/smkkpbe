<?php $this->load->view('merchant_layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('merchant_layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('merchant_layout/header') ?>

        <main class="content">
            <div class="container-fluid p-0">
                <div class="row removable">
                    <div class="col-lg-12">
                        <div class="card flex-fill">
                            <div class="card-header d-flex justify-content-between">
                                <h5 class="card-title mb-0"><?= $title ?></h5>
                            </div>
                            <div class="card-body">
                              
                                <div class="table-responsive">
                                    <table id="table" style="width: 100%;" class="table table-hover my-0 ">

                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Kategori</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 0;
                                            foreach ($categories as $category) : ?>
                                                <tr>
                                                    <td><?= ++$no; ?></td>
                                                    <td><?= $category['kategori'] ?></td>
                                                    <td>
                                                        <a class="fas fa-location-arrow btn-sm btn bg-primary text-white" href="<?= base_url('merchant/categories/products/' . $category['id_category']) ?>"></a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </main>

        <?php $this->load->view('merchant_layout/footer') ?>
    </div>
</div>

<?php $this->load->view('merchant_layout/foot') ?>