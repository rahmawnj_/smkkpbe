<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="SMK KP Baleendah - Emoney">
    <meta name="author" content="Emoney">
    <meta name="keywords" content="SMK KP Baleendah Emoney">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta http-equiv="Content-Security-Policy" content="block-all-mixed-content">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="shortcut icon" href="<?= base_url('assets/img/icons/emoney.png') ?>">
    <title><?= $title ?> | E-Money</title>
    <link href="<?= base_url('assets/css/css2.css?family=Inter:wght@300;400;600&display=swap') ?>" rel="stylesheet">
    <link class="js-stylesheet" href="<?= base_url('assets/css/light.css') ?>" rel="stylesheet">
</head>
<!--
  HOW TO USE: 
  data-theme: default (default), dark, light, colored
  data-layout: fluid (default), boxed
  data-sidebar-position: left (default), right
  data-sidebar-layout: default (default), compact
-->

<body data-theme="light" data-layout="fluid" data-sidebar-position="left" data-sidebar-layout="default">