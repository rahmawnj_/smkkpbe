<header class="navbar navbar-expand navbar-dark navbar-bg">
	<a class="sidebar-toggle js-sidebar-toggle">
		<i class="hamburger align-self-center"></i>
	</a>

	<div class="navbar-collapse collapse">
		<ul class="navbar-nav navbar-align">
			
			<li class="nav-item">
				<a class="nav-icon js-fullscreen d-none d-lg-block" href="#">
					<div class="position-relative">
						<i class="align-middle" data-feather="maximize"></i>
					</div>
				</a>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-icon pe-md-0 dropdown-toggle" href="#" data-bs-toggle="dropdown">
					<img src="<?= base_url('assets/img/uploads/' . $this->session->userdata('foto')) ?? '' ?>" class="avatar img-fluid rounded" alt="<?=  $this->session->userdata('username') ?>">
				</a>
				<div class="dropdown-menu dropdown-menu-end">
					<a class="dropdown-item" href="<?= base_url('auth/user_profile') ?>"><i class="align-middle me-1" data-feather="user"></i> Profile</a>
					<?php if ($this->session->userdata('role') == 'administrator') : ?>
					<a class="dropdown-item" href="<?= base_url('settings') ?>"><i class="align-middle me-1" data-feather="settings"></i> Settings</a>
					<?php endif?>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="<?= base_url('auth/logout') ?>"><i class="align-middle me-1" data-feather="log-out"></i> Log out</a>

				</div>
			</li>
		</ul>
	</div>
</header>