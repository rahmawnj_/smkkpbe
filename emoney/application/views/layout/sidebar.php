<aside id="sidebar" class="sidebar js-sidebar">
	<div class="sidebar-content js-simplebar">
		<a class="sidebar-brand" href="index.html">
			<span class="sidebar-brand-text align-middle">
				E - Money
			</span>
		</a>

		<div class="sidebar-user">
			<div class="d-flex justify-content-center">
				<div class="flex-shrink-0">
					<img src="<?= base_url('assets/img/uploads/' . $this->session->userdata('foto')) ?? '' ?>" class="avatar img-fluid rounded me-1" alt="<?= $this->session->userdata('name') ?>">
				</div>
				<div class="flex-grow-1 ps-2">
				<div class="sidebar-user-subtitle"><?= $this->session->userdata('name') ?></div>
					<div class="sidebar-user-subtitle"><?= $this->session->userdata('role') ?></div>
				</div>
			</div>
		</div>

		<ul class="sidebar-nav">

			<?php if ($this->session->userdata('role') == 'administrator') : ?>
				<li class="sidebar-item <?= ($title == 'Dashboard') ? 'active' : ''; ?>">
					<a class="sidebar-link " href="<?= base_url('dashboard') ?>">
						<i class="align-middle" data-feather="sliders"></i> <span class="align-middle">Dashboard</span>
					</a>
				</li>
			<?php endif ?>

			<li class="sidebar-item <?= ($title == 'Top Up') ? 'active' : ''; ?>">
				<a class="sidebar-link " href="<?= base_url('dashboard/top_ups/create') ?>">
					<i class="align-middle" data-feather="plus-square"></i> <span class="align-middle">Top Up</span>
				</a>
			</li>
			<li class="sidebar-item <?= ($title == 'Cek Saldo') ? 'active' : ''; ?>">
				<a class="sidebar-link " href="<?= base_url('dashboard/check_balance') ?>">
					<i class="align-middle" data-feather="credit-card"></i> <span class="align-middle">Cek Saldo</span>
				</a>
			</li>

			<?php if ($this->session->userdata('role') == 'administrator') : ?>
				<li class="sidebar-header">
					Data Master
				</li>
			<?php endif ?>

			<?php if ($this->session->userdata('role') == 'administrator') : ?>
				<li class="sidebar-item <?= ($title == 'Users') ? 'active' : ''; ?>">
					<a class="sidebar-link " href="<?= base_url('/dashboard/users') ?>">
						<i class="align-middle" data-feather="user"></i> <span class="align-middle">User</span>
					</a>
				</li>
			<?php endif ?>

			<?php if ($this->session->userdata('role') == 'administrator') : ?>
				<li class="sidebar-item <?= ($title == 'Roles') ? 'active' : ''; ?>">
					<a class="sidebar-link " href="<?= base_url('/dashboard/roles') ?>">
						<i class="align-middle" data-feather="target"></i> <span class="align-middle">Role</span>
					</a>
				</li>
			<?php endif ?>

			<?php if ($this->session->userdata('role') == 'administrator') : ?>
				<li class="sidebar-item <?= ($title == 'Guru / Staff') ? 'active' : ''; ?>">
					<a class="sidebar-link" href="<?= base_url('/dashboard/teacher_staffs') ?>">
						<i class="align-middle" data-feather="users"></i> <span class="align-middle">Guru / Staff</span>
					</a>
				</li>
			<?php endif ?>
			<?php if ($this->session->userdata('role') == 'administrator') : ?>
				<li class="sidebar-item <?= ($title == 'Siswa') ? 'active' : ''; ?>">
					<a class="sidebar-link" href="<?= base_url('/dashboard/students') ?>">
						<i class="align-middle" data-feather="users"></i> <span class="align-middle">Siswa</span>
					</a>
				</li>
			<?php endif ?>

			<?php if ($this->session->userdata('role') == 'administrator') : ?>
				<li class="sidebar-item <?= ($title == 'Kelas') ? 'active' : ''; ?>">
					<a class="sidebar-link" href="<?= base_url('/dashboard/classes') ?>">
						<i class="align-middle" data-feather="bookmark"></i> <span class="align-middle">Kelas</span>
					</a>
				</li>
			<?php endif ?>


			<?php if ($this->session->userdata('role') == 'administrator') : ?>
				<li class="sidebar-item <?= ($title == 'Devices') ? 'active' : ''; ?>">
					<a class="sidebar-link " href="<?= base_url('/dashboard/devices') ?>">
						<i class="align-middle" data-feather="speaker"></i> <span class="align-middle">Devices</span>
					</a>
				</li>
			<?php endif ?>

			<?php if ($this->session->userdata('role') == 'administrator') : ?>
				<li class="sidebar-item <?= ($title == 'Merchants') ? 'active' : ''; ?>">
					<a class="sidebar-link " href="<?= base_url('/dashboard/merchants') ?>">
						<i class="align-middle" data-feather="shopping-bag"></i> <span class="align-middle">Merchant</span>
					</a>
				</li>
			<?php endif ?>
			<?php if ($this->session->userdata('role') == 'administrator') : ?>
				<li class="sidebar-item <?= ($title == 'Kategori') ? 'active' : ''; ?>">
					<a class="sidebar-link" href="<?= base_url('/dashboard/categories') ?>">
						<i class="align-middle" data-feather="grid"></i> <span class="align-middle">Kategori</span>
					</a>
				</li>
			<?php endif ?>
			<?php if ($this->session->userdata('role') == 'administrator') : ?>
				<li class="sidebar-item <?= ($title == 'Produk') ? 'active' : ''; ?>">
					<a class="sidebar-link" href="<?= base_url('/dashboard/products') ?>">
						<i class="align-middle" data-feather="tag"></i> <span class="align-middle">Produk</span>
					</a>
				</li>
			<?php endif ?>

			<?php if ($this->session->userdata('role') == 'administrator') : ?>
				<li class="sidebar-header">
					Data Transaksi
				</li>
			<?php endif ?>

			<?php if ($this->session->userdata('role') == 'administrator') : ?>
				<li class="sidebar-item <?= ($title == 'Parkir Transaksi Siswa' || $title == 'Parkir Transaksi Guru Staff' || $title == 'Order Transaksi') ? 'active' : ''; ?>">
					<a data-bs-target="#multi" data-bs-toggle="collapse" class="sidebar-link collapsed">
						<i class="align-middle" data-feather="credit-card"></i> <span class="align-middle">Data Transaksi</span>
					</a>
					<ul id="multi" class="sidebar-dropdown list-unstyled collapse" data-bs-parent="#sidebar">
						<li class="sidebar-item">
							<a data-bs-target="#multi-1" data-bs-toggle="collapse" class="sidebar-link collapsed">Parkir Transaksi</a>
							<ul id="multi-1" class="sidebar-dropdown list-unstyled collapse">
								<li class="sidebar-item">
									<a class="sidebar-link <?= ($title == 'Parkir Transaksi Guru Staff') ? 'active' : ''; ?>" href="<?= base_url('dashboard/teacher_staff_parking_transactions') ?>">Guru / Staff</a>
									<a class="sidebar-link <?= ($title == 'Parkir Transaksi Siswa') ? 'active' : ''; ?>" href="<?= base_url('dashboard/student_parking_transactions') ?>">Siswa</a>
								</li>
							</ul>
						</li>
						<li class="sidebar-item  <?= ($title == 'Order Transaksi') ? 'active' : ''; ?>"><a class="sidebar-link" href="<?= base_url('dashboard/order_transactions') ?>">Order Transaksi</a></li>
					</ul>
				</li>
			<?php endif ?>

			<li class="sidebar-header">
				Laporan
			</li>

			<li class="sidebar-item <?= ($title == 'Data Top Up') ? 'active' : ''; ?>">
				<a class="sidebar-link" href="<?= base_url('dashboard/top_ups') ?>">
					<i class="align-middle" data-feather="folder-plus"></i> <span class="align-middle">Top Up</span>
				</a>
			</li>

		</ul>

	</div>
</aside>