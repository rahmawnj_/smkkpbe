<?php $this->load->view('layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('layout/header') ?>

        <main class="content">
        <div class="container-fluid p-0">
            <div class="row removable">
                <div class="col-lg-12">
                    <div class="card flex-fill">
                        <div class="card-header d-flex justify-content-between">
                            <h5 class="card-title mb-0"><?= $title ?></h5>
                            <!-- <a href="<?= base_url('dashboard/products/create') ?>" class="btn btn-primary float-right fas fa-plus"></a> -->
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                            <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>
                            <div class="flash-data-error" data-flashdataerror="<?= $this->session->flashdata('error') ?>"></div>
                            
                                <table class="table table-hover my-0" id="table" style="width:100% ;" >
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Gambar</th>
                                            <th>Nama</th>
                                            <th>Stok</th>
                                            <th>Merchant</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 0;
                                        foreach ($products as $product) : ?>
                                            <tr>
                                                <td><?= ++$no; ?></td>
                                                <td>
                                                    <img src="<?= base_url('../assets/img/uploads/' . $product['gambar']) ?>" class="img-fluid img-thumbnail mx-auto d-block text-center overflow-hidden" style="height:100px; width:100px" alt="">

                                                </td>
                                                <td><?= $product['nama'] ?></td>
                                                <td><?= $product['stok'] ?></td>
                                                <td><?= $product['merchant_nama'] ?></td>

                                                <td>
                                                    <!-- <a class="fa fa-edit btn bg-warning text-white" href="<?= base_url('dashboard/products/edit/' . $product['id_product']) ?>"></a> -->
                                                    <a class="fas fa-eye btn bg-info text-white" href="<?= base_url('dashboard/products/' . $product['id_product']) ?>"></a>
                                                    <!-- <a id="delete-button" class="fas fa-trash btn bg-danger text-white" href="<?= base_url('products/delete/' . $product['id_product']) ?>"></a> -->
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </main>

        <?php $this->load->view('layout/footer') ?>
    </div>
</div>

<?php $this->load->view('layout/foot') ?>