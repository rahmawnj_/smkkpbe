<?php $this->load->view('layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('layout/header') ?>

        <main class="content">
            <div class="container-fluid p-0">
                <div class="row removable">
                    <div class="col-lg-12">
                        <div class="card flex-fill">
                            <div class="card-header d-flex justify-content-between">
                                <h5 class="card-title mb-0"><?= $title ?></h5>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>


                                    <table id="table" class="table table-hover my-0 " style="width:100% ;">

                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nominal</th>
                                                <th>Nama Siswa</th>
                                                <th>NIS</th>
                                                <th>Tanggal</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 0;
                                            foreach ($top_ups as $top_up) : ?>
                                                <tr>
                                                    <td><?= ++$no; ?></td>
                                                    <td><?= $top_up['nominal'] ?></td>
                                                    <td><?= $top_up['student_nama'] ?></td>
                                                    <td><?= $top_up['student_nis'] ?></td>
                                                    <td><?= $top_up['created_at'] ?></td>
                                                    <td>
                                                        <a class="fas fa-eye btn btn-sm bg-info text-white" href="<?= base_url('dashboard/top_ups/' . $top_up['id_top_up']) ?>"></a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php $this->load->view('layout/footer') ?>
    </div>
</div>

<?php $this->load->view('layout/foot') ?>