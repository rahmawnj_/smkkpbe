<?php $this->load->view('layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('layout/header') ?>

        <main class="content">
        <div class="container-fluid p-0">
            <div class="row removable">
                <div class="col-lg-12">
                    <div class="card flex-fill">
                        <div class="card-header d-flex justify-content-between">
                            <h5 class="card-title mb-0"><?= $title ?></h5>
                            <a href="<?= base_url('dashboard/students/create') ?>" class="btn btn-primary float-right fas fa-plus"></a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                            <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>

                                <table class="table table-hover my-0" style="width: 100%;" id="table">

                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Foto</th>
                                            <th>NIS</th>
                                            <th>Nama</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 0;
                                        foreach ($students as $student) : ?>
                                            <tr>
                                                <td><?= ++$no; ?></td>
                                                <td>
                                                    <img src="<?= base_url('assets/img/uploads/' . $student['foto']) ?>" class="img-fluid img-thumbnail mx-auto d-block text-center overflow-hidden" style="height:100px; width:100px" alt="">

                                                </td>
                                                <td><?= $student['nis'] ?></td>
                                                <td><?= $student['nama'] ?></td>

                                                <td>
                                                    <!-- <a class="fa fa-key btn bg-warning text-white" href="<?= base_url('dashboard/top_ups/change_pin/' . $student['id_student']) ?>"></a> -->
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </main>
        <?php $this->load->view('layout/footer') ?>
    </div>
</div>

<?php $this->load->view('layout/foot') ?>

