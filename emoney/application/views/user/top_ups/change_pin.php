
<?php $this->load->view('layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('layout/header') ?>

        <main class="content">
        <div class="container-fluid p-0">
            <div class="row removable">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title mb-0"><?= $title ?></h5>
                        </div>
                        <div class="card-body">
                        <div class="flash-data-warning" data-flashdatawarning="<?= $this->session->flashdata('warning') ?>"></div>

                            <form action="<?= base_url('top_ups/change_pin_update') ?>" method="post">
                                <input type="hidden" class="form-control" name="id_student" value="<?= $student['id_student'] ?>">

                                <div class="form-group">
                                    <label for="pin">Pin</label>
                                    <input autocomplete="off" type="password" class="form-control" name="pin"  id="pin" aria-describedby="pin" placeholder="Masukan Pin">
                                    <span class="text-danger">
                                        <?= form_error('pin') ?>
                                    </span>
                                </div>

                                <div class="form-group">
                                    <label for="pin_konfirmasi">Pin Konfirmasi</label>
                                    <input autocomplete="off" type="password" class="form-control" name="pin_konfirmasi"  id="pin_konfirmasi" aria-describedby="pin_konfirmasi" placeholder="Masukan Pin Konfirmasi">
                                    <span class="text-danger">
                                        <?= form_error('pin_konfirmasi') ?>
                                    </span>
                                </div>

                                <div class="form-group">
                                    <button type="submit" value="upload" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

        <?php $this->load->view('layout/footer') ?>
    </div>
</div>

<?php $this->load->view('layout/foot') ?>

