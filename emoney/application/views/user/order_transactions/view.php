<?php $this->load->view('layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('layout/header') ?>

        <main class="content">
            <div class="container-fluid p-0">
                <div class="row removable">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title mb-0"><?= $title ?></h5>
                            </div>
                            <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>


                            <div class="card-body">
                         
                                <div class="row mt-2">
                                    <div class="col-md-3">Merchant</div>
                                    <div class="col"><?= $order_transaction['merchant_nama'] ?></div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-md-3">Nama</div>
                                    <div class="col"><?= $order_transaction['nama'] ?></div>
                                </div>
                                <?php if ($order_transaction['metode'] == 'emoney') : ?>
                                    <div class="row mt-2">
                                        <div class="col-md-3">Saldo</div>
                                        <div class="col"><?= "Rp " . number_format($order_transaction['saldo_awal'], 0, ',', '.')  ?></div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-3">Sisa Saldo</div>
                                        <div class="col"><?= "Rp " . number_format($order_transaction['saldo_akhir'], 0, ',', '.')   ?></div>
                                    </div>
                                <?php endif ?>
                                <div class="row mt-2">
                                    <div class="col-md-3">Tanggal</div>
                                    <div class="col"><?= $order_transaction['created_at'] ?></div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-md-3">Status</div>
                                    <div class="col"><?= $order_transaction['status'] ?></div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-md-3">Metode Pembayaran</div>
                                    <div class="col"><?= $order_transaction['metode'] ?></div>
                                </div>
                                <div class="mt-5">
                                    <h4> <?php
                                            $carts = $items;
                                            $jumlah = 0;
                                            foreach ($carts as $cart) {
                                                $jumlah += $cart['jumlah'];
                                            }
                                            echo $jumlah;
                                            ?> Barang</h4>
                                    <table class="table table-sm">
                                        <thead>
                                            <tr>
                                                <th>Deskripsi</th>
                                                <th>Jumlah</th>
                                                <th>Harga</th>
                                                <th>Jumlah Harga</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($items as $item) : ?>
                                                <tr>
                                                    <td><?= $item['nama'] ?></td>
                                                    <td><?= $item['jumlah'] ?></td>
                                                    <td><?= "Rp " . number_format($item['harga_jual'], 0, ',', '.')  ?></td>
                                                    <td><?= "Rp " . number_format($item['harga_jual'] * $item['jumlah'], 0, ',', '.')  ?></td>
                                                </tr>
                                            <?php endforeach ?>

                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th>Total</th>
                                                <th><?= "Rp " . number_format($order_transaction['harga'], 0, ',', '.')  ?></th>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <div class="text-center">
                                        <a href="<?= base_url('order_transactions/invoice/' . $order_transaction['id_order_transaction']) ?>" class="btn btn-primary">
                                            <i class="fas fa-print"></i>
                                            Print
                                        </a>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>



        <?php $this->load->view('layout/footer') ?>
    </div>
</div>

<?php $this->load->view('layout/foot') ?>