<?php $this->load->view('layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('layout/header') ?>

        <main class="content">
        <div class="container-fluid p-0">
            <div class="row removable">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                        <h5 class="card-title mb-0"><?= $title ?></h5>
                        </div>
                        <div class="card-body">
                            <?= $this->session->flashdata('message'); ?>
                            <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>

                            <form action="<?= base_url('settings/update') ?>" method="post">
                                <input type="hidden" class="form-control" name="id_setting" value="<?= $settings_secret_key['id_setting'] ?>">

                                <div class="form-group">
                                    <label for="secret_key">Secret Key</label>
                                    <input autocomplete="off" type="text" class="form-control" name="secret_key" value="<?= $settings_secret_key['value'] ?>" id="secret_key" aria-describedby="secret_key" placeholder="Masukan Kelas">
                                    <span class="text-danger">
                                        <?= form_error('secret_key') ?>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <button type="submit" value="upload" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

        <?php $this->load->view('layout/footer') ?>
    </div>
</div>

<?php $this->load->view('layout/foot') ?>
