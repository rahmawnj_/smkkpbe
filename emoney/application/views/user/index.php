<?php $this->load->view('layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('layout/header') ?>

        <main class="content">
            <div class="container-fluid p-0">
                <div class="row">
                    <div class="col-sm-6 col-xl-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col mt-0">
                                        <h5 class="card-title">Users</h5>
                                    </div>

                                    <div class="col-auto">
                                        <div class="stat text-primary">
                                            <i class="align-middle" data-feather="user"></i>
                                        </div>
                                    </div>
                                </div>
                                <h1 class="mt-1 mb-3"><?= count($users) ?></h1>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xl-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col mt-0">
                                        <h5 class="card-title">Siswa</h5>
                                    </div>

                                    <div class="col-auto">
                                        <div class="stat text-primary">
                                            <i class="align-middle" data-feather="users"></i>
                                        </div>
                                    </div>
                                </div>
                                <h1 class="mt-1 mb-3"><?= count($students) ?></h1>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xl-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col mt-0">
                                        <h5 class="card-title">Merchant</h5>
                                    </div>

                                    <div class="col-auto">
                                        <div class="stat text-primary">
                                            <i class="align-middle" data-feather="shopping-bag"></i>
                                        </div>
                                    </div>
                                </div>
                                <h1 class="mt-1 mb-3"><?= count($merchants) ?></h1>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xl-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col mt-0">
                                        <h5 class="card-title">Device</h5>
                                    </div>

                                    <div class="col-auto">
                                        <div class="stat text-primary">
                                            <i class="align-middle" data-feather="speaker"></i>
                                        </div>
                                    </div>
                                </div>
                                <h1 class="mt-1 mb-3"><?= count($devices) ?></h1>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-4 col-xxl-3 d-flex">
                        <div class="card flex-fill w-100">
                            <div class="card-header">

                                <h5 class="card-title mb-0">Transaksi</h5>
                            </div>
                            <div class="card-body d-flex w-100">
                                <div class="chart w-100">
                                    <div id="jmlh_transaksi"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-8 col-xxl-9 d-flex">
                        <div class="card flex-fill">
                            <div class="card-header">
                                <div class="card-actions float-end">
                                    <form action="" method="get" class="input-group mb-3 input-group-sm">
                                        <input type="date" name="ParkingStartDate" class="form-control">
                                        <input type="date" name="ParkingEndDate" class="form-control">
                                        <button class="btn btn-secondary" type="submit"> <i class="fas fa-search"></i> </button>
                                    </form>
                                </div>
                                <h5 class="card-title mb-0">Pemasukan</h5>
                            </div>
                            <div class="card-body">
                                <div class="chart w-100">
                                    <div id="pemasukan-chart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-8 col-xxl-9 d-flex">
                        <div class="card flex-fill">
                            <div class="card-header">
                                <div class="card-actions float-end">
                                    <form class="" method="GET" action="<?= base_url('dashboard') ?>">
                                        <div class="input-group input-group-sm">
                                            <input type="date" class="form-control" name="MarketStartDate">
                                            <input type="date" class="form-control" name="MarketEndDate">

                                            <button class="btn btn-secondary" type="submit">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </form>


                                </div>
                                <h5 class="card-title mb-0">Keuntungan</h5>
                            </div>
                            <div class="card-body">
                                <div class="chart w-100">
                                    <div id="apexcharts-area"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4 col-xxl-3 d-flex">
                        <div class="card flex-fill">
                            <div class="card-header">
                                <h5 class="card-title">Transaksi</h5>
                            </div>
                            <div class="card-body">
                                <div class="chart w-100">
                                    <div id="column-transaksi"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </main>

        <?php $this->load->view('layout/footer') ?>
    </div>
</div>
<script>
    var tr = JSON.parse('<?= $tr ?>')
    var tanggal = JSON.parse('<?= $tanggal ?>')
    var jmlTransaksi = JSON.parse('<?= $jml_transaksi ?>')
    var arr = [];
    var fails = []
    var succeses = []

    jmlTransaksi.forEach(jt => {
        fails.push(jt.gagal)
        succeses.push(jt.sukses)
    })
    tr.forEach(e => {
        var profits = []
        e.transaction.forEach(tr => {
            profits.push(tr.profit)
        })
        arr.push({
            name: e.merchant_nama,
            data: profits
        })
    });

    document.addEventListener("DOMContentLoaded", function() {
        // Column chart
        var options = {
            chart: {
                height: 350,
                type: "bar",
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    endingShape: "rounded",
                    columnWidth: "55%",
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ["transparent"]
            },
            series: [{
                name: "Gagal",
                data: fails
            }, {
                name: "Sukses",
                data: succeses
            }],
            xaxis: {
                categories: tanggal,
            },
            yaxis: {
                title: {
                    text: "transaksi"
                }
            },
            fill: {
                opacity: 1
            },
            tooltip: {
                y: {
                    formatter: function(val) {
                        return val + " Transaksi"
                    }
                }
            }
        }
        var chart = new ApexCharts(
            document.querySelector("#column-transaksi"),
            options
        );
        chart.render();
    });

    document.addEventListener("DOMContentLoaded", function() {

        var options = {
            chart: {
                height: 350,
                type: "area",
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: "smooth"
            },
            series: arr,
            xaxis: {
                type: "date",
                categories: tanggal,
            },
            tooltip: {
                x: {
                    format: "dd/MM/yy"
                },
            }
        }
        var chart = new ApexCharts(
            document.querySelector("#apexcharts-area"),
            options
        );
        chart.render();
    });
</script>

<script>
    var tr = JSON.parse('<?= $parkingTransaksi ?>')
    var dates = []
    var suksess = [];
    var gagals = [];
    tr.forEach(e => {
        dates.push(e.date)
        suksess.push(e.sukses)
        gagals.push(e.gagal)
    });

    var jmlh_transaksi = []
    var hargas = []
    var tanggals = [];

    var avenueParking = JSON.parse('<?= $avenueParking ?>')
    avenueParking.forEach(a => {
        hargas.push(a.jmlh_harga)
        tanggals.push(a.tanggal)
        jmlh_transaksi.push(a.jmlh_transaksi)
    })
    document.addEventListener("DOMContentLoaded", function() {

        var chart = new ApexCharts(
            document.querySelector("#pemasukan-chart"), {
                chart: {
                    height: 350,
                    type: "line",
                    stacked: false,
                },
                stroke: {
                    width: [0, 2, 5],
                    curve: "smooth"
                },
                series: [{
                    name: "Transaksi",
                    type: "column",
                    data: jmlh_transaksi
                }, {
                    name: "Pemasukan",
                    type: "area",
                    data: hargas
                }],
                fill: {
                    opacity: [0.85, 0.25, 1],
                    gradient: {
                        inverseColors: false,
                        shade: "light",
                        type: "vertical",
                        opacityFrom: 0.85,
                        opacityTo: 0.55,
                        stops: [0, 100, 100, 100]
                    }
                },
                labels: tanggals,
                markers: {
                    size: 0
                },
                xaxis: {
                    type: "date",
                    categories: dates
                },
                yaxis: {
                    title: {
                        text: "Transaksi",
                    },
                    min: 0
                },
                tooltip: {
                    shared: true,
                    intersect: false,
                    y: {
                        formatter: function(y) {
                            if (typeof y !== "undefined") {
                                return y.toFixed(0);
                            }
                            return y;
                        }
                    }
                }
            }
        );
        chart.render();
    });

    document.addEventListener("DOMContentLoaded", function() {
        // Column chart
        var options = {
            chart: {
                height: 350,
                type: "bar",
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    endingShape: "rounded",
                    columnWidth: "55%",
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ["transparent"]
            },
            series: [{
                name: "Gagal",
                data: gagals
            }, {
                name: "Sukses",
                data: suksess
            }],
            xaxis: {
                categories: dates,
            },
            yaxis: {
                title: {
                    text: "Transaksi"
                }
            },
            fill: {
                opacity: 1
            },
            tooltip: {
                y: {
                    formatter: function(val) {
                        return val + " Transaksi"
                    }
                }
            }
        }
        var chart = new ApexCharts(
            document.querySelector("#jmlh_transaksi"),
            options
        );
        chart.render();
    });
</script>

<?php $this->load->view('layout/foot') ?>