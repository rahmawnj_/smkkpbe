<?php $this->load->view('layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('layout/header') ?>

        <main class="content">
            <div class="container-fluid p-0">
                <div class="row removable">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title mb-0"><?= $title ?></h5>
                            </div>
                            <div class="card-body">
                                <?= $this->session->flashdata('message'); ?>

                                <form id="" action="<?= base_url('dashboard/check_balance') ?>" method="get">
                                    <div class="input-group w-100 d-flex justify-content center ">
                                        <div class="form-outline">
                                            <input type="text" autocomplete="off" name="keyword" value="<?= $this->input->get('keyword') ?>" class="form-control w-100" placeholder="Masukan NIS atau RFID">
                                        </div>
                                        <button class="btn btn-primary">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </form>

                                <?php if ($this->input->get('keyword')) :  ?>
                                    <?php if (count($student) !== 0) : ?>
                                        <div class="card mt-5">
                                            <div class="card-body">
                                                <div class="container-fluid">
                                                    <img src="<?= base_url('assets/img/uploads/' . $student['foto']) ?>" class="img-fluid img-thumbnail mx-auto d-block overflow-hidden" style="height:100px; width:100px" alt="">
                                                    <div class="row mt-2 mb-2">
                                                        <div class="col-md-1"><strong>Nama</strong></div>
                                                        <div class="col"><?= $student['nama'] ?></div>
                                                    </div>
                                                    <div class="row mt-2 mb-2">
                                                        <div class="col-md-1"><strong>NIS</strong></div>
                                                        <div class="col"><?= $student['nis'] ?></div>
                                                    </div>
                                                    <div class="row mt-2 mb-2">
                                                        <div class="col-md-1"><strong>Saldo</strong></div>
                                                        <div class="col"><?= "Rp " . number_format($student['saldo'], 0, ',', '.') ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php else : ?>
                                        <div class="alert alert-danger alert-dismissible mt-5" role="alert">
                                            <div class="alert-message">
                                                RFID atau NIS tidak ditemukan.

                                            </div>
                                        </div>

                                    <?php endif ?>
                                <?php endif ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <?php $this->load->view('layout/footer') ?>
    </div>
</div>

<?php $this->load->view('layout/foot') ?>