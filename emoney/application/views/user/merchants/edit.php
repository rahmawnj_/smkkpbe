<?php $this->load->view('layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('layout/header') ?>

        <main class="content">
            <div class="container-fluid p-0">
                <div class="row removable">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title mb-0"><?= $title ?></h5>
                            </div>
                            <div class="card-body">
                                <?= $this->session->flashdata('message'); ?>

                                <?php echo form_open_multipart('merchants/update'); ?>
                                <input type="hidden" class="form-control" name="id_merchant" value="<?= $merchant['id_merchant'] ?>">

                                <div class="img-preview d-flex">
                                    <img src="<?= base_url('../assets/img/uploads/' . $merchant['gambar']) ?>" id="gmbr" class="img-fluid img-thumbnail mx-auto d-block text-center" style="height:200px;" alt="">
                                </div>
                                <div class="form-group">
                                    <label for="gambar">Gambar</label>
                                    <input type="file" id="gambar" class="form-control" size="20" name="gambar" id="gambar" aria-describedby="gambar" placeholder="Masukan gambar">
                                    <span class="text-danger">
                                        <?= form_error('gambar') ?>
                                    </span>
                                </div>

                                <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <input autocomplete="off" type="text" class="form-control" name="nama" value="<?= $merchant['nama'] ?>" id="nama" aria-describedby="nama" placeholder="Masukan Nama">
                                    <span class="text-danger">
                                        <?= form_error('nama') ?>
                                    </span>
                                </div>

                                <h4 class="mt-3">Owner</h4>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="username_owner">Username</label>
                                            <input autocomplete="off" type="text" class="form-control" name="username_owner" value="<?= $merchant_owner['username'] ?>" id="username_owner" aria-describedby="username_owner" placeholder="Masukan Username Owner" required>
                                            <span class="text-danger">
                                                <?= form_error('username_owner') ?>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="password_owner">Password</label>
                                            <input autocomplete="off" type="password" class="form-control" name="password_owner" id="password_owner" aria-describedby="password_owner" placeholder="Masukan Password Owner">
                                            <span class="text-danger">
                                                <?= form_error('password_owner') ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <h4 class="mt-3">Kasir</h4>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="username_cashier">Username</label>
                                            <input autocomplete="off" type="text" class="form-control" name="username_cashier" value="<?= $merchant_cashier['username'] ?>" id="username_cashier" aria-describedby="username_cashier" placeholder="Masukan Username Kasir" required>
                                            <span class="text-danger">
                                                <?= form_error('username_cashier') ?>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="password_cashier">Password</label>
                                            <input autocomplete="off" type="password" class="form-control" name="password_cashier" id="password_cashier" aria-describedby="password_cashier" placeholder="Masukan Password Kasir">
                                            <span class="text-danger">
                                                <?= form_error('password_cashier') ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                                <?= form_close() ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php $this->load->view('layout/footer') ?>
    </div>
</div>

<?php $this->load->view('layout/foot') ?>