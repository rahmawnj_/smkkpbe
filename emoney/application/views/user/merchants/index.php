<?php $this->load->view('layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('layout/header') ?>

        <main class="content">
        <div class="container-fluid p-0">
            <div class="row removable">
                <div class="col-lg-12">
                    <div class="card flex-fill">
                        <div class="card-header d-flex justify-content-between">
                            <h5 class="card-title mb-0"><?= $title ?></h5>
                            <a href="<?= base_url('dashboard/merchants/create') ?>" class="btn btn-primary float-right fas fa-plus"></a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                            <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>
                            <div class="flash-data-error" data-flashdataerror="<?= $this->session->flashdata('error') ?>"></div>
                                <table class="table table-hover my-0" id="table" style="width:100%;" >

                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Gambar</th>
                                            <th>Nama</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 0;
                                        foreach ($merchants as $merchant) : ?>
                                            <tr>
                                                <td><?= ++$no; ?></td>
                                                <td>
                                                    <img src="<?= base_url('../assets/img/uploads/' . $merchant['gambar']) ?>" class="img-fluid img-thumbnail mx-auto d-block text-center overflow-hidden" style="height:100px; width:100px" alt="">

                                                </td>
                                                <td><?= $merchant['nama'] ?></td>
                                                <td>
                                                    <!-- <a class="fas btn-sm fa-pencil-square btn bg-primary text-white" href="<?= base_url('dashboard/merchants/products/' . $merchant['id_merchant']) ?>"></a> -->
                                                    <a class="fa btn-sm fa-edit btn bg-warning text-white" href="<?= base_url('dashboard/merchants/edit/' . $merchant['id_merchant']) ?>"></a>
                                                    <a class="fas btn-sm fa-eye btn bg-info text-white" href="<?= base_url('dashboard/merchants/' . $merchant['id_merchant']) ?>"></a>
                                                    <a id="delete-button" class="fas btn-sm fa-trash btn bg-danger text-white" href="<?= base_url('merchants/delete/' . $merchant['id_merchant']) ?>"></a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </main>

        <?php $this->load->view('layout/footer') ?>
    </div>
</div>

<?php $this->load->view('layout/foot') ?>