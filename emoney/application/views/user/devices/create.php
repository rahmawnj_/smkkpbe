
<?php $this->load->view('layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('layout/header') ?>

        <main class="content">
        <div class="container-fluid p-0">
            <div class="row removable">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title mb-0"><?= $title ?></h5>
                        </div>
                        <div class="card-body">
                            <?= $this->session->flashdata('message'); ?>

                            <?php echo form_open_multipart('devices/store'); ?>
                            <div class="img-preview d-flex">
                                <img src="<?= base_url('assets/img/default_device.jfif') ?>" id="gmbr" class="img-fluid img-thumbnail mx-auto d-block text-center" style="height:200px" alt="">
                            </div>
                            <div class="form-group">
                                <label for="gambar">Gambar</label>
                                <input autocomplete="off" type="file" id="gambar" class="form-control" size="20" name="gambar" id="gambar" aria-describedby="gambar" required placeholder="Masukan gambar">
                                <span class="text-danger">
                                    <?= form_error('gambar') ?>
                                </span>
                            </div>

                            <div class="form-group">
                                <label for="device">ID Device</label>
                                <input autocomplete="off" type="number" class="form-control" name="device" id="device" aria-describedby="device" placeholder="Masukan ID Device" required>
                                <span class="text-danger">
                                    <?= form_error('device') ?>
                                </span>
                            </div>

                            <div class="form-group">
                                <label for="harga">Harga</label>
                                <input autocomplete="off" type="number" class="form-control" name="harga" id="harga" aria-describedby="harga" placeholder="Masukan Harga" required>
                                <span class="text-danger">
                                    <?= form_error('harga') ?>
                                </span>
                            </div>
                            <div class="form-group">
                                <label for="lokasi">Lokasi</label>
                                <input autocomplete="off"autocomplete="off" type="lokasi" class="form-control" required name="lokasi" id="lokasi" aria-describedby="lokasi" placeholder="Masukan Lokasi">
                                <span class="text-danger">
                                    <?= form_error('lokasi') ?>
                                </span>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            <?= form_close() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

        <?php $this->load->view('layout/footer') ?>
    </div>
</div>

<?php $this->load->view('layout/foot') ?>