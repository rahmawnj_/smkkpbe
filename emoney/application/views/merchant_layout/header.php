<header class="navbar navbar-expand navbar-light navbar-bg">
	<a class="sidebar-toggle js-sidebar-toggle">
		<i class="hamburger align-self-center"></i>
	</a>

	<div class="navbar-collapse collapse">
		<ul class="navbar-nav navbar-align">
			<li class="nav-item dropdown">
				<a class="nav-icon dropdown-toggle" href="#" id="alertsDropdown" data-bs-toggle="dropdown">
					<div class="position-relative">
						<i class="align-middle" data-feather="shopping-cart"></i>
						<span class="indicator" id="nav_total">
							<?php
							$ci = &get_instance();
							$ci->load->model('cart_model');
							$carts = $ci->cart_model->get_cart('carts.id_merchant', $this->session->userdata('id_merchant'));
							$jumlah = 0;
							foreach ($carts as $cart) {
								$jumlah += $cart['jumlah'];
							}
							echo $jumlah;
							?>
						</span>
					</div>
				</a>
				<div class="dropdown-menu dropdown-menu-lg menu-cart dropdown-menu-end py-0 pt-3 pb-3" aria-labelledby="alertsDropdown">
					<div class="row total-header-section">
						<div class="col-lg-6 col-sm-6 col-6">
							<i class="fa fa-shopping-cart" aria-hidden="true"></i>
							<span class="badge badge-pill badge-danger" id="nav_total_pill">
								<?php
								$ci = &get_instance();
								$ci->load->model('cart_model');
								$carts = $ci->cart_model->get_cart('carts.id_merchant', $this->session->userdata('id_merchant'));
								$jumlah = 0;
								foreach ($carts as $cart) {
									$jumlah += $cart['jumlah'];
								}
								echo $jumlah;
								?>
							</span>
						</div>
						<div class="col-lg-6 col-sm-6 col-6 total-section text-right">
							<p>Total:
								<span class="text-info" id="total_harga_nav">
									<?php
									$ci = &get_instance();
									$ci->load->model('cart_model');
									$carts = $ci->cart_model->get_cart('carts.id_merchant', $this->session->userdata('id_merchant'));
									$jumlah_harga = 0;
									foreach ($carts as $cart) {
										$jumlah_harga += $cart['harga_jual'] * $cart['jumlah'];
									}
									echo "Rp " . number_format($jumlah_harga, 0, ',', '.');
									?>
								</span>
							</p>
						</div>
					</div>
					<?php
					$ci = &get_instance();
					$ci->load->model('cart_model');
					$carts = $ci->cart_model->get_cart('carts.id_merchant', $this->session->userdata('id_merchant'));
					foreach ($carts as $cart) : ?>
						<div class="row cart-detail" id="cart-item-nav-<?php echo $cart["id_cart"]; ?>">
							<div class="col-lg-4 col-sm-4 col-4 cart-detail-img">
								<img src="<?= base_url('assets/img/uploads/' . $cart['gambar']) ?>">
							</div>
							<div class="col-lg-8 col-sm-8 col-8 cart-detail-product">
								<div class="row">
									<div class="col-md-9">
										<p> <?= $cart['nama'] ?> </p>
										<span class="price text-info">
											<?=
											"Rp " . number_format($cart['harga_jual'], 0, ',', '.');
											?>
										</span>
										<span class="count"> x
											<span id="total-items-<?= $cart['id_cart'] ?>"><?= $cart['jumlah'] ?></span>
										</span>
									</div>
									<div class="col-md-3 text-md-end">
										<div class="row">
											<div class="item-right col-md-12 float-right text-right clearfix ">
												<a href="<?= base_url('carts/delete/' . $cart['id_cart']); ?>" onclick="deleteCart()" class="btn btn-sm text-right btn-danger float-right pull-right">x</a>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					<?php endforeach ?>

					<div class="row">
						<div class="d-grid gap-1">
							<a class="btn btn-info mt-3" href="<?= base_url('merchant/carts/edit') ?>"><i class="fa fa-shopping-cart text-white"></i> Keranjang</a>
							<a class="btn btn-dark" href="<?= base_url('order_transactions/create') ?>"><i class="fa fa-money-check-alt text-white"></i> Checkout</a>
						</div>

					</div>
				</div>
			</li>


			<li class="nav-item">
				<a class="nav-icon js-fullscreen d-none d-lg-block" href="#">
					<div class="position-relative">
						<i class="align-middle" data-feather="maximize"></i>
					</div>
				</a>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-icon pe-md-0 dropdown-toggle" href="#" data-bs-toggle="dropdown">
					<img src="<?= base_url('assets/img/uploads/' . $this->session->userdata('gambar')) ?? '' ?>" class="avatar img-fluid rounded" alt="<?= $this->session->userdata('username') ?>">
				</a>
				<div class="dropdown-menu dropdown-menu-end">
					<a class="dropdown-item" href="<?= base_url('auth/merchant_profile') ?>"><i class="align-middle me-1" data-feather="user"></i> Profile</a>
					<!-- <a class="dropdown-item" href="#"><i class="align-middle me-1" data-feather="pie-chart"></i> Analytics</a> -->
					<!-- <div class="dropdown-divider"></div>
					<a class="dropdown-item" href="pages-settings.html"><i class="align-middle me-1" data-feather="settings"></i> Settings &
						Privacy</a>
					<a class="dropdown-item" href="#"><i class="align-middle me-1" data-feather="help-circle"></i> Help Center</a> -->
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="<?= base_url('auth/logout') ?>"><i class="align-middle me-1" data-feather="log-out"></i> Log out</a>
				</div>
			</li>
		</ul>
	</div>
</header>