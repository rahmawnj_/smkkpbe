<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Devices extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("form_validation");
        $this->load->model(['device_model']);
        $this->load->helper(['form', 'url']);
        if (!$this->session->userdata('status')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
    }

    public function index()
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }

        $data = [
            'title' => 'Devices',
            'devices' => $this->device_model->get_devices()
        ];

        $this->load->view('user/devices/index', $data);
    }

    public function create()
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $data = [
            'title' => 'Devices',
        ];

        $this->load->view('user/devices/create', $data);
    }

    public function store()
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $this->form_validation->set_rules('harga', 'Price', 'required|trim');
        $this->form_validation->set_rules('lokasi', 'Location', 'required|trim');
        $this->form_validation->set_rules('device', 'Device', 'required|trim|is_unique[devices.device]');

        if ($this->form_validation->run() == false) {
            $this->create();
        } else {

            $config['upload_path']          = '../assets/img/uploads/devices';
            $config['allowed_types']        = 'gif|jpg|png|jpeg|jfif';
            $config['max_size']             = 10000;
            $config['max_width']            = 10000;
            $config['max_height']           = 10000;

            $this->load->library('upload', $config);
            $this->upload->do_upload('gambar');

            $this->device_model->insert([
                'device' => $this->input->post('device'),
                'deleted' => 0,
                'harga' => $this->input->post('harga'),
                'lokasi' => $this->input->post('lokasi'),
                'gambar' => 'devices/' . $this->upload->data('file_name'),
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $this->session->set_flashdata('success', 'Devices Berhasil Ditambahkan!');
            redirect('dashboard/devices');
        }
    }

    public function view($id_device)
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $data = [
            'title' => 'Devices',
            'device' => $this->device_model->get_device('id_device', $id_device)[0]
        ];

        $this->load->view('user/devices/view', $data);
    }

    public function edit($id_device)
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $data = [
            'title' => 'Devices',
            'device' => $this->device_model->get_device('id_device', $id_device)[0]
        ];

        $this->load->view('user/devices/edit', $data);
    }

    public function update()
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $device = $this->device_model->get_device('id_device', $this->input->post('id_device'))[0];

        if ($this->input->post('device') == $device['device']) {
            $device_rules = 'required|trim';
        } else {
            $device_rules = 'required|trim|is_unique[devices.device]';
        }

        $this->form_validation->set_rules('lokasi', 'Location', 'required|trim');
        $this->form_validation->set_rules('harga', 'Price', 'required|trim');
        $this->form_validation->set_rules('device', 'Device', $device_rules);

        if ($this->form_validation->run() == false) {
            $this->edit($this->input->post('id_device'));
        } else {
            $config['upload_path']          = '../assets/img/uploads/devices';
            $config['allowed_types']        = 'gif|jpg|png|jpeg|jfif';
            $config['max_size']             = 10000;
            $config['max_width']            = 10000;
            $config['max_height']           = 10000;

            $this->load->library('upload', $config);
            $this->upload->do_upload('gambar');
            $this->session->set_userdata([
                'gambar' => 'devices/' . $this->upload->data('file_name'),
            ]);

            if ($this->upload->data('file_name') == '') {
                $gambar = $device['gambar'];
            } else {
                unlink('../assets/img/uploads/' . $device['gambar']);
                $gambar = 'devices/' . $this->upload->data('file_name');
            }

            $this->device_model->update($this->input->post('id_device'), [
                'harga' => $this->input->post('harga'),
                'device' => $this->input->post('device'),
                'lokasi' => $this->input->post('lokasi'),
                'gambar' => $gambar,
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            $this->session->set_flashdata('success', 'Devices Berhasil Diperbarui!');
            redirect('dashboard/devices');
        }
    }

    public function delete($id_device)
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $device = $this->device_model->get_device('id_device', $id_device)[0];
        $this->device_model->delete($id_device);
        unlink('../assets/img/uploads/' . $device['gambar']);
        $this->session->set_flashdata('success', 'Devices Berhasil Dihapus!');
        
        redirect('dashboard/devices');
    }
}
