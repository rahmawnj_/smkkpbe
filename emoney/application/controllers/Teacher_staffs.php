<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Teacher_staffs extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("form_validation");
        $this->load->model(['teacher_staff_model']);
        $this->load->helper(['form', 'url', 'encryption']);
        if (!$this->session->userdata('status')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
    }

    public function template()
	{
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
        // set Header
        $sheet->SetCellValue('A1', 'nama')->getColumnDimension('A')->setAutoSize(true);
        $sheet->SetCellValue('B1', 'rfid')->getColumnDimension('B')->setAutoSize(true);
        $sheet->SetCellValue('C1', 'jabatan')->getColumnDimension('C')->setAutoSize(true);

        $sheet->SetCellValue('A2', 'Putra')->getColumnDimension('A')->setAutoSize(true);
        $sheet->SetCellValue('B2', '1111111111')->getColumnDimension('B')->setAutoSize(true);
        $sheet->SetCellValue('C2', 'Guru')->getColumnDimension('C')->setAutoSize(true);

        $sheet->SetCellValue('A3', 'Putri')->getColumnDimension('A')->setAutoSize(true);
        $sheet->SetCellValue('B3', '2222222222')->getColumnDimension('B')->setAutoSize(true);
        $sheet->SetCellValue('C3', 'Staff')->getColumnDimension('C')->setAutoSize(true);
		$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
		$filename = 'example-guru-staff';

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
		header('Cache-Control: max-age=0');
		
		$writer->save('php://output'); // download file 

	}


    public function export_excel()
	{
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
        // set Header
        $sheet->SetCellValue('A1', 'Nama')->getColumnDimension('A')->setAutoSize(true);
        $sheet->SetCellValue('B1', 'RFID')->getColumnDimension('B')->setAutoSize(true);
        $sheet->SetCellValue('C1', 'jabatan')->getColumnDimension('C')->setAutoSize(true);      
        // set Row
		$teacher_staffs = $this->teacher_staff_model->get_teacher_staffs();
        $rowCount = 2;
        foreach ($teacher_staffs as $teacher_staff) {
            $sheet->SetCellValue('A' . $rowCount, $teacher_staff['nama'])->getColumnDimension('A')->setAutoSize(true);
            $sheet->SetCellValue('B' . $rowCount, $teacher_staff['rfid'])->getColumnDimension('B')->setAutoSize(true);
            $sheet->SetCellValue('C' . $rowCount, $teacher_staff['jabatan'])->getColumnDimension('C')->setAutoSize(true);
            $rowCount++;
        }
		
		$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);

		$filename = 'guru-staff';

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
		header('Cache-Control: max-age=0');
		
		$writer->save('php://output'); // download file 

	}

    // file upload functionality
    public function upload()
    {
        $data = array();
        // Load form validation library
        $this->load->library('form_validation');
        $this->form_validation->set_rules('fileURL', 'Upload File', 'callback_checkFileValidation');
        if ($this->form_validation->run() == false) {

            $this->load->view('spreadsheet/index', $data);
        } else {
            // If file uploaded
            if (!empty($_FILES['fileURL']['name'])) {
                // get file extension
                $extension = pathinfo($_FILES['fileURL']['name'], PATHINFO_EXTENSION);

                if ($extension == 'csv') {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
                } elseif ($extension == 'xlsx') {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                } else {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                }
                // file path
                $spreadsheet = $reader->load($_FILES['fileURL']['tmp_name']);
                $allDataInSheet = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

                // array Count
                $arrayCount = count($allDataInSheet);
                $flag = 0;
                $createArray = ['nama', 'rfid', 'jabatan'];
                $makeArray = ['nama' => 'nama', 'rfid' => 'rfid', 'jabatan' => 'jabatan'];
                $SheetDataKey = [];
                foreach ($allDataInSheet as $dataInSheet) {
                    foreach ($dataInSheet as $key => $value) {
                        if (in_array(trim($value), $createArray)) {
                            $value = preg_replace('/\s+/', '', $value);
                            $SheetDataKey[trim($value)] = $key;
                        }
                    }
                }

                $dataDiff = array_diff_key($makeArray, $SheetDataKey);
                if (empty($dataDiff)) {
                    $flag = 1;
                }

                // match excel sheet column
                if ($flag == 1) {
                    for ($i = 2; $i <= $arrayCount; $i++) {
                        $nama = $SheetDataKey['nama'];
                        $jabatan = $SheetDataKey['jabatan'];
                        $rfid = $SheetDataKey['rfid'];


                        $nama = filter_var(trim($allDataInSheet[$i][$nama]), FILTER_SANITIZE_STRING);
                        $jabatan = filter_var(trim($allDataInSheet[$i][$jabatan]), FILTER_SANITIZE_STRING);
                        $rfid = filter_var(trim($allDataInSheet[$i][$rfid]), FILTER_SANITIZE_STRING);
                        $deleted = filter_var(trim(0), FILTER_SANITIZE_STRING);
                        $fetchData[] = ['deleted' => $deleted, 'nama' => $nama, 'jabatan' => $jabatan, 'rfid' => $rfid];
                    }
                    $data['dataInfo'] = $fetchData;
                    $this->teacher_staff_model->setBatchImport($fetchData);
                    $this->teacher_staff_model->importData();
                } else {
                    echo "Please import correct file, did not match excel sheet column";
                }
                $this->session->set_flashdata('success', 'Guru / Staff Berhasil Ditambahkan!');
                redirect('dashboard/teacher_staffs');
            }
        }
    }

    // checkFileValidation
    public function checkFileValidation($string)
    {
        $file_mimes = array(
            'text/x-comma-separated-values',
            'text/comma-separated-values',
            'application/octet-stream',
            'application/vnd.ms-excel',
            'application/x-csv',
            'text/x-csv',
            'text/csv',
            'application/csv',
            'application/excel',
            'application/vnd.msexcel',
            'text/plain',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        );
        if (isset($_FILES['fileURL']['name'])) {
            $arr_file = explode('.', $_FILES['fileURL']['name']);
            $extension = end($arr_file);
            if (($extension == 'xlsx' || $extension == 'xls' || $extension == 'csv') && in_array($_FILES['fileURL']['type'], $file_mimes)) {
                return true;
            } else {
                $this->form_validation->set_message('checkFileValidation', 'Please choose correct file.');
                return false;
            }
        } else {
            $this->form_validation->set_message('checkFileValidation', 'Please choose a file.');
            return false;
        }
    }

    public function index()
    {
       
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $data = [
            'title' => 'Guru / Staff',
            'teacher_staffs' => $this->teacher_staff_model->get_teacher_staffs()
        ];

        $this->load->view('user/teacher_staffs/index', $data);
    }

    public function create()
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $data = [
            'title' => 'Guru / Staff',
        ];

        $this->load->view('user/teacher_staffs/create', $data);
    }

    public function store()
    {

        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $this->form_validation->set_rules('nama', 'Name', 'required|trim');
        $this->form_validation->set_rules('rfid', 'RFID', 'required|trim|is_unique[teacher_staffs.rfid]|is_unique[students.rfid]');
        $this->form_validation->set_rules('jabatan', 'Position', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->create();
        } else {

            $config['upload_path']          = '../assets/img/uploads/teacher_staffs';
            $config['allowed_types']        = 'gif|jpg|png|jpeg|jfif';
            $config['max_size']             = 10000;
            $config['max_width']            = 10000;
            $config['max_height']           = 10000;

            $this->load->library('upload', $config);
            $this->upload->do_upload('foto');
      
            $this->teacher_staff_model->insert([
                'nama' => $this->input->post('nama'),
                'rfid' => $this->input->post('rfid'),
                'jabatan' => $this->input->post('jabatan'),
                'deleted' => 0,
                'foto' => 'teacher_staffs/' . $this->upload->data('file_name'),
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $this->session->set_flashdata('success', 'Guru / Staff Berhasil Ditambahkan!');
            redirect('dashboard/teacher_staffs');
        }
    }

    public function view($id_teacher_staff)
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $data = [
            'title' => 'Guru / Staff',
            'teacher_staff' => $this->teacher_staff_model->get_teacher_staff('id_teacher_staff', $id_teacher_staff)[0]
        ];

        $this->load->view('user/teacher_staffs/view', $data);
    }

    public function edit($id_teacher_staff)
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $data = [
            'title' => 'Guru / Staff',
            'teacher_staff' => $this->teacher_staff_model->get_teacher_staff('id_teacher_staff', $id_teacher_staff)[0]
        ];

        $this->load->view('user/teacher_staffs/edit', $data);
    }

    public function update()
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $teacher_staff = $this->teacher_staff_model->get_teacher_staff('id_teacher_staff', $this->input->post('id_teacher_staff'))[0];

        if ($this->input->post('rfid') == $teacher_staff['rfid']) {
            $rfid_rules = 'required|trim';
        } else {
            $rfid_rules = 'required|trim|is_unique[teacher_staffs.rfid]|is_unique[students.rfid]';
        }

        $this->form_validation->set_rules('nama', 'Name', 'required|trim');
        $this->form_validation->set_rules('rfid', 'RFID', $rfid_rules);
        $this->form_validation->set_rules('jabatan', 'Position', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->edit($this->input->post('id_teacher_staff'));
        } else {
            $config['upload_path']          = '../assets/img/uploads/teacher_staffs';
            $config['allowed_types']        = 'gif|jpg|png|jpeg|jfif';
            $config['max_size']             = 10000;
            $config['max_width']            = 10000;
            $config['max_height']           = 10000;

            $this->load->library('upload', $config);
            $this->upload->do_upload('foto');

            if ($this->upload->data('file_name') == '') {
                $foto = $teacher_staff['foto'];
            } else {
                unlink('../assets/img/uploads/' . $teacher_staff['foto']);
                $foto = 'teacher_staffs/' . $this->upload->data('file_name');
            }

            $this->teacher_staff_model->update($this->input->post('id_teacher_staff'), [
                'nama' => $this->input->post('nama'),
                'rfid' => $this->input->post('rfid'),
                'jabatan' => $this->input->post('jabatan'),
                'foto' => $foto,
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            $this->session->set_flashdata('success', 'Guru / Staff Berhasil Diperbarui!');
            redirect('dashboard/teacher_staffs');
        }
    }

    public function delete($id_teacher_staff)
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $teacher_staff = $this->teacher_staff_model->get_teacher_staff('id_teacher_staff', $id_teacher_staff)[0];

        $this->teacher_staff_model->delete($id_teacher_staff);
        unlink('../assets/img/uploads/' . $teacher_staff['foto']);
        $this->session->set_flashdata('success', 'Guru / Staff Berhasil Dihapus!');
        redirect('dashboard/teacher_staffs');
    }
}
