<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['student_model', 'class_model', 'user_model', 'device_model', 'category_model', 'merchant_model', 'order_transaction_model', 'cart_model', 'product_model', 'student_parking_transaction_model']);

        $this->load->helper(['form', 'url', 'encryption']);
        $this->load->library('form_validation');

        if (!$this->session->userdata('status')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
    }

    public function arrayGroupByMerchantAndDate($data)
    {

        $result = [];
        foreach ($data as $key => $value) {
            $result[$value['merchant_id']][$value['date']][] = $value;
        }
        return $result;
    }

    function groupArrayByMerchantAndDateAndAddProfit($data)
    {
        $result = [];
        foreach ($data as $key => $value) {
            $temp = explode(" ", $value['created_at']);
            $date = $temp[0];

            $result[$value['id_merchant']][$date]['profit'] = 0;
            $result[$value['id_merchant']][$date]['profit'] += $value['profit'];
        }
        return $result;
    }


    public function check_balance()
    {
        if ($this->session->userdata('table') !== 'user') {
            show_404();
        }
        $student = $this->student_model->search_student($this->input->get('keyword'));
        if (count($student) !== 0) {
            $student = $student[0];
            $student['saldo'] = decryption($student['saldo'], de_iv(),  $student['nis']);
        }
        $data = [
            'title' => 'Cek Saldo',
            'student' => $student
        ];

        $this->load->view('user/check_balance', $data);
    }

    public function merchant_check_balance()
    {
        if ($this->session->userdata('table') !== 'merchant') {
            show_404();
        }

        $student = $this->student_model->search_student($this->input->get('keyword'));
        if (count($student) !== 0) {
            $student = $student[0];

            $student['saldo'] = decryption($student['saldo'], de_iv(),  $student['nis']);
        }
        $data = [
            'title' => 'Cek Saldo',
            'student' => $student
        ];

        $this->load->view('merchant/check_balance', $data);
    }

    public function merchant_dashboard()
    {
        if ($this->session->userdata('role') !== 'owner') {
            show_404();
        }
        $aWeekAgo = strtotime("-1 week");

        $order_transactions = $this->order_transaction_model->get_otr($this->session->userdata('id_merchant'));

        $transactionByDate = [];
        $total_profit = 0;
        foreach ($order_transactions as $key => $value) {
            $temp = explode(" ", $value['created_at']);
            $date = $temp[0];
            $strDate = strtotime($value['created_at']);

            if ($strDate > $aWeekAgo) {
                $total = (isset($transactionByDate[$date]['profit']) ? $transactionByDate[$date]['profit'] + $value['profit'] : $value['profit']);

                $transactionByDate[$date] = ['date' => $date, 'profit' => intval($total)];
                $total_profit += $value['profit'];
            }
        }
        $trans = [];
        foreach ($transactionByDate as $tbd) {
            $trans[] = $tbd;
        }

        $products = $this->product_model->get_product('products.id_merchant', $this->session->userdata('id_merchant'));

        $ctgrs = [];
        foreach ($products as $key => $value) {
            $total = (isset($ctgrs[$value['kategori']]['jmlh']) ? $ctgrs[$value['kategori']]['jmlh'] + 1 : 1);
            $ctgrs[$value['kategori']] = ['category' => $value['kategori'], 'jmlh' => intval($total)];
        }
        $prdcts = [];
        foreach ($ctgrs as $ctgr) {
            $prdcts[] = $ctgr;
        }

        $data = [
            'title' => 'Dashboard',
            'trans' => json_encode($trans),
            'ctgr' => json_encode($prdcts),
            'categories' => $prdcts,
            'products' => $this->product_model->get_product('products.id_merchant', $this->session->userdata('id_merchant')),
            'trnsctn' => $order_transactions,
            'profit' => $total_profit,
            'order_transactions' => $this->order_transaction_model->get_order_transaction('order_transactions.id_merchant', $this->session->userdata('id_merchant'))
        ];

        $this->load->view('merchant/index', $data);
    }

    public function index()
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }

        if ($this->input->GET('ParkingStartDate', TRUE) && $this->input->GET('ParkingEndDate', TRUE)) {

            $this->db->order_by('student_parking_transactions.created_at', 'asc');
            $student_parking_transactions = $this->student_parking_transaction_model->get_by_date(
                $this->input->get('ParkingStartDate'),
                $this->input->get('ParkingEndDate')
            );
            $avenueParking = $this->student_parking_transaction_model->avenueParking(
                $this->input->get('ParkingStartDate'),
                $this->input->get('ParkingEndDate')
            );

        } else {
            $this->db->order_by('student_parking_transactions.created_at', 'asc');
            $student_parking_transactions = $this->student_parking_transaction_model->get_by_date(
                date('Y-m-d', strtotime('-' . date('w') . ' days')),
                date('Y-m-d', strtotime('+' . (6 - date('w')) . ' days'))
            );
            $avenueParking = $this->student_parking_transaction_model->avenueParking(
                date('Y-m-d', strtotime('-' . date('w') . ' days')),
                date('Y-m-d', strtotime('+' . (6 - date('w')) . ' days'))
            );
        }

        $parkingByDate = [];
        $total_harga = 0;
        foreach ($student_parking_transactions as $key => $value) {
            $temp = explode(" ", $value['created_at']);
            $date = $temp[0];
            $strDate = strtotime($value['created_at']);
                if ($value['status'] == 'sukses') {
                    if (isset($parkingByDate[$date]['sukses'])) {
                        $sukses = $parkingByDate[$date]['sukses'] + 1;
                        $gagal = (isset($parkingByDate[$date]['gagal']) ? $parkingByDate[$date]['gagal'] : 0);
                    } else {
                        $sukses = 1;
                        $gagal = (isset($parkingByDate[$date]['gagal']) ? $parkingByDate[$date]['gagal'] : 0);
                    }
                } else {
                    if (isset($parkingByDate[$date]['gagal'])) {
                        $gagal = $parkingByDate[$date]['gagal'] + 1;
                        $sukses = (isset($parkingByDate[$date]['sukses']) ? $parkingByDate[$date]['sukses'] : 0);
                    } else {
                        $sukses = (isset($parkingByDate[$date]['sukses']) ? $parkingByDate[$date]['sukses'] : 0);
                        $gagal = 1;
                    }
                }

                $jmlh_transaksi = (isset($parkingByDate[$date]['jmlh_transaksi']) ? $parkingByDate[$date]['jmlh_transaksi'] + 1 : 1);
                $parkingByDate[$date] = ['date' => $date, 'jmlh_transaksi' => $jmlh_transaksi, 'gagal' => $gagal, 'sukses' => $sukses];
                $total_harga += $value['harga'];
        }

        $parkingTransaksi = [];
        foreach ($parkingByDate as $tbd) {
            $parkingTransaksi[] = $tbd;
        }

    // ??????????????????????????????????????

   

        if ($this->input->get('MarketStartDate', TRUE) && $this->input->get('MarketEndDate', TRUE)) {
            $MarketStartDate = $this->input->get('MarketStartDate');
            $MarketEndDate = $this->input->get('MarketEndDate');
            $this->db->order_by('order_transactions.created_at', 'asc');
            $order_transactions = $this->order_transaction_model->get_by_date(
               $MarketStartDate,
                $MarketEndDate
            );
        } else {
            $MarketStartDate = date('Y-m-d', strtotime('-' . date('w') . ' days'));
            $MarketEndDate = date('Y-m-d', strtotime('+' . (6 - date('w')) . ' days'));
            
            $this->db->order_by('order_transactions.created_at', 'asc');
            $order_transactions = $this->order_transaction_model->get_by_date(
                $MarketStartDate, $MarketEndDate
            );
        }
       

        $merchant = [];
        foreach ($order_transactions as $tr) {
            $merchant[$tr['id_merchant']][] = $tr;
        }
        $markets = [];
        $date = [];
        $tanggal = [];
        $dates = [];
        foreach ($merchant as $key => $mr) {
            foreach ($mr as $key => $m) {
                $temp = explode(" ", $m['created_at']);
                $date = $temp[0];
                $strDate = strtotime($m['created_at']);
                    $tanggal[$date] = $date;
                    $dates[$date] = [
                        'date' => $date,
                        'profit' => 0
                    ];
              
            }
        }
        $tgl = [];
        foreach ($tanggal as $tg) {
            $tgl[] = $tg;
        }

        $jmlTransaksi = [];
        foreach ($order_transactions as $key => $value) {
            $temp = explode(" ", $value['created_at']);
            $date = $temp[0];
            $strDate = strtotime($value['created_at']);
                if ($value['status'] == 'sukses') {
                    if (isset($jmlTransaksi[$date]['sukses'])) {
                        $sukses = $jmlTransaksi[$date]['sukses'] + 1;
                        $gagal = (isset($jmlTransaksi[$date]['gagal']) ? $jmlTransaksi[$date]['gagal'] : 0);
                    } else {
                        $sukses = 1;
                        $gagal = (isset($jmlTransaksi[$date]['gagal']) ? $jmlTransaksi[$date]['gagal'] : 0);
                    }
                } else {
                    if (isset($jmlTransaksi[$date]['gagal'])) {
                        $gagal = $jmlTransaksi[$date]['gagal'] + 1;
                        $sukses = (isset($jmlTransaksi[$date]['sukses']) ? $jmlTransaksi[$date]['sukses'] : 0);
                    } else {
                        $sukses = (isset($jmlTransaksi[$date]['sukses']) ? $jmlTransaksi[$date]['sukses'] : 0);
                        $gagal = 1;
                    }
                }

                $jmlh_transaksi = (isset($jmlTransaksi[$date]['jmlh_transaksi']) ? $jmlTransaksi[$date]['jmlh_transaksi'] + 1 : 1);
                $jmlTransaksi[$date] = ['date' => $date, 'jmlh_transaksi' => $jmlh_transaksi, 'gagal' => $gagal, 'sukses' => $sukses];
          
        }

        $jTr = [];
        foreach ($jmlTransaksi as $value) {
            $jTr[] = $value;
        }

        foreach ($merchant as $key => $mr) {
            $transactionByDate = [];
            $total_profit = 0;
            $merchant_nama = 0;

            foreach ($mr as $key => $m) {
                $temp = explode(" ", $m['created_at']);
                $date = $temp[0];
                $strDate = strtotime($m['created_at']);
                    $total = (isset($transactionByDate[$date]['profit']) ? $transactionByDate[$date]['profit'] + $m['profit'] : $m['profit']);

                    $transactionByDate[$date] = ['date' => $date, 'profit' => intval($total)];
                    $total_profit += $m['profit'];
               
                $merchant_nama = $m['merchant_nama'];
            }
            $d = array_replace($dates, $transactionByDate);

            $id = [];
            foreach ($d as $ds) {
                $id[] = $ds;
            }
            $markets[$merchant_nama]['merchant_nama'] = $merchant_nama;
            $markets[$merchant_nama]['transaction'] = $id;
        }

        $trans = [];
        foreach ($markets as $mrkt) {
            $trans[] = $mrkt;
        }

        $data = [
            'title' => 'Dashboard',
            'users' => $this->user_model->get_users(),
            'students' => $this->student_model->get_students(),
            'merchants' => $this->merchant_model->get_merchants(),
            'devices' => $this->device_model->get_devices(),
            'parkingTransaksi' => json_encode($parkingTransaksi),
            'tr' => json_encode($trans),
            'tanggal' => json_encode($tgl),
            'jml_transaksi' => json_encode($jTr),
            'avenueParking' => json_encode($avenueParking)
        ];

        $this->load->view('user/index', $data);
    }
}
