<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Top_ups extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("form_validation");
        $this->load->model(['top_up_model', 'student_model', 'user_model']);
        $this->load->helper(['form', 'url']);
        if (!$this->session->userdata('status')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
    }

    public function index()
    {
        if (!$this->session->userdata('status')) {
            show_404();
        }

        $this->db->order_by('created_at', 'desc');
        $top_ups = $this->top_up_model->get_top_ups();
        $data = [
            'title' => 'Data Top Up',
            'top_ups' => $top_ups
        ];

        $this->load->view('user/top_ups/index', $data);
    }

    public function create()
    {
        if (!$this->session->userdata('status')) {
            show_404();
        }
        $data = [
            'title' => 'Top Up',
            'students' =>  $this->student_model->get_students()
        ];

        $this->load->view('user/top_ups/create', $data);
    }

    public function store()
    {
        if (!$this->session->userdata('status')) {
            show_404();
        }
        $this->form_validation->set_rules('nominal', 'Nominal', 'required|trim');
        $this->form_validation->set_rules('rfid_nis', 'RFID / NIS', 'required|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->create();
        } else {
            $user =  $this->user_model->get_user('id_user', $this->session->userdata('id_user'))[0];
            if (password_verify($this->input->post('password'), $user['password'])) {
                $this->db->where('nis', $this->input->post('rfid_nis'));
                $this->db->or_where('rfid', $this->input->post('rfid_nis'));
                $student = $this->db->get('students')->result_array()[0];

             
                $student['saldo'] = decryption($student['saldo'], de_iv(),  $student['nis']);
                $this->top_up_model->insert([
                    'id_student'  => $student['id_student'],
                    'id_user' => $this->session->userdata('id_user'),
                    'nominal' => $this->input->post('nominal'),
                    'saldo_awal' => $student['saldo'],
                    'saldo_akhir' => $student['saldo'] + $this->input->post('nominal'),
                    'created_at' => date('Y-m-d H:i:s')
                ]);

                $encryption =  encryption($student['saldo'] + $this->input->post('nominal'), en_iv(), $student['nis']);

                $this->student_model->update($student['id_student'], [
                    'saldo' => $encryption,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
              
                if (password_verify('000000', $student['pin'])) {
                    $this->session->set_flashdata('warning', 'Password default perlu diganti!');
                    redirect('dashboard/top_ups/change_pin/' . $student['id_student']);
                } else {
                $this->session->set_flashdata('success', 'Transaksi Berhasil!');
                redirect('dashboard/top_ups');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
                <div class="alert-message">
                Password Salah!
                </div>
            </div>');
                redirect('dashboard/top_ups/create');
            }
        }
    }

    public function view($id_top_up)
    {
        if (!$this->session->userdata('status')) {
            show_404();
        }

        $data = [
            'title' => 'Data Top Up',
            'top_up' => $this->top_up_model->get_top_up($id_top_up)[0]
        ];
        $this->load->view('user/top_ups/view', $data);
    }

    public function edit($id_top_up)
    {
    }

    public function update()
    {
    }

    public function delete($id_top_up)
    {
    }

    public function change_pin($id_student)
    {
        if (!$this->session->userdata('status')) {
            show_404();
        }

        $data = [
            'title' => 'Top Up',
            'student' => $this->student_model->get_student('id_student', $id_student)[0]
        ];


        $this->load->view('user/top_ups/change_pin', $data);
    }

    public function change_pin_update()
    {
        if (!$this->session->userdata('status')) {
            show_404();
        }
        $this->form_validation->set_rules('pin', 'Pin', 'required|trim');
        $this->form_validation->set_rules('pin_konfirmasi', 'Password Confirmation', 'required|trim|matches[pin]');
        if ($this->form_validation->run() == false) {
            $this->change_pin($this->input->post('id_student'));
        } else {
            $this->student_model->update($this->input->post('id_student'), [
                'student_pin' => password_hash($this->input->post('pin'), PASSWORD_DEFAULT),
            ]);

            $this->session->set_flashdata('success', 'Pin Berhasil Diperbarui!');
            redirect('dashboard/top_ups/students');
        }
    }

    public function students()
    {
        if (!$this->session->userdata('status')) {
            show_404();
        }
        $data = [
            'title' => 'Top Up Siswa',
            'students' => $this->student_model->get_students()
        ];


        $this->load->view('user/top_ups/students', $data);
    }
}
