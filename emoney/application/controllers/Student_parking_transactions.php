<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class student_parking_transactions extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(["form_validation", 'session', 'encryption']);
        $this->load->model(['student_parking_transaction_model', 'student_model']);
        $this->load->helper(['form', 'url']);
        if (!$this->session->userdata('status')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
    }

    public function index()
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $this->db->order_by('created_at', 'desc');

        $student_parking_transactions = $this->student_parking_transaction_model->get_student_parking_transactions();
        $data = [
            'title' => 'Parkir Transaksi Siswa',
            'student_parking_transactions' => $student_parking_transactions
        ];

        $this->load->view('user/student_parking_transactions/index', $data);
    }

    public function create()
    {
    }

    public function store()
    {
    }

    public function view($id_student_parking_transaction)
    {

        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }

        $data = [
            'title' => 'Parkir Transaksi Siswa',
            'student_parking_transactions' => $this->student_parking_transaction_model->get_student_parking_transaction('id_student_parking_transaction', $id_student_parking_transaction)[0],
        ];


        $this->load->view('user/student_parking_transactions/view', $data);
    }

    public function edit($id_student_parking_transaction)
    {
    }

    public function update()
    {
    }

    public function delete($id_student_parking_transaction)
    {
    }

    public function students($id_student_parking_transaction)
    {
    }

    public function group_by($key, $data)
    {
        $result = [];
        foreach ($data as $val) {
            if (array_key_exists($key, $val)) {
                $result[$val][$key];
            } else {
                $result[''][] = $val;
            }
        }
        return $result;
    }


    public function invoice($id_student_parking_transaction)
    {
        if (!$this->session->userdata('status')) {
            show_404();
        }
        $data = [
            'student_parking_transactions' => $this->student_parking_transaction_model->get_student_parking_transaction('id_student_parking_transaction', $id_student_parking_transaction)[0],
        ];

        $mpdf = new \Mpdf\Mpdf();
        $html = $this->load->view('user/student_parking_transactions/invoice_pdf', $data, true);
        $mpdf->WriteHTML($html);
        $mpdf->Output('INV- ' . $data['student_parking_transactions']['created_at'] . '.pdf', 'D');
    }

    public function export_excel()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        if ($this->input->GET('startDate', TRUE) && $this->input->GET('endDate', TRUE)) {
            $this->db->order_by('student_parking_transactions.created_at', 'desc');
            $student_parking_transactions = $this->student_parking_transaction_model->get_by_date(
                $this->input->get('startDate'),
                $this->input->get('endDate')
            );
            $filename = $this->input->GET('startDate', TRUE) . " - " . $this->input->GET('endDate', TRUE);
        } else {
            $this->db->order_by('student_parking_transactions.created_at', 'desc');
            $student_parking_transactions = $this->student_parking_transaction_model->get_student_parking_transactions();
            $filename = date('D-M-Y');
        }

        // set Header
        $sheet->SetCellValue('A1', 'Nama');
        $sheet->SetCellValue('B1', 'NIS');
        $sheet->SetCellValue('C1', 'Saldo Awal');
        $sheet->SetCellValue('D1', 'Saldo Akhir');
        $sheet->SetCellValue('E1', 'harga');
        $sheet->SetCellValue('F1', 'Tanggal');
        $sheet->SetCellValue('G1', 'Status');
        // set Row
     
        $rowCount = 2;
        foreach ($student_parking_transactions as $ptransaction) {
            $sheet->SetCellValue('A' . $rowCount, $ptransaction['student_nama'])->getColumnDimension('A')->setAutoSize(true);
            $sheet->SetCellValue('B' . $rowCount, $ptransaction['student_nis'])->getColumnDimension('B')->setAutoSize(true);
            $sheet->SetCellValue('C' . $rowCount, $ptransaction['saldo_awal'])->getColumnDimension('C')->setAutoSize(true);
            $sheet->SetCellValue('D' . $rowCount, $ptransaction['saldo_akhir'])->getColumnDimension('D')->setAutoSize(true);
            $sheet->SetCellValue('E' . $rowCount, $ptransaction['harga'])->getColumnDimension('E')->setAutoSize(true);
            $sheet->SetCellValue('F' . $rowCount, $ptransaction['created_at'])->getColumnDimension('F')->setAutoSize(true);
            $sheet->SetCellValue('G' . $rowCount, $ptransaction['status'])->getColumnDimension('G')->setAutoSize(true);
            $rowCount++;
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output'); // download file 

    }
}
