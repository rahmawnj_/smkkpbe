<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Categories extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("form_validation");
        $this->load->model(['category_model', 'product_model']);
        $this->load->helper(['form', 'url', 'date']);
        if (!$this->session->userdata('status')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
    }

    public function index()
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }

        $data = [
            'title' => 'Kategori',
            'categories' => $this->category_model->get_categories()
        ];

        $this->load->view('user/categories/index', $data);
    }

    public function create()
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $data = [
            'title' => 'Kategori',
        ];


        $this->load->view('user/categories/create', $data);
    }

    public function store()
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }

        $this->form_validation->set_rules('category', 'Category', 'required|trim');


        if ($this->form_validation->run() == false) {
            $this->create();
        } else {

            $this->category_model->insert([
                'kategori' => $this->input->post('category'),
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $this->session->set_flashdata('success', 'Kategori Berhasil Ditambahkan!');
            redirect('dashboard/categories');
        }
    }

    public function view($id_category)
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }

        $data = [
            'title' => 'Kategori',
            'category' => $this->category_model->get_category('id_category', $id_category)
        ];

        $this->load->view('user/categories/view', $data);
    }

    public function edit($id_category)
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }

        $data = [
            'title' => 'Kategori',
            'category' => $this->category_model->get_category('id_category', $id_category)[0]
        ];
        $this->load->view('user/categories/edit', $data);
    }

    public function update()
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $this->form_validation->set_rules('kategori', 'Kategori', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->edit($this->input->post('id_category'));
        } else {
            $this->category_model->update($this->input->post('id_category'), [
                'kategori' => $this->input->post('kategori'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            $this->session->set_flashdata('success', 'Kategori Berhasil Diperbarui!');
            redirect('dashboard/categories');
        }
    }

    public function delete($id_category)
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $products = $this->product_model->get_product('products.id_category', $id_category);
        if (!$products) {
            $this->category_model->delete($id_category);
            $this->session->set_flashdata('success', 'Kategori Berhasil Dihapus!');
        } else {
            $this->session->set_flashdata('error', 'Kategori Gagal Dihapus!');
        }
        redirect('dashboard/categories');
    }

    public function products($id_category)
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $data = [
            'title' => 'Kategori',
            'category' => $this->category_model->get_category('id_category', $id_category)[0],
            'products' => $this->product_model->get_product('products.id_category', $id_category)
        ];

        $this->load->view('user/categories/products', $data);
    }

    public function categories()
    {
        if ($this->session->userdata('role') !== 'owner') {
            show_404();
        }
        $products = $this->product_model->get_product('products.id_merchant', $this->session->userdata('id_merchant'));
        $categories = [];
        foreach ($products as $key => $product) {
            $found = in_array($product['id_category'], array_column($categories, 'id_category'));
            if (!$found) {
                $categories[] = [
                    'id_category' => $product['id_category'],
                    'kategori' => $product['kategori'],
                ];
            }
        }
        $data = [
            'title' => 'Kategori',
            'categories' => $categories
        ];

        $this->load->view('merchant/categories/index', $data);
    }

    public function merchant_product($id_category)
    {
        if ($this->session->userdata('role') !== 'owner') {
            show_404();
        }

        $data = [
            'title' => 'Kategori',
            'category' => $this->category_model->get_category('id_category', $id_category)[0],
            'products' => $this->product_model->get_product_merchant($this->session->userdata('id_merchant'), 'products.id_category', $id_category)
        ];

        $this->load->view('merchant/categories/products', $data);
    }
}
