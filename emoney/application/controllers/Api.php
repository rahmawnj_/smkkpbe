

<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Api extends RestController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("form_validation");
        $this->load->model(['student_model', 'teacher_staff_model', 'setting_model', 'device_model', 'student_parking_transaction_model', 'teacher_staff_parking_transaction_model']);
        $this->load->helper(['form', 'url']);
    }

    public function parkir_json_post()
    {
        if (is_null($this->input->post('secret_key'))) {
            $this->response(['msg' => 'secret_key perlu dikirim', 'status' => 404]);
        }
        if (is_null($this->input->post('id_device'))) {
            $this->response(['msg' => 'id_device perlu dikirim', 'status' => 404]);
        }
        if (is_null($this->input->post('rfid'))) {
            $this->response(['msg' => 'rfid perlu dikirim', 'status' => 404]);
        }

        $setting_secret_key = $this->setting_model->get_setting('name', 'secret_key');
        if ($setting_secret_key[0]['value'] == $this->input->post('secret_key')) {

            $device = $this->device_model->get_device('device', $this->input->post('id_device'));
            if ($device) {
                $student = $this->student_model->get_student('rfid', $this->input->post('rfid'));
                $teacher_staff = $this->teacher_staff_model->get_teacher_staff('rfid', $this->input->post('rfid'));

                if (count($student) > 0) {
                    $device = $device[0];
                    $student = $student[0];
                    $student['saldo'] = decryption($student['saldo'], de_iv(),  $student['nis']);

                    if (($student['saldo'] - $device['harga']) >= 0) {
                        $encryption =  encryption($student['saldo'] - $device['harga'], en_iv(), $student['nis']);
                        $this->student_model->update($student['id_student'], [
                            'saldo' => $encryption,
                            'updated_at' => date('Y-m-d H:i:s')
                        ]);
                        $this->student_parking_transaction_model->insert([
                            'id_student'  => $student['id_student'],
                            'id_device' => $device['id_device'],
                            'harga' => $device['harga'],
                            'saldo_awal' => $student['saldo'],
                            'status' => 'sukses',
                            'saldo_akhir' => $student['saldo'] - $device['harga'],
                            'created_at' => date('Y-m-d H:i:s')
                        ]);
                        $this->response(['msg' => 'Berhasil', 'status' => 200, 'harga_parkir' => number_format($device['harga'], 0, ',', '.'), 'saldo' => number_format($student['saldo'] - $device['harga'], 0, ',', '.'), 'nis' => $student['nis'], 'nama' => $student['nama'], 'kelas' => $student['kelas']]);
                    } else {
                        $this->student_parking_transaction_model->insert([
                            'id_student'  => $student['id_student'],
                            'id_device' => $device['id_device'],
                            'harga' => $device['harga'],
                            'saldo_awal' => $student['saldo'],
                            'status' => 'gagal',
                            'saldo_akhir' => $student['saldo'],
                            'created_at' => date('Y-m-d H:i:s')
                        ]);
                        $this->response(['msg' => 'Saldo Kurang', 'status' => 202, 'harga_parkir' => number_format($device['harga'], 0, ',', '.'), 'saldo' => number_format($student['saldo'], 0, ',', '.'), 'nis' => $student['nis'], 'nama' => $student['nama'], 'kelas' => $student['kelas']]);
                    }
                } else if (count($teacher_staff) > 0) {
                    $teacher_staff = $teacher_staff[0];
                    $device = $device[0];

                    $this->teacher_staff_parking_transaction_model->insert([
                        'id_teacher_staff'  => $teacher_staff['id_teacher_staff'],
                        'id_device' => $device['id_device'],
                        'created_at' => date('Y-m-d H:i:s')
                    ]);
                    $this->response(['msg' => 'Berhasil', 'status' => 200, 'harga_parkir' => number_format(0, 0, ',', '.'), 'nama' => $teacher_staff['nama']]);
                } else {
                    $this->response(['msg' => 'rfid tidak ditemukan', 'status' => 404]);
                }
            } else {
                $this->response(['msg' => 'id_device tidak ditemukan', 'status' => 404]);
            }
        } else {
            $this->response(['msg' => 'secret_key salah', 'status' => 404]);
        }
    }
}
