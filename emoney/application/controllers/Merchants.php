<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Merchants extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("form_validation");
        $this->load->model(['merchant_model', 'merchant_account_model', 'product_model']);
        $this->load->helper(['form', 'url']);
        if (!$this->session->userdata('status')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
    }

    public function index()
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }

        $data = [
            'title' => 'Merchants',
            'merchants' => $this->merchant_model->get_merchants()
        ];

        $this->load->view('user/merchants/index', $data);
    }

    public function create()
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $data = [
            'title' => 'Merchants',
        ];

        $this->load->view('user/merchants/create', $data);
    }

    public function store()
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $this->form_validation->set_rules('nama', 'Name', 'required|trim');
        $this->form_validation->set_rules('username_owner', 'Username Owner', 'required|trim|is_unique[merchant_accounts.username]');
        $this->form_validation->set_rules('password_owner', 'Password Owner', 'required|trim|min_length[6]');
        $this->form_validation->set_rules('username_cashier', 'Username Kasir', 'required|trim|is_unique[merchant_accounts.username]');
        $this->form_validation->set_rules('password_cashier', 'Password Kasir', 'required|trim|min_length[6]');

        if ($this->form_validation->run() == false) {
            $this->create();
        } else {
            if ($this->input->post('username_owner') == $this->input->post('username_cashier')) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
                <div class="alert-message">
                    Username tidak boleh sama
                </div>
            </div>');
                redirect('dashboard/merchants/create');
            }
            $config['upload_path']          = '../assets/img/uploads/merchants';
            $config['allowed_types']        = 'gif|jpg|png|jpeg|jfif';
            $config['max_size']             = 10000;
            $config['max_width']            = 10000;
            $config['max_height']           = 10000;

            $this->load->library('upload', $config);
            $this->upload->do_upload('gambar');

            $this->merchant_model->insert([
                'nama' => $this->input->post('nama'),
                'deleted' => 0,
                'gambar' => 'merchants/' . $this->upload->data('file_name'),
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $this->db->order_by('created_at', 'desc');
            $merchant = $this->merchant_model->get_merchants()[0]['id_merchant'];

            $this->merchant_account_model->insert([
                'id_merchant' => $merchant,
                'username' => $this->input->post('username_owner'),
                'password' => password_hash($this->input->post('password_owner'), PASSWORD_DEFAULT),
                'role' => 'owner',
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $this->merchant_account_model->insert([
                'id_merchant' => $merchant,
                'username' => $this->input->post('username_cashier'),
                'password' => password_hash($this->input->post('password_cashier'), PASSWORD_DEFAULT),
                'role' => 'cashier',
                'created_at' => date('Y-m-d H:i:s')
            ]);


            $this->session->set_flashdata('success', 'Merchant Berhasil Ditambahkan!');
            redirect('dashboard/merchants');
        }
    }

    public function view($id_merchant)
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $data = [
            'title' => 'Merchants',
            'merchant' => $this->merchant_model->get_merchant('id_merchant', $id_merchant)[0],
            'merchant_account_owner' => $this->merchant_account_model->get_merchant_account2('merchant_accounts.id_merchant', $id_merchant, 'role', 'owner')[0],
            'merchant_account_cashier' => $this->merchant_account_model->get_merchant_account2('merchant_accounts.id_merchant', $id_merchant, 'role', 'cashier')[0],
        ];

        $this->load->view('user/merchants/view', $data);
    }

    public function edit($id_merchant)
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }

        $data = [
            'title' => 'Merchants',
            'merchant' => $this->merchant_model->get_merchant('id_merchant', $id_merchant)[0],
            'merchant_owner' => $this->merchant_account_model->get_merchant_account2('merchant_accounts.id_merchant', $id_merchant, 'role', 'owner')[0],
            'merchant_cashier' => $this->merchant_account_model->get_merchant_account2('merchant_accounts.id_merchant', $id_merchant, 'role', 'cashier')[0],
        ];

        $this->load->view('user/merchants/edit', $data);
    }

    public function update()
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }

        if ($this->input->post('username_owner') == $this->input->post('username_cashier')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
                Username tidak boleh sama
            </div>
        </div>');
            redirect('dashboard/merchants/edit/'. $this->input->post('id_merchant') );
        }

        $merchant = $this->merchant_model->get_merchant('merchants.id_merchant', $this->input->post('id_merchant'))[0];
        $merchant_owner = $this->merchant_account_model->get_merchant_account2('merchant_accounts.id_merchant', $this->input->post('id_merchant'), 'role', 'owner')[0];
        $merchant_cashier = $this->merchant_account_model->get_merchant_account2('merchant_accounts.id_merchant', $this->input->post('id_merchant'), 'role', 'cashier')[0];

        if ($this->input->post('username_owner') == $merchant_owner['username'] || $this->input->post('username_cashier') == $merchant_cashier['username']) {
            $owner_rules = 'required|trim';
            $cashier_rules = 'required|trim';
        } else {
            $owner_rules = 'required|trim|is_unique[merchant_accounts.username]';
            $cashier_rules = 'required|trim|is_unique[merchant_accounts.username]';
        }

        $this->form_validation->set_rules('nama', 'Name', 'required|trim');
        $this->form_validation->set_rules('password_owner', 'Password Owner', 'trim');
        $this->form_validation->set_rules('password_cashier', 'Password Cashier', 'trim');
        $this->form_validation->set_rules('username_owner', 'Username Owner', $owner_rules);
        $this->form_validation->set_rules('username_cashier', 'Username Cashier', $cashier_rules);

        if ($this->form_validation->run() == false) {
            $this->edit($this->input->post('id_merchant'));
        } else {

            $config['upload_path']          = '../assets/img/uploads/merchants';
            $config['allowed_types']        = 'gif|jpg|png|jpeg|jfif';
            $config['max_size']             = 10000;
            $config['max_width']            = 10000;
            $config['max_height']           = 10000;

            $this->load->library('upload', $config);
            $this->upload->do_upload('gambar');
            $this->session->set_userdata([
                'gambar' => 'merchants/' . $this->upload->data('file_name'),
                'name' => $this->input->post('nama')
            ]);

            if ($this->upload->data('file_name') == '') {
                $gambar = $merchant['gambar'];
            } else {
                unlink('../assets/img/uploads/' . $merchant['gambar']);
                $gambar = 'merchants/' . $this->upload->data('file_name');
            }

            if ($this->input->post('password_owner') == '' || $this->input->post('password_cashier') == '') {
                $password_owner = $merchant_owner['password'];
                $password_cashier = $merchant_cashier['password'];
            } else {
                $password_owner = password_hash($this->input->post('password_owner'), PASSWORD_DEFAULT);
                $password_cashier = password_hash($this->input->post('password_cashier'), PASSWORD_DEFAULT);
            }

            $this->merchant_account_model->update($merchant_owner['id_merchant_account'], [
                'username' => $this->input->post('username_owner'),
                'password' => $password_owner,
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            $this->merchant_account_model->update($merchant_cashier['id_merchant_account'], [
                'username' => $this->input->post('username_cashier'),
                'password' => $password_cashier,
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            $this->merchant_model->update($this->input->post('id_merchant'), [
                'nama' => $this->input->post('nama'),
                'gambar' => $gambar,
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            $this->session->set_flashdata('success', 'Merchant Berhasil Diperbarui!');
            redirect('dashboard/merchants');
        }
    }

    public function delete($id_merchant)
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $products = $this->product_model->get_product('products.id_merchant', $id_merchant);
        if (!$products) {
            $merchant = $this->merchant_model->get_merchant('id_merchant', $id_merchant)[0];
            $merchant_owner = $this->merchant_account_model->get_merchant_account2('merchant_accounts.id_merchant', $id_merchant, 'role', 'owner')[0];
            $merchant_cashier = $this->merchant_account_model->get_merchant_account2('merchant_accounts.id_merchant', $id_merchant, 'role', 'cashier')[0];
      
            $this->merchant_model->delete($id_merchant);
            $this->merchant_account_model->delete($merchant_owner['id_merchant_account']);
            $this->merchant_account_model->delete($merchant_cashier['id_merchant_account']);
    
            unlink('../assets/img/uploads/' . $merchant['gambar']);
            $this->session->set_flashdata('success', 'Merchant Berhasil Dihapus!');
        } else {
            $this->session->set_flashdata('error', 'Merchant Gagal Dihapus!');
        }

        redirect('dashboard/merchants');
    }

    public function products($id_merchant)
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $data = [
            'title' => 'Merchant',
            'merchant' => $this->merchant_model->get_merchant('id_merchant', $id_merchant)[0],
            'products' => $this->product_model->get_product('products.id_merchant', $id_merchant)
        ];

        $this->load->view('user/merchants/products', $data);
    }
}
