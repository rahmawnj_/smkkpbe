<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Carts extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(["form_validation", 'session']);
        $this->load->model(['cart_model', 'product_model']);
        $this->load->helper(['form', 'url']);
        if (!$this->session->userdata('status')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
    }

    public function index()
    {
    }

    public function create()
    {
    }

    public function store()
    {
        $cart = $this->cart_model->get_cart('carts.id_product', $this->input->post('id_product'));

        if (count($cart) !== 0) {
            $jumlah = $cart[0]['jumlah'] + $this->input->post('jumlah');

            $this->cart_model->update($cart[0]['id_cart'], [
                'jumlah' => $jumlah,
            ]);
        } else {
            $jumlah = $this->input->post('jumlah');
            $this->cart_model->insert([
                'id_merchant' => $this->session->userdata('id_merchant'),
                'id_product' => $this->input->post('id_product'),
                'jumlah' => $jumlah,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }

        redirect('merchant/catalogs');
    }

    public function view()
    {
        $carts = $this->cart_model->get_cart('carts.id_merchant', $this->session->userdata('id_merchant'));
        $jumlah_harga = 0;
        $jumlah_items = 0;
        $items = [];
        foreach ($carts as $key => $cart) {
            $jumlah_harga += $cart['harga_jual'] * $cart['jumlah'];
            $jumlah_items += $cart['jumlah'];
            $items[] = [
                'nama' => $cart['nama'],
                'harga_satuan' => $cart['harga_jual'],
                'harga' => $cart['harga_jual'] * $cart['jumlah'],
            ];
        }
        echo json_encode(['jumlah_harga' => "Rp " . number_format($jumlah_harga, 0, ',', '.'), 'items' => $items, 'jumlah_items' => $jumlah_items]);
    }

    public function edit()
    {
        if ($this->session->userdata('table') !== 'merchant') {
            show_404();
        } 
        $data = [
            'title' => 'Keranjang',
            'carts' => $this->cart_model->get_cart('carts.id_merchant', $this->session->userdata('id_merchant')),
        ];

        $this->load->view('merchant/cart', $data);
    }

    public function update()
    {
        $cart = $this->cart_model->get_cart('carts.id_cart', $this->input->post('id_cart'))[0];

        $this->cart_model->update($this->input->post('id_cart'), [
            'jumlah' => $this->input->post('jumlah'),
        ]);

        $cart = $this->cart_model->get_cart('carts.id_cart', $this->input->post('id_cart'))[0];
        $total = $cart['harga_jual'] * $cart['jumlah'];

        $carts = $this->cart_model->get_cart('carts.id_merchant', $this->session->userdata('id_merchant'));
        $jumlah_harga = 0;
        $total_item = 0;
        foreach ($carts as $c) {
            $jumlah_harga += $c['harga_jual'] * $c['jumlah'];
            $total_item += $c['jumlah'];
        }

        echo json_encode(['jumlah' => $this->input->post('jumlah'), 'total' => "Rp " . number_format($total, 0, ',', '.'), 'jumlah_harga' => "Rp " . number_format($jumlah_harga, 0, ',', '.'), 'total_item' => $total_item]);
    }

    public function delete_json($id_cart)
    {
        $this->cart_model->delete($id_cart);
    }

    public function delete($id_cart)
    {
        if ($this->session->userdata('table') !== 'merchant') {
            show_404();
        }

        $this->cart_model->delete($id_cart);
        redirect('merchant/catalogs');
    }
}
