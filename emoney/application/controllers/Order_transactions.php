<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Order_transactions extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(["form_validation", 'session', 'encryption']);
        $this->load->model(['order_transaction_model', 'student_model', 'cart_model', 'product_model', 'purchased_item_model']);
        $this->load->helper(['form', 'url']);
        if (!$this->session->userdata('status')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
    }

    public function index()
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $this->db->order_by('order_transactions.created_at', 'desc');
        $transaksi = $this->order_transaction_model->get_order_transactions();

        $data = [
            'title' => 'Order Transaksi',
            'order_transactions' => $transaksi
        ];

        $this->load->view('user/order_transactions/index', $data);
    }
    public function merchant_index()
    {
        if ($this->session->userdata('table') !== 'merchant') {
            show_404();
        }
        $this->db->order_by('order_transactions.created_at', 'desc');
        $transaksi = $this->order_transaction_model->get_order_transaction('order_transactions.id_merchant', $this->session->userdata('id_merchant'));

        $data = [ 
            'title' => 'Order Transaksi',
            'order_transactions' => $transaksi
        ];

        $this->load->view('merchant/order_transactions/index', $data);
    }


    public function create()
    {
        if ($this->session->userdata('table') !== 'merchant') {
            show_404();
        }
        $carts = $this->cart_model->get_cart('carts.id_merchant', $this->session->userdata('id_merchant'));
        $jumlah = 0;
        $total = 0;
        foreach ($carts as $cart) {
            $jumlah += $cart['jumlah'];
            $total += $cart['harga_jual'] * $cart['jumlah'];
        }
        $data = [
            'title' => 'Checkout',
            'jumlah' => $jumlah,
            'carts' => $carts,
            'total_harga' => $total
        ];

        $this->load->view('merchant/order_transactions/create', $data);
    }

    public function store()
    {
        if ($this->session->userdata('table') !== 'merchant') {
            show_404();
        }

        $this->form_validation->set_rules('payment_method', 'Payment Method', 'required|trim');
        if ($this->input->post('payment_method') == 'emoney') {
        
            $this->form_validation->set_rules('rfid', 'RFID', 'required|trim');
            $this->form_validation->set_rules('pin', 'PIN', 'required|trim');
        } else {
            $this->form_validation->set_rules('nama', 'Name', 'required|trim');

        }

        if ($this->form_validation->run() == false) {
            $this->create();
        } else {

            if ($this->input->post('payment_method') == 'emoney') {
                $student = $this->student_model->get_student('rfid', $this->input->post('rfid'))[0];
                if (password_verify($this->input->post('pin'), $student['pin'])) {
                    $student['saldo'] = decryption($student['saldo'], de_iv(), $student['nis']);
                    $sisa_saldo = $student['saldo'] - $this->input->post('total_harga');
                    if ($sisa_saldo >= 0) {
                        
                        $carts = $this->cart_model->get_cart('carts.id_merchant', $this->session->userdata('id_merchant'));
                        $this->student_model->update($student['id_student'], [
                            'saldo' => encryption($sisa_saldo, en_iv(), $student['nis']),
                            'updated_at' => date('Y-m-d H:i:s')
                        ]);
                        $profit = 0;
                        foreach ($carts as $cart) {
                            $profit += ($cart['harga_jual'] - $cart['harga_modal']) * $cart['jumlah'];
                        }

                        $this->order_transaction_model->insert([
                            'id_merchant' => $this->session->userdata('id_merchant'),
                            'nama' => $student['nama'],
                            'saldo_awal' => $student['saldo'],
                            'saldo_akhir' => $sisa_saldo,
                            'harga' => $this->input->post('total_harga'),
                            'profit' => $profit,
                            'metode' => 'emoney',
                            'status' => 'sukses',
                            'created_at' => date('Y-m-d H:i:s')
                        ]);

                        $last_transaction  = $this->order_transaction_model->get_last_order_transaction($this->session->userdata('id_merchant'))[0];
                        foreach ($carts as $item) {
                            $this->product_model->update($item['id_product'], [
                                'stok' => $item['stok'] - $item['jumlah']
                            ]);
                            $this->purchased_item_model->insert([
                                'id_order_transaction' => $last_transaction['id_order_transaction'],
                                'id_product' => $item['id_product'],
                                'harga' => $item['harga_jual'],
                                'jumlah' => $item['jumlah'],
                            ]);
                        }

                        $this->session->set_flashdata('success', 'Transaksi Berhasil Dilakukan');
                        $this->cart_model->delete_where('id_merchant', $this->session->userdata('id_merchant'));
                        redirect('order_transactions/merchant_view/' . $last_transaction['id_order_transaction']);
                    } else {
                        $this->order_transaction_model->insert([
                            'id_merchant' => $this->session->userdata('id_merchant'),
                            'nama' => $student['nama'],
                            'saldo_awal' => $student['saldo'],
                            'saldo_akhir' => $student['saldo'],
                            'harga' => $this->input->post('total_harga'),
                            'profit' => '0',
                            'status' => 'gagal',
                            'metode' => 'emoney',
                            'created_at' => date('Y-m-d H:i:s')
                        ]);

                        $this->session->set_flashdata('error', 'Transaksi Gagal Dilakukan. Saldo Kurang!');
                        redirect('order_transactions/create');
                    }
                } else {
                    $this->session->set_flashdata('error', 'Pin Salah!');
                    redirect('order_transactions/create');
                }
            } else {
                // CASH
                $carts = $this->cart_model->get_cart('carts.id_merchant', $this->session->userdata('id_merchant'));

                $profit = 0;
                foreach ($carts as $cart) {
                    $profit += ($cart['harga_jual'] - $cart['harga_modal']) * $cart['jumlah'];
                }

                $this->order_transaction_model->insert([
                    'id_merchant' => $this->session->userdata('id_merchant'),
                    'nama' => $this->input->post('nama'),
                    'saldo_awal' => '-',
                    'saldo_akhir' => '-',
                    'harga' => $this->input->post('total_harga'),
                    'profit' => $profit,
                    'metode' => 'cash',
                    'status' => 'sukses',
                    'created_at' => date('Y-m-d H:i:s')
                ]);

                $last_transaction  = $this->order_transaction_model->get_last_order_transaction($this->session->userdata('id_merchant'))[0];
                foreach ($carts as $item) {
                    $this->product_model->update($item['id_product'], [
                        'stok' => $item['stok'] - $item['jumlah']
                    ]);
                    $this->purchased_item_model->insert([
                        'id_order_transaction' => $last_transaction['id_order_transaction'],
                        'id_product' => $item['id_product'],
                        'harga' => $item['harga_jual'],
                        'jumlah' => $item['jumlah'],
                    ]);
                }

                $this->session->set_flashdata('success', 'Transaksi Berhasil Dilakukan');
                $this->cart_model->delete_where('id_merchant', $this->session->userdata('id_merchant'));
                redirect('order_transactions/merchant_view/' . $last_transaction['id_order_transaction']);
            }
        }
    }

    public function export_excel()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        if ($this->input->GET('startDate', TRUE) && $this->input->GET('endDate', TRUE)) {
            $this->db->order_by('created_at', 'desc');
            $order_transactions = $this->order_transaction_model->get_by_date(
                $this->input->get('startDate'),
                $this->input->get('endDate')
            );
            $filename = $this->input->GET('startDate', TRUE) . "-" . $this->input->GET('endDate', TRUE);
        } else {
            $this->db->order_by('created_at', 'desc');
            $order_transactions = $this->order_transaction_model->get_order_transactions();
            $filename = date('D-M-Y');
        }


        $sheet->SetCellValue('A1', 'Nama');
        $sheet->SetCellValue('B1', 'Saldo Awal');
        $sheet->SetCellValue('C1', 'Saldo Akhir');
        $sheet->SetCellValue('D1', 'Harga');
        $sheet->SetCellValue('E1', 'Metode Pembayaran');
        $sheet->SetCellValue('F1', 'Status');
        $sheet->SetCellValue('G1', 'Tanggal');
        $sheet->SetCellValue('H1', 'Produk');

        $rowCount = 2;
        foreach ($order_transactions as $transaction) {
            $items = $this->purchased_item_model->get_item('purchased_items.id_order_transaction', $transaction['id_order_transaction']);
            $produk = '';
            foreach ($items as $key => $item) {
                $produk .= $item['nama'] . '(' . $item['jumlah'] * $item['harga'] . ')' . ', ';
            }

            $sheet->SetCellValue('A' . $rowCount, $transaction['nama'])->getColumnDimension('A')->setAutoSize(true);
            $sheet->SetCellValue('B' . $rowCount, $transaction['saldo_awal'])->getColumnDimension('B')->setAutoSize(true);
            $sheet->SetCellValue('C' . $rowCount, $transaction['saldo_akhir'])->getColumnDimension('C')->setAutoSize(true);
            $sheet->SetCellValue('D' . $rowCount, $transaction['profit'])->getColumnDimension('D')->setAutoSize(true);
            $sheet->SetCellValue('E' . $rowCount, $transaction['metode'])->getColumnDimension('E')->setAutoSize(true);
            $sheet->SetCellValue('F' . $rowCount, $transaction['status'])->getColumnDimension('F')->setAutoSize(true);
            $sheet->SetCellValue('G' . $rowCount, $transaction['created_at'])->getColumnDimension('G')->setAutoSize(true);
            $sheet->SetCellValue('H' . $rowCount, $produk)->getColumnDimension('H')->setAutoSize(true);

            $rowCount++;
        }


        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output'); // download file

    }

    public function merchant_view($id_order_transaction)
    {

        if ($this->session->userdata('table') !== 'merchant') {
            show_404();
        }

        $data = [
            'title' => 'Order Transaksi',
            'order_transaction' => $this->order_transaction_model->get_order_transaction('id_order_transaction', $id_order_transaction)[0],
            'items' => $this->purchased_item_model->get_item('purchased_items.id_order_transaction', $id_order_transaction)
        ];


        $this->load->view('merchant/order_transactions/view', $data);
    }

    public function view($id_order_transaction)
    {

        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }

        $data = [
            'title' => 'Order Transaksi',
            'order_transaction' => $this->order_transaction_model->get_order_transaction('id_order_transaction', $id_order_transaction)[0],
            'items' => $this->purchased_item_model->get_item('purchased_items.id_order_transaction', $id_order_transaction)
        ];
        $this->load->view('user/order_transactions/view', $data);
    }

    public function edit($id_order_transaction)
    {
    }

    public function update()
    {
    }

    public function delete($id_order_transaction)
    {
    }

    public function students($id_order_transaction)
    {
    }

    public function group_by($key, $data)
    {
        $result = [];
        foreach ($data as $val) {
            if (array_key_exists($key, $val)) {
                $result[$val][$key];
            } else {
                $result[''][] = $val;
            }
        }
        return $result;
    }

    public function invoice($id_order_transaction)
    {

        $data = [
            'order_transactions' => $this->order_transaction_model->get_order_transaction('id_order_transaction', $id_order_transaction)[0],
            'items' => $this->purchased_item_model->get_item('purchased_items.id_order_transaction', $id_order_transaction)
        ];

        $mpdf = new \Mpdf\Mpdf();
        $html = $this->load->view('invoice_pdf', $data, true);
        $mpdf->WriteHTML($html);
        $mpdf->Output('INV- ' . $data['order_transactions']['created_at'] . '.pdf', 'D');
    }

    public function merchant_export_excel()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        // set Header
        $sheet->SetCellValue('A1', 'Nama');
        $sheet->SetCellValue('B1', 'Saldo Awal');
        $sheet->SetCellValue('C1', 'Saldo Akhir');
        $sheet->SetCellValue('D1', 'Profit');
        $sheet->SetCellValue('E1', 'Status');
        $sheet->SetCellValue('F1', 'Metode');
        $sheet->SetCellValue('G1', 'Tanggal');
        $sheet->SetCellValue('H1', 'Produk');
        // set Row
        $this->db->order_by('order_transactions.created_at', 'desc');

        $transactions = $this->order_transaction_model->get_order_transaction('order_transactions.id_merchant', $this->session->userdata('id_merchant'));
        $rowCount = 2;
        foreach ($transactions as $transaction) {
            $items = $this->purchased_item_model->get_item('purchased_items.id_order_transaction', $transaction['id_order_transaction']);
            $produk = '';
            foreach ($items as $key => $item) {
                $produk .= $item['nama'] . '(' . $item['jumlah'] * $item['harga'] . ')' . ', ';
            }

            $sheet->SetCellValue('A' . $rowCount, $transaction['nama'])->getColumnDimension('A')->setAutoSize(true);
            $sheet->SetCellValue('B' . $rowCount, $transaction['saldo_awal'])->getColumnDimension('B')->setAutoSize(true);
            $sheet->SetCellValue('C' . $rowCount, $transaction['saldo_akhir'])->getColumnDimension('C')->setAutoSize(true);
            $sheet->SetCellValue('D' . $rowCount, $transaction['profit'])->getColumnDimension('D')->setAutoSize(true);
            $sheet->SetCellValue('E' . $rowCount, $transaction['status'])->getColumnDimension('E')->setAutoSize(true);
            $sheet->SetCellValue('F' . $rowCount, $transaction['metode'])->getColumnDimension('F')->setAutoSize(true);
            $sheet->SetCellValue('G' . $rowCount, $transaction['created_at'])->getColumnDimension('G')->setAutoSize(true);
            $sheet->SetCellValue('H' . $rowCount, $produk)->getColumnDimension('H')->setAutoSize(true);
            $rowCount++;
        }

        $writer = new Xlsx($spreadsheet);

        $filename = $this->session->userdata('name') . '-' . date('D-M-Y');

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output'); // download file 

    }
}
