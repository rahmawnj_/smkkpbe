<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Products extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("form_validation");
        $this->load->model(['product_model', 'category_model', 'merchant_model', 'cart_model']);
        $this->load->helper(['form', 'url']);
        if (!$this->session->userdata('status')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
    }

    public function index()
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $data = [
            'title' => 'Produk',
            'products' => $this->product_model->get_products()
        ];
    
        $this->load->view('user/products/index', $data);
    }

    public function create()
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $data = [
            'title' => 'Produk',
            'categories' => $this->category_model->get_categories(),
            'merchants' => $this->merchant_model->get_merchants(),
        ];

        $this->load->view('user/products/create', $data);
    }

    public function store()
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $this->form_validation->set_rules('category', 'Category', 'required|trim');
        $this->form_validation->set_rules('merchant', 'Merchant', 'required|trim');
        $this->form_validation->set_rules('nama', 'Name', 'required|trim');
        $this->form_validation->set_rules('stok', 'Stock', 'required|trim');
        $this->form_validation->set_rules('harga_modal', 'Capital Price', 'required|trim');
        $this->form_validation->set_rules('harga_jual', 'Selling Price', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->create();
        } else {

            $config['upload_path']          = '../assets/img/uploads/products';
            $config['allowed_types']        = 'gif|jpg|png|jpeg|jfif';
            $config['max_size']             = 10000;
            $config['max_width']            = 10000;
            $config['max_height']           = 10000;

            $this->load->library('upload', $config);
            $this->upload->do_upload('gambar');

            $this->product_model->insert([
                'id_category' => $this->input->post('category'),
                'id_merchant' => $this->input->post('merchant'),
                'nama' => $this->input->post('nama'),
                'stok' => $this->input->post('stok'),
                'deleted' => 0,
                'harga_modal' => $this->input->post('harga_modal'),
                'harga_jual' => $this->input->post('harga_jual'),
                'gambar' => 'products/' . $this->upload->data('file_name'),
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $this->session->set_flashdata('success', 'Produk Berhasil Ditambahkan!');
            redirect('dashboard/products');
        }
    }

    public function view($id_product)
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $data = [
            'title' => 'Produk',
            'product' => $this->product_model->get_product('id_product', $id_product)[0]
        ];

        $this->load->view('user/products/view', $data);
    }

    public function edit($id_product)
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $data = [
            'title' => 'Produk',
            'categories' => $this->category_model->get_categories(),
            'product' => $this->product_model->get_product('id_product', $id_product)[0],
            'merchants' => $this->merchant_model->get_merchants(),

        ];

        $this->load->view('user/products/edit', $data);
    }

    public function update()
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $product = $this->product_model->get_product('id_product', $this->input->post('id_product'))[0];
        $this->form_validation->set_rules('merchant', 'Merchant', 'required|trim');
        $this->form_validation->set_rules('category', 'Category', 'required|trim');
        $this->form_validation->set_rules('nama', 'Name', 'required|trim');
        $this->form_validation->set_rules('stok', 'Stock', 'required|trim');
        $this->form_validation->set_rules('harga_modal', 'Capital Price', 'required|trim');
        $this->form_validation->set_rules('harga_jual', 'Selling Price', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->edit($this->input->post('id_product'));
        } else {
            $config['upload_path']          = '../assets/img/uploads/products';
            $config['allowed_types']        = 'gif|jpg|png|jpeg|jfif';
            $config['max_size']             = 10000;
            $config['max_width']            = 10000;
            $config['max_height']           = 10000;

            $this->load->library('upload', $config);
            $this->upload->do_upload('gambar');

            if ($this->upload->data('file_name') == '') {
                $gambar = $product['gambar'];
            } else {
                unlink('../assets/img/uploads/' . $product['gambar']);
                $gambar = 'products/' . $this->upload->data('file_name');
            }

            $this->product_model->update($this->input->post('id_product'), [
                'id_category' => $this->input->post('category'),
                'id_merchant' => $this->input->post('merchant'),
                'nama' => $this->input->post('nama'),
                'stok' => $this->input->post('stok'),
                'harga_modal' => $this->input->post('harga_modal'),
                'harga_jual' => $this->input->post('harga_jual'),
                'gambar' => $gambar,
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            $this->session->set_flashdata('success', 'Produk Berhasil Diperbarui!');
            redirect('dashboard/products');
        }
    }

    public function delete($id_product)
    {
        if ($this->session->userdata('role') == 'administrator') {
            show_404();
        }
        $this->product_model->update($id_product, [
            'deleted' => 1
        ]);

        $this->session->set_flashdata('success', 'Produk Berhasil Dihapus!');
        redirect('dashboard/products');
    }

    public function merchant_delete($id_product)
    {
        if ($this->session->userdata('role') !== 'owner') {
            show_404();
        }

        $this->product_model->update($id_product, [
            'deleted' => 1,
        ]);
        $cart = $this->cart_model->get_cart('carts.id_product', $id_product)[0];
        $this->cart_model->delete($cart['id_cart']);
        $this->session->set_flashdata('success', 'Produk Berhasil Dihapus!');
        redirect('merchant/products');
    }


    public function merchant_index()
    {
        if ($this->session->userdata('role') !== 'owner') {
            show_404();
        }
        $data = [
            'title' => 'Produk',
            'products' => $this->product_model->get_product('products.id_merchant', $this->session->userdata('id_merchant'))
        ];
      
        $this->load->view('merchant/products/index', $data);
    }

    public function merchant_create()
    {
        if ($this->session->userdata('role') !== 'owner') {
            show_404();
        }
        $data = [
            'title' => 'Produk',
            'categories' => $this->category_model->get_categories(),
        ];

        $this->load->view('merchant/products/create', $data);
    }

    public function merchant_store()
    {

        if ($this->session->userdata('role') !== 'owner') {
            show_404();
        }

        $this->form_validation->set_rules('category', 'Category', 'required|trim');
        $this->form_validation->set_rules('nama', 'Name', 'required|trim');
        $this->form_validation->set_rules('stok', 'Stock', 'required|trim');
        $this->form_validation->set_rules('harga_modal', 'Capital Price', 'required|trim');
        $this->form_validation->set_rules('harga_jual', 'Selling Price', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->merchant_create();
        } else {

            $config['upload_path']          = '../assets/img/uploads/products';
            $config['allowed_types']        = 'gif|jpg|png|jpeg|jfif';
            $config['max_size']             = 10000;
            $config['max_width']            = 10000;
            $config['max_height']           = 10000;

            $this->load->library('upload', $config);
            $this->upload->do_upload('gambar');

            $this->product_model->insert([
                'id_category' => $this->input->post('category'),
                'id_merchant' => $this->session->userdata('id_merchant'),
                'nama' => $this->input->post('nama'),
                'stok' => $this->input->post('stok'),
                'deleted' => 0,
                'harga_modal' => $this->input->post('harga_modal'),
                'harga_jual' => $this->input->post('harga_jual'),
                'gambar' => 'products/' . $this->upload->data('file_name'),
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $this->session->set_flashdata('success', 'Produk Berhasil Ditambahkan!');
            redirect('merchant/products');
        }
    }

    public function merchant_view($id_product)
    {
        if ($this->session->userdata('role') !== 'owner') {
            show_404();
        }
        $data = [
            'title' => 'Produk',
            'product' => $this->product_model->get_product('id_product', $id_product)[0]
        ];

        $this->load->view('merchant/products/view', $data);
    }

    public function merchant_edit($id_product)
    {
        if ($this->session->userdata('role') !== 'owner') {
            show_404();
        }
        $data = [
            'title' => 'Produk',
            'categories' => $this->category_model->get_categories(),
            'product' => $this->product_model->get_product('id_product', $id_product)[0],
        ];
        $this->load->view('merchant/products/edit', $data);
    }

    public function merchant_update()
    {
        if ($this->session->userdata('role') !== 'owner') {
            show_404();
        }
        $product = $this->product_model->get_product('id_product', $this->input->post('id_product'))[0];
        $this->form_validation->set_rules('category', 'Category', 'required|trim');
        $this->form_validation->set_rules('nama', 'Name', 'required|trim');
        $this->form_validation->set_rules('stok', 'Stock', 'required|trim');
        $this->form_validation->set_rules('harga_modal', 'Capital Price', 'required|trim');
        $this->form_validation->set_rules('harga_jual', 'Selling Price', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->edit($this->input->post('id_product'));
        } else {
            $config['upload_path']          = '../assets/img/uploads/products';
            $config['allowed_types']        = 'gif|jpg|png|jpeg|jfif';
            $config['max_size']             = 10000;
            $config['max_width']            = 10000;
            $config['max_height']           = 10000;

            $this->load->library('upload', $config);
            $this->upload->do_upload('gambar');

            if ($this->upload->data('file_name') == '') {
                $gambar = $product['gambar'];
            } else {
                unlink('../assets/img/uploads/' . $product['gambar']);
                $gambar = 'products/' . $this->upload->data('file_name');
            }

            $this->product_model->update($this->input->post('id_product'), [
                'id_category' => $this->input->post('category'),
                'id_merchant' => $this->session->userdata('id_merchant'),
                'nama' => $this->input->post('nama'),
                'stok' => $this->input->post('stok'),
                'harga_modal' => $this->input->post('harga_modal'),
                'harga_jual' => $this->input->post('harga_jual'),
                'gambar' => $gambar,
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            $this->session->set_flashdata('success', 'Produk Berhasil Diperbarui!');
            redirect('merchant/products');
        }
    }

    public function catalogs()
    {
        if ($this->session->userdata('role') == 'administrator') {
            show_404();
        } else if ($this->session->userdata('role') == 'top_up'){
            show_404();
        }
        $data = [
            'title' => 'Katalog',
            'products' => $this->product_model->get_product('products.id_merchant', $this->session->userdata('id_merchant'))
        ];

        $this->load->view('merchant/catalogs/index', $data);
    }

    public function detail($id_product)
    {
        if ($this->session->userdata('role') !== 'administrator') {
            show_404();
        }
        $data = [
            'title' => 'Produk',
            'product' => $this->product_model->get_product('id_product', $id_product)
        ];

        $this->load->view('dashboard/catalogs/detail', $data);
    }
}
