<?php
class Top_up_model extends CI_Model
{
    public function get_top_ups()
    {
        $this->db->select('top_ups.*, students.nama as student_nama, students.nis as student_nis, students.foto as student_foto, students.rfid as student_rfid, users.nama as user_nama');
        
        $this->db->join('users', 'users.id_user = top_ups.id_user');
        $this->db->join('students', 'students.id_student = top_ups.id_student');

        return $this->db->get('top_ups')->result_array();
    }

    public function get_top_up($id_top_up = null)
    {
        $this->db->select('top_ups.*, students.nama as student_nama, students.nis as student_nis, students.foto as student_foto, students.rfid as student_rfid, users.nama as user_nama');
        $this->db->join('users', 'users.id_user = top_ups.id_user');
        $this->db->join('students', 'students.id_student = top_ups.id_student');
        
        return $this->db->get_where('top_ups', array('id_top_up' => $id_top_up))->result_array();
    }

    public function insert(...$data)
    {
        $this->db->insert('top_ups', $data[0]);
    }

}
