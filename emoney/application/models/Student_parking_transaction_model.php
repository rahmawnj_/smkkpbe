<?php
class Student_parking_transaction_model extends CI_Model
{
    public function get_student_parking_transactions()
    {
        $this->db->select('student_parking_transactions.*, students.nama as student_nama, students.nis as student_nis, students.foto as student_foto, students.rfid as student_rfid, devices.device as device_nama, devices.harga as device_harga ');
        $this->db->join('devices', 'devices.id_device = student_parking_transactions.id_device');
        $this->db->join('students', 'students.id_student = student_parking_transactions.id_student');

        return $this->db->get('student_parking_transactions')->result_array();
    }

    public function get_student_parking_transaction($field, $where)
    {
        $this->db->select('student_parking_transactions.*, students.nama as student_nama, students.nis as student_nis, students.foto as student_foto, students.rfid as student_rfid, devices.*');
        $this->db->join('devices', 'devices.id_device = student_parking_transactions.id_device');
        $this->db->join('students', 'students.id_student = student_parking_transactions.id_student');
        return $this->db->get_where('student_parking_transactions', [$field => $where])->result_array();
    }

    public function get_ptr($id_device)
    {
        $this->db->order_by('created_at', 'asc');
        return $this->db->get_where('student_parking_transactions', ['student_parking_transactions.id_device' => $id_device])->result_array();
    }

    public function get_by_date($startDate = null, $endDate = null)
    {
        if ($startDate && $endDate) {
            
            $this->db->where('student_parking_transactions.created_at >=', date('Y-m-d', strtotime($startDate)));
            $this->db->where('student_parking_transactions.created_at <=', date('Y-m-d', strtotime($endDate)));
            $this->db->select('student_parking_transactions.*, devices.device as device, students.nama as student_nama, students.nis as student_nis');
            $this->db->join('devices', 'devices.id_device = student_parking_transactions.id_device');
            $this->db->join('students', 'students.id_student = student_parking_transactions.id_student');
            return $this->db->get('student_parking_transactions')->result_array();
        } else {
            $this->db->select('student_parking_transactions.*, devices.device');
            $this->db->from('student_parking_transactions');
            $this->db->join('devices', 'devices.id_device = devices.id_device');
            return $this->db->get()->result_array();
        }
    }

    public function avenueParking($startDate, $endDate)
    {
        return $this->db->query(
            "SELECT sum(harga) as jmlh_harga, date(created_at) as tanggal, count(date(created_at)) as jmlh_transaksi
            from student_parking_transactions
            WHERE status = 'sukses'
            && date(created_at) >= '$startDate' && date(created_at) < '$endDate' 
            GROUP BY date(created_at)
            ORDER BY date(created_at) ASC"
            )->result_array();
    }

    public function insert(...$data)
    {
        $this->db->insert('student_parking_transactions', $data[0]);
    }

    public function get_last_student_parking_transaction($id_device)
    {
        $this->db->order_by('created_at', 'desc');
        return $this->db->get_where('student_parking_transactions', ['id_device' => $id_device])->result_array();
    }
}
