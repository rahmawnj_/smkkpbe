<?php
class Class_model extends CI_Model
{
    public function get_classes()
    {
        $this->db->where(['deleted' => 0, 'id_class !=' => 0]);
        return $this->db->get('classes')->result_array();
    }

    public function get_class($field, $id_class)
    {
        return $this->db->get_where('classes', array($field => $id_class))->result_array();
    }

    public function insert(...$data)
    {
        $this->db->insert('classes', $data[0]);
    }

    public function update($id_class, ...$data)
    {
        $this->db->update('classes', $data[0], ['id_class' => $id_class]);
    }

    public function delete($id_class)
    {
        // $this->db->where('id_class', $id_class);
        // $this->db->delete('classes');

        $this->db->update('classes', ['deleted' => 1], ['id_class' => $id_class]);

    }
}
