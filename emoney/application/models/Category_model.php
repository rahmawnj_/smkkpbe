<?php
class Category_model extends CI_Model
{
    public function get_categories()
    {
        return $this->db->get('categories')->result_array();
    }

    public function get_category($field, $id_category)
    {
        return $this->db->get_where('categories', array($field => $id_category))->result_array();
    }

    public function insert(...$data)
    {
        $this->db->insert('categories', $data[0]);
    }

    public function update($id_category, ...$data)
    {
        $this->db->update('categories', $data[0], ['id_category' => $id_category]);
    }

    public function delete($id_category)
    {
        $this->db->where('id_category', $id_category);
        $this->db->delete('categories');
    }
}
