<?php
class Merchant_account_model extends CI_Model
{
    public function get_merchant_accounts()
    {
        $this->db->where(['merchant_accounts.deleted' => 0]);
        $this->db->join('merchants', 'merchants.id_merchant = merchant_accounts.id_merchant');
        return $this->db->get('merchant_accounts')->result_array();
    }

    public function get_merchant_account($field, $data)
    {

        $this->db->where(['merchant_accounts.deleted' => 0]);
        $this->db->join('merchants', 'merchants.id_merchant = merchant_accounts.id_merchant');
        return $this->db->get_where('merchant_accounts', [$field => $data])->result_array();
    }

    public function get_merchant_account2($field, $data, $field2 = null, $data2  = null)
    {

        $this->db->where(['merchant_accounts.deleted' => 0]);
        $this->db->join('merchants', 'merchants.id_merchant = merchant_accounts.id_merchant');
        return $this->db->get_where('merchant_accounts', [$field => $data, $field2 => $data2])->result_array();
    }



    public function insert(...$data)
    {
        $this->db->insert('merchant_accounts', $data[0]);
    }

    public function update($id_merchant_account, ...$data)
    {
        $this->db->update('merchant_accounts', $data[0], ['id_merchant_account' => $id_merchant_account]);
    }

    public function delete($id_merchant_account)
    {

        $this->db->update('merchant_accounts', ['deleted' => 1, 'username' => '', 'password' => ''], ['id_merchant_account' => $id_merchant_account]);
    }
}
