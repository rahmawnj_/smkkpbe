<?php
class Cart_model extends CI_Model
{
    public function get_carts()
    {
        $this->db->join('merchants', 'merchants.id_merchant = carts.id_merchant');
        $this->db->join('products', 'products.id_product = carts.id_product');
        return $this->db->get('carts')->result_array();
    }

    public function get_cart($fields, $data)
    {
        $this->db->join('merchants', 'merchants.id_merchant = carts.id_merchant');
        $this->db->join('products', 'products.id_product = carts.id_product');
        return $this->db->order_by('carts.updated_at', 'desc')->get_where('carts', [$fields => $data])->result_array();
    }

    public function insert(...$data)
    {
        $this->db->insert('carts', $data[0]);
    }

    public function update($id_cart, ...$data)
    {
        $this->db->update('carts', $data[0], ['id_cart' => $id_cart]);
    }

    public function delete($id_cart)
    {
        $this->db->where('id_cart', $id_cart);
        $this->db->delete('carts');
    }

    public function delete_where($field, $id_cart)
    {
        $this->db->where($field, $id_cart);
        $this->db->delete('carts');
    }

    public function search_carts($filter)
    {
      
        return $this->db->get('carts')->result_array();

    }
}

