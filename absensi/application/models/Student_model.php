<?php
class Student_model extends CI_Model
{
    private $_batchImport;

    public function setBatchImport($batchImport)
    {
        $this->_batchImport = $batchImport;
    }

    // save data
    public function importData()
    {
        $data = $this->_batchImport;
        $this->db->insert_batch('students', $data);
    }
    // get employee list
   

    public function get_students()
    {
        $this->db->join('classes', 'classes.id_class = students.id_class');
        $this->db->where(['students.deleted' => 0]);
        return $this->db->get('students')->result_array();
    }

    public function get_student($field, $data)
    {
        $this->db->join('classes', 'classes.id_class = students.id_class');
        $this->db->where(['students.deleted' => 0]);
        return $this->db->get_where('students', [$field => $data])->result_array();
    }

    public function insert(...$data)
    {
        $this->db->insert('students', $data[0]);
    }

    public function update($id_student, ...$data)
    {
        $this->db->update('students', $data[0], ['id_student' => $id_student]);
    }

    public function delete($id_student)
    {
        // $this->db->where('id_student', $id_student);
        // $this->db->delete('students');
        $this->db->update('students', ['deleted' => 1, 'nis' => '', 'rfid' => ''], ['id_student' => $id_student]);
    }

    public function search_student($filter)
    {
        $this->db->where('nama', $filter)->where('deleted', 0);
        $this->db->or_where('nis', $filter)->where('deleted', 0);
        $this->db->or_where('rfid', $filter)->where('deleted', 0);
        return $this->db->get('students')->result_array();
    }
}
