<?php
class Attandance_model extends CI_Model
{
    public function get_attandances()
    {
        $this->db->where(['deleted' => 0, 'id_attandance !=' => 0]);        
        $this->db->join('students', 'students.id_student = students.id_student');
        $this->db->select('attandances.*, students.nama as student_nama');
        return $this->db->get('attandances')->result_array();
    }

    public function get_attandace($field, $id_attandance)
    {
        $this->db->join('students', 'students.id_student = students.id_student');
        return $this->db->get_where('attandances', array($field => $id_attandance))->result_array();
    }
    public function get_attandace2($field, $id_attandance, $field2, $id_attandance2)
    {
        $this->db->join('students', 'students.id_student = students.id_student');
        return $this->db->get_where('attandances', array($field => $id_attandance, $field2 => $id_attandance2))->result_array();
    }

    public function insert(...$data)
    {
        $this->db->insert('attandances', $data[0]);
    }

    public function update($id_attandance, ...$data)
    {
        $this->db->update('attandances', $data[0], ['id_attandance' => $id_attandance]);
    }

    public function delete($id_attandance)
    {
        // $this->db->where('id_attandance', $id_attandance);
        // $this->db->delete('attandances');

        $this->db->update('attandances', ['deleted' => 1], ['id_attandance' => $id_attandance]);

    }
}
