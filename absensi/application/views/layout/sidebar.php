<aside id="sidebar" class="sidebar js-sidebar">
	<div class="sidebar-content js-simplebar">
		<a class="sidebar-brand" href="index.html">
			<span class="sidebar-brand-text align-middle">
				Absensi
			</span>
		</a>

		<div class="sidebar-user">
			<div class="d-flex justify-content-center">
				<div class="flex-shrink-0">
					<img src="<?= base_url('assets/img/uploads/' . $this->session->userdata('foto')) ?? '' ?>" class="avatar img-fluid rounded me-1" alt="<?= $this->session->userdata('name') ?>">
				</div>
				<div class="flex-grow-1 ps-2">
				<div class="sidebar-user-subtitle"><?= $this->session->userdata('name') ?></div>
					<div class="sidebar-user-subtitle"><?= $this->session->userdata('role') ?></div>
				</div>
			</div>
		</div>

		<ul class="sidebar-nav">

			<?php if ($this->session->userdata('role') == 'admin_absensi') : ?>
				<li class="sidebar-header">
					Data Master
				</li>
			<?php endif ?>

			<?php if ($this->session->userdata('role') == 'admin_absensi') : ?>
				<li class="sidebar-item <?= ($title == 'Users') ? 'active' : ''; ?>">
					<a class="sidebar-link " href="<?= base_url('/dashboard/users') ?>">
						<i class="align-middle" data-feather="user"></i> <span class="align-middle">User</span>
					</a>
				</li>
			<?php endif ?>

			<?php if ($this->session->userdata('role') == 'admin_absensi') : ?>
				<li class="sidebar-item <?= ($title == 'Roles') ? 'active' : ''; ?>">
					<a class="sidebar-link " href="<?= base_url('/dashboard/roles') ?>">
						<i class="align-middle" data-feather="target"></i> <span class="align-middle">Role</span>
					</a>
				</li>
			<?php endif ?>

		
			<?php if ($this->session->userdata('role') == 'admin_absensi') : ?>
				<li class="sidebar-item <?= ($title == 'Siswa') ? 'active' : ''; ?>">
					<a class="sidebar-link" href="<?= base_url('/dashboard/students') ?>">
						<i class="align-middle" data-feather="users"></i> <span class="align-middle">Siswa</span>
					</a>
				</li>
			<?php endif ?>

			<?php if ($this->session->userdata('role') == 'admin_absensi') : ?>
				<li class="sidebar-item <?= ($title == 'Kelas') ? 'active' : ''; ?>">
					<a class="sidebar-link" href="<?= base_url('/dashboard/classes') ?>">
						<i class="align-middle" data-feather="bookmark"></i> <span class="align-middle">Kelas</span>
					</a>
				</li>
			<?php endif ?>


			<?php if ($this->session->userdata('role') == 'admin_absensi') : ?>
				<li class="sidebar-item <?= ($title == 'Devices Absensi') ? 'active' : ''; ?>">
					<a class="sidebar-link " href="<?= base_url('/dashboard/attandance_devices
					') ?>">
						<i class="align-middle" data-feather="speaker"></i> <span class="align-middle">Devices</span>
					</a>
				</li>
			<?php endif ?>

			<?php if ($this->session->userdata('role') == 'admin_absensi') : ?>
				<li class="sidebar-header">
					Laporan Absensi
				</li>
			<?php endif ?>

			<?php if ($this->session->userdata('role') == 'admin_absensi') : ?>
				<li class="sidebar-item <?= ($title == 'Laporan Absen Siswa') ? 'active' : ''; ?>">
					<a data-bs-target="#multi" data-bs-toggle="collapse" class="sidebar-link collapsed">
						<i class="align-middle" data-feather="credit-card"></i> <span class="align-middle">Data Absensi</span>
					</a>
					<ul id="multi" class="sidebar-dropdown list-unstyled collapse" data-bs-parent="#sidebar">
						<li class="sidebar-item">
							<ul id="multi-1" class="sidebar-dropdown list-unstyled collapse">
								<li class="sidebar-item">
									<a class="sidebar-link <?= ($title == 'Laporan Absen Siswa') ? 'active' : ''; ?>" href="<?= base_url('dashboard/student_parking_transactions') ?>">Siswa</a>
								</li>
							</ul>
						</li>
						<li class="sidebar-item  <?= ($title == 'Order Transaksi') ? 'active' : ''; ?>"><a class="sidebar-link" href="<?= base_url('dashboard/attandances') ?>">Absensi Siswa</a></li>
					</ul>
				</li>
			<?php endif ?>

			<?php if ($this->session->userdata('role') == 'admin_absensi') : ?>
				<li class="sidebar-header">
					Pengaturan
				</li>
			<?php endif ?>
			<?php if ($this->session->userdata('role') == 'admin_absensi') : ?>
				<li class="sidebar-item <?= ($title == 'Pengaturan') ? 'active' : ''; ?>">
					<a class="sidebar-link " href="<?= base_url('/settings
					') ?>">
						<i class="align-middle" data-feather="speaker"></i> <span class="align-middle">Pengaturan</span>
					</a>
				</li>
			<?php endif ?>
		</ul>

	</div>
</aside>