<?php $this->load->view('layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('layout/header') ?>

        <main class="content">
            <div class="container-fluid p-0">
                <div class="row removable">
                    <div class="col-lg-12">
                        <div class="card flex-fill">
                            <div class="card-header d-flex justify-content-between">
                                <h5 class="card-title mb-0"><?= $title ?></h5>
                                <a href="<?= base_url('dashboard/users/create') ?>" class="btn btn-primary float-right fas fa-plus"></a>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-hover my-0" id="table" style="width:100%">
                                        <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Foto</th>
                                                <th>Nama</th>
                                                <th>Username</th>
                                                <th>Role</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 0;
                                            foreach ($users as $user) : ?>
                                                <tr>
                                                    <td><?= ++$no; ?></td>
                                                    <td>
                                                        <img src="<?= base_url('../assets/img/uploads/' . $user['foto']) ?>" class="img-fluid img-thumbnail mx-auto d-block text-center overflow-hidden" style="height:100px; width:100px" alt="">

                                                    </td>
                                                    <td><?= $user['nama'] ?></td>
                                                    <td><?= $user['username'] ?></td>
                                                    <td>
                                                        <span <?php if ($user['role'] == 'administrator') {
                                                                    echo 'class="badge bg-success"';
                                                                } else {
                                                                    echo 'class="badge bg-info"';
                                                                } ?>><?= $user['role'] ?>
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <a class="fa btn-sm fa-edit btn bg-warning text-white" href="<?= base_url('dashboard/users/edit/' . $user['id_user']) ?>"></a>
                                                        <a class="fas fa-eye btn-sm btn bg-info text-white" href="<?= base_url('dashboard/users/' . $user['id_user']) ?>"></a>
                                                        <a id="delete-button" class="fas btn-sm fa-trash btn bg-danger text-white" href="<?= base_url('users/delete/' . $user['id_user']) ?>"></a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <?php $this->load->view('layout/footer') ?>
    </div>
</div>


<?php $this->load->view('layout/foot') ?>