<?php $this->load->view('layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('layout/header') ?>

        <main class="content">
            <div class="container-fluid p-0">
                <div class="row removable">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title mb-0"><?= $title ?></h5>
                            </div>
                            <div class="card-body">
                                <?php echo validation_errors(); ?>

                                <?= $this->session->flashdata('message'); ?>

                                <?php echo form_open_multipart('students/update'); ?>
                                <input type="hidden" class="form-control" name="id_student" value="<?= $student['id_student'] ?>">

                                <div class="img-preview d-flex">
                                    <img src="<?= base_url('../assets/img/uploads/' . $student['foto']) ?>" id="gmbr" class="img-fluid img-thumbnail mx-auto d-block text-center" style="height:200px;width:200px;" alt="">
                                </div>
                                <div class="form-group">
                                    <label for="foto">Foto</label>
                                    <input type="file" id="foto" class="form-control" size="20" name="foto" id="foto" aria-describedby="foto" placeholder="Masukan foto">
                                    <span class="text-danger">
                                        <?= form_error('foto') ?>
                                    </span>
                                </div>

                                <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <input autocomplete="off" type="text" class="form-control" name="nama" value="<?= $student['nama'] ?>" id="nama" aria-describedby="nama" placeholder="Masukan Nama">
                                    <span class="text-danger">
                                        <?= form_error('nama') ?>
                                    </span>
                                </div>

                                <div class="form-group">
                                    <label for="nis">NIS</label>
                                    <input autocomplete="off" type="number" class="form-control" value="<?= $student['nis'] ?>" name="nis" id="nis" aria-describedby="nis" placeholder="Masukan NIS" required>
                                    <span class="text-danger">
                                        <?= form_error('nis') ?>
                                    </span>
                                </div>

                                <div class="form-group">
                                    <label for="rfid">RFID</label>
                                    <input autocomplete="off" type="text" class="form-control" name="rfid" value="<?= $student['rfid'] ?>" id="rfid" aria-describedby="rfid" placeholder="Masukan RFID" required>
                                    <span class="text-danger">
                                        <?= form_error('rfid') ?>
                                    </span>
                                </div>

                                <div class="form-group">
                                    <label for="tempat_lahir">Tempat Lahir</label>
                                    <input autocomplete="off" type="text" class="form-control" value="<?= $student['tempat_lahir'] ?>" name="tempat_lahir" id="tempat_lahir" aria-describedby="tempat_lahir" placeholder="Masukan Tempat Lahir" required>
                                    <span class="text-danger">
                                        <?= form_error('tempat_lahir') ?>
                                    </span>
                                </div>

                                <div class="form-group">
                                    <label for="tanggal_lahir">Tanggal Lahir</label>
                                    <input autocomplete="off" type="date" class="form-control" value="<?= $student['tanggal_lahir'] ?>" name="tanggal_lahir" id="tanggal_lahir" aria-describedby="tanggal_lahir" placeholder="Masukan Tanggal Lahir" required>
                                    <span class="text-danger">
                                        <?= form_error('tanggal_lahir') ?>
                                    </span>
                                </div>

                                <div class="form-group">
                                    <label for="alamat">Alamat</label>
                                    <input autocomplete="off" type="text" class="form-control" value="<?= $student['alamat'] ?>" name="alamat" id="alamat" aria-describedby="alamat" placeholder="Masukan Alamat" required>
                                    <span class="text-danger">
                                        <?= form_error('alamat') ?>
                                    </span>
                                </div>

                                <div class="form-group">
                                    <label for="no_hp">No. HP</label>
                                    <input autocomplete="off" type="number" class="form-control" value="<?= $student['no_hp'] ?>" name="no_hp" id="no_hp" aria-describedby="no_hp" placeholder="Masukan No HP" required>
                                    <span class="text-danger">
                                        <?= form_error('no_hp') ?>
                                    </span>
                                </div>

                                <div class="form-group">
                                    <label for="class">Kelas</label>
                                    <select name="class" class="form-control" id="class" required>
                                        <option selected disabled>-- Pilih Kelas --</option>
                                        <?php foreach ($classes as $class) : ?>
                                            <option <?= ($student['id_class'] == $class['id_class'] ? "selected" : "") ?> value="<?= $class['id_class'] ?>"><?= $class['kelas'] ?></option>
                                        <?php endforeach ?>
                                    </select>
                                    <span class="text-danger">
                                        <?= form_error('class') ?>
                                    </span>
                                </div>

                                <div class="form-group">
                                    <label for="jenis_kelamin">Jenis Kelamin</label>
                                    <select name="jenis_kelamin" class="form-control" id="jenis_kelamin" required>
                                        <option selected disabled>-- Pilih Jenis Kelamin --</option>
                                        <option <?= ($student['jenis_kelamin'] == "Laki-laki" ? "selected" : "") ?> value="Laki-laki">Laki - laki</option>
                                        <option <?= ($student['jenis_kelamin'] == "Perempuan" ? "selected" : "") ?> value="Perempuan">Perempuan</option>
                                    </select>
                                    <span class="text-danger">
                                        <?= form_error('jenis_kelamin') ?>
                                    </span>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                                <?= form_close() ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <?php $this->load->view('layout/footer') ?>
    </div>
</div>

<?php $this->load->view('layout/foot') ?>