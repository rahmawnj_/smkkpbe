<?php $this->load->view('layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('layout/header') ?>

        <main class="content">
            <div class="container-fluid p-0">
                <div class="row removable">
                    <div class="col-lg-12">
                        <div class="card flex-fill">
                            <div class="card-header d-flex justify-content-between">
                                <h5 class="card-title mb-0"><?= $title ?></h5>
                                <a href="<?= base_url('dashboard/students/create') ?>" class="btn btn-primary float-right fas fa-plus"></a>
                            </div>

                            <div class="modal fade" id="defaultModalPrimary" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Import Siswa</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body m-3">
                                            <form action="<?= base_url('students/upload') ?>" enctype="multipart/form-data" method="post">
                                                <div class="input-group mb-3">
                                                    <input class="form-control" type="file" id="file-import-siswa" name="fileURL">
                                                    <button type="submit" class="btn btn-primary">Import</button>

                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <a href="<?= base_url('students/template') ?>">Download Template</a>
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <?php if (form_error('fileURL')) { ?>
                                    <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php print form_error('fileURL'); ?>
                                    </div>
                                <?php } ?>

                                <div class="btn-group mb-3 btn-group-sm" >
                                    <a class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#defaultModalPrimary">Import</a>
                                    <a href="<?= base_url('students/export_excel') ?>" type="button" class="btn btn-success">Export</a>
                                </div>

                                <div class="table-responsive">
                                    <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>
                                    <div class="flash-data-error" data-flashdataerror="<?= $this->session->flashdata('error') ?>"></div>
                                    <table class="table table-hover my-0" id="table" style="width:100%;">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Foto</th>
                                                <th>NIS</th>
                                                <th>Nama</th>
                                                <th>Kelas</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 0;
                                            foreach ($students as $student) : ?>
                                                <tr>
                                                    <td><?= ++$no; ?></td>
                                                    <td>
                                                        <img src="<?= base_url('../assets/img/uploads/' . $student['foto']) ?>" class="img-fluid img-thumbnail mx-auto d-block text-center overflow-hidden" style="height:100px; width:100px" alt="">

                                                    </td>
                                                    <td><?= $student['nis'] ?></td>
                                                    <td><?= $student['nama'] ?></td>
                                                    <td><?= $student['kelas']  ?></td>

                                                    <td>
                                                        <a class="fa btn-sm fa-edit btn bg-warning text-white" href="<?= base_url('dashboard/students/edit/' . $student['id_student']) ?>"></a>
                                                        <a class="fas fa-eye btn-sm btn bg-info text-white" href="<?= base_url('dashboard/students/' . $student['id_student']) ?>"></a>
                                                        <a id="delete-button" class="fas btn-sm fa-trash btn bg-danger text-white" href="<?= base_url('students/delete/' . $student['id_student']) ?>"></a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </main>

        <?php $this->load->view('layout/footer') ?>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        $('#btn_upload').click(function() {

            var fd = new FormData();
            var files = $('#file')[0].files[0];
            fd.append('file', files);

            // AJAX request
            $.ajax({
                url: 'ajaxfile.php',
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response != 0) {
                        // Show image preview
                        $('#preview').append("<img src='" + response + "' width='100' height='100' style='display: inline-block;'>");
                    } else {
                        alert('file not uploaded');
                    }
                }
            });
        });
    });
</script>

<?php $this->load->view('layout/foot') ?>