<?php $this->load->view('layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('layout/header') ?>

        <main class="content">
        <div class="container-fluid p-0">
            <div class="row removable">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                        <h5 class="card-title mb-0"><?= $title ?></h5>
                        </div>
                        <div class="card-body">
                            <?= $this->session->flashdata('message'); ?>
                            <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>
                            <form action="<?= base_url('/operational_times/update') ?>" method="post">
                                <input type="hidden" name="_token" value="<?= $operational_time['id_operational_time']?>">    
                            <div class="row justify-content-center">
                            <div class="col-md-5 text-center">
                                <label for="waktu_masuk">Waktu Masuk Siswa</label>
                            </div>
                            <div class="col-md-5 text-center">
                                <label for="waktu_keluar">Waktu Keluar Siswa</label>
                            </div>
                            <div class="col-md-2 text-center">
                                <label for="telat">Batas Waktu Telat</label>
                            </div>

                            <div class="col-md-5">
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <input type="time" name="waktu_masuk_awal" id="waktu_masuk_awal"
                                            class="form-control text-center" value="<?= $operational_time['waktu_masuk_awal']?>">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="time" name="waktu_masuk_akhir" id="waktu_masuk_akhir"
                                            class="form-control text-center" value="<?= $operational_time['waktu_masuk_akhir']?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <input type="time" name="waktu_keluar_awal" id="waktu_keluar_awal"
                                            class="form-control text-center" value="<?= $operational_time['waktu_keluar_awal']?>">
                                    </div>

                                    <div class="col-md-6">
                                        <input type="time" name="waktu_keluar_akhir" id="waktu_keluar_akhir"
                                            class="form-control text-center" value="<?= $operational_time['waktu_keluar_akhir']?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <input type="time" name="telat" id="telat"
                                            class="form-control text-center" value="<?= $operational_time['telat']?>">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-center mt-2">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-danger">Set Waktu</button>
                                </div>
                            </div>
                        </div>
                    </form>

                            <form action="<?= base_url('settings/update') ?>" method="post">
                                <input type="hidden" class="form-control" name="id_setting" value="<?= $settings_secret_key['id_setting'] ?>">

                                <div class="form-group">
                                    <label for="secret_key">Secret Key</label>
                                    <input autocomplete="off" type="text" class="form-control" name="secret_key" value="<?= $settings_secret_key['value'] ?>" id="secret_key" aria-describedby="secret_key" placeholder="Masukan Kelas">
                                    <span class="text-danger">
                                        <?= form_error('secret_key') ?>
                                    </span>
                                </div>
                                <div class="form-group mt-1">
                                    <button type="submit" value="upload" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

        <?php $this->load->view('layout/footer') ?>
    </div>
</div>

<?php $this->load->view('layout/foot') ?>
