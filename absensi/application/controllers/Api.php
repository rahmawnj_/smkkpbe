

<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Api extends RestController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("form_validation");
        $this->load->model(['student_model', 'attandance_model', 'holiday_model', 'attandance_device_model', 'operational_time_model', 'setting_model']);
        $this->load->helper(['form', 'url']);
    }

    public function absen_json_post()
    {
        
        if (is_null($this->input->post('secret_key'))) {
            $this->response(['msg' => 'secret_key perlu dikirim', 'status' => 404]);
        }
        if (is_null($this->input->post('id_device'))) {
            $this->response(['msg' => 'id_device perlu dikirim', 'status' => 404]);
        }
        if (is_null($this->input->post('rfid'))) {
            $this->response(['msg' => 'rfid perlu dikirim', 'status' => 404]);
        }

        $setting_secret_key = $this->setting_model->get_setting('name', 'secret_key');

  
        if ($setting_secret_key[0]['value'] == $this->input->post('secret_key')) {

            $device = $this->attandance_device_model->get_attandance_device('device', $this->input->post('id_device'));
            if ($device) {
               
                $student = $this->student_model->get_student('rfid', $this->input->post('rfid'));
                if (count($student) !== 0) {
                    $date_now = date('Y-m-d');
                    $day_now = date('l');

                    $holiday = $this->holiday_model->get_holiday('waktu', $date_now);
                    if ((count($holiday) === 0 && $day_now !== 'Saturday') && (count($holiday) === 0 && $day_now !== 'Sunday') ) {
                        $operational_time = $this->operational_time_model->get_operational_time('id_operational_time', 1);
                        $masuk = explode('-', $operational_time[0]['waktu_masuk']);
                        $keluar = explode('-', $operational_time[0]['waktu_keluar']);
                        $masuk_awal = $masuk[0];
                        $masuk_akhir = $masuk[1];
                        $keluar_awal = $keluar[0];
                        $keluar_akhir = $keluar[1];
                        $telat = $operational_time[0]['telat'];
                        $time_now = "11:10";
                        if ($time_now < $masuk_akhir && $time_now > $masuk_awal ) {
                            $attandance = $this->attandance_model->get_attandace('date', $date_now);
                            if (count($attandance) === 0) {
                                if ($time_now < $telat) {
                                    $ket = 'Hadir - Tepat waktu';
                                } else {
                                    $ket = 'Hadir - Telat';
                                }
                                $this->attandance_model->insert([
                                    'id_device' => $device[0]['id_attandance_device'],
                                    'id_student' => $student[0]['id_student'],
                                    'masuk' => 1,
                                    'waktu_masuk' => $time_now,
                                    'keluar' => 0,
                                    'status_hadir' => 'Hadir',
                                    'ket'  => $ket,
                                    'date' => $date_now,
                                ]);
                                $this->response(['msg' => $ket, 'status' => 200]);
                            } else {
                                $this->response(['msg' => 'Error - Sudah Absen', 'status' => 404]);

                            }
                           
                        } else if($time_now > $keluar_awal && $time_now < $keluar_akhir){
                            $attandance = $this->attandance_model->get_attandace('date', $date_now);
                            if (count($attandance) !== 0) {
                                $this->attandance_model->update($attandance[0]['id_attandance'], [
                                    'keluar' => 1,
                                    'waktu_keluar' => $time_now,
                                ]);
                                $this->response(['msg' => 'Keluar', 'status' => 200]);
                            } else {
                               
                                $this->response(['msg' => 'Absensi Gagal', 'status' => 404]);
                            }
                        }
                        else {
                            $this->response(['msg' => 'Error waktu operasional', 'status' => 404]);
                        }
                    } else {
                        $this->response(['msg' => 'Hari Libur', 'status' => 404]);
                    }
                } else {
                    $this->response(['msg' => 'rfid tidak ditemukan', 'status' => 404]);
                }
            } else {
                $this->response(['msg' => 'id_device tidak ditemukan', 'status' => 404]);
            }
        } else {
            $this->response(['msg' => 'secret_key salah', 'status' => 404]);
        }
    }
}
