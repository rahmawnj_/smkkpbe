<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cron extends CI_Controller 
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['attandance_model', 'student_model']);
    }
    
    /**
     * This function is used to update the age of users automatically
     * This function is called by cron job once in a day at midnight 00:00
     */
    public function update_absen()
    {    
        $date_now = date('Y-m-d');
        $students = $this->student_model->get_students();
        foreach($students as $student) {
            $attandance = $this->attandance_model->get_attandace2('date', $date_now, 'id_student', $student['id_student']);
            if (count($attandance) === 0) {
                $this->attandance_model->insert([
                    'id_device' => 0,
                    'id_student' => $student['id_student'],
                    'masuk' => 0,
                    'waktu_masuk' => "-",
                    'keluar' => 0,
                    'waktu_keluar' => "-",
                    'status_hadir' => 'Alfa',
                    'ket'  => 'Alfa',
                    'date' => $date_now,
                ]);
            }
        }

        
    }
}
?>