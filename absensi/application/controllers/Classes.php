<?php
defined('BASEPATH') or exit('No direct script access allowed');
// require 'vendor/autoload.php';

// use PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\Reader\Csv;
// use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class Classes extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(["form_validation", 'session']);
        $this->load->model(['class_model', 'student_model']);
        $this->load->helper(['form', 'url']);
        if (!$this->session->userdata('status')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
    }

    public function index()
    {
        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }
        $data = [
            'title' => 'Kelas',
            'classes' => $this->class_model->get_classes()
        ];

        $this->load->view('dashboard/classes/index', $data);
    }

    public function create()
    {
        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }
        $data = [
            'title' => 'Kelas',
        ];

        $this->load->view('dashboard/classes/create', $data);
    }

    public function store()
    {
        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }
        $this->form_validation->set_rules('kelas', 'Kelas', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->create();
        } else {

            $this->class_model->insert([
                'kelas' => $this->input->post('kelas'),
                'created_at' => date('Y-m-d H:i:s'),
                'deleted' => 0,
            ]);

            $this->session->set_flashdata('success', 'Kelas Berhasil Ditambahkan!');
            redirect('dashboard/classes');
        }
    }

    public function view($id_class)
    {
        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }
        $data = [
            'title' => 'Kelas',
            'class' => $this->class_model->get_class('id_class', $id_class)
        ];

        $this->load->view('dashboard/classes/view', $data);
    }

    public function edit($id_class)
    {
        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }
        $data = [
            'title' => 'Kelas',
            'class' => $this->class_model->get_class('id_class', $id_class)[0]
        ];
        $this->load->view('dashboard/classes/edit', $data);
    }

    public function update()
    {
        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }
        $this->form_validation->set_rules('kelas', 'Kelas', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->edit($this->input->post('id_class'));
        } else {
            $this->class_model->update($this->input->post('id_class'), [
                'kelas' => $this->input->post('kelas'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            $this->session->set_flashdata('success', 'Kelas Berhasil Diperbarui!');
            redirect('dashboard/classes');
        }
    }

    public function delete($id_class)
    {
        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }

        $students = $this->student_model->get_student('students.id_class', $id_class);
        if (!$students) {
            $this->class_model->delete($id_class);
            $this->session->set_flashdata('success', 'Kelas Berhasil Dihapus!');
        } else {
            $this->session->set_flashdata('error', 'Kelas Gagal Dihapus!');
        }
        redirect('dashboard/classes');
    }

    public function students($id_class)
    {

        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }

        $data = [
            'title' => 'Kelas',
            'classes' => $this->class_model->get_classes(),
            'class' => $this->class_model->get_class('id_class', $id_class)[0],
            'students' => $this->student_model->get_student('students.id_class', $id_class)
        ];

        $this->load->view('dashboard/classes/students', $data);
    }

    public function student_update()
    {
      if ($this->input->post('chkbox[]') !== null) {
        foreach ($this->input->post('chkbox[]') as $chkbox) {
            $this->student_model->update($chkbox, [
                'id_class' => $this->input->post('class'),
            ]);
        }
        $this->students($this->input->post('id_class'));
      } else {
          $this->students($this->input->post('id_class'));

      }
       
    }
   
}
