<?php
defined('BASEPATH') or exit('No direct script access allowed');

class settings extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(["form_validation", 'session']);
        $this->load->model(['setting_model', 'operational_time_model']);
        $this->load->helper(['form', 'url']);
        if (!$this->session->userdata('status')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
    }

    public function index()
    {
        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }
        $operational_time = $this->operational_time_model->get_operational_time('id_operational_time', 1)[0];
        $waktu_masuk_awal  = explode('-', $operational_time['waktu_masuk'])[0];
        $waktu_masuk_akhir  = explode('-', $operational_time['waktu_masuk'])[1];
        $waktu_keluar_awal  = explode('-', $operational_time['waktu_keluar'])[0];
        $waktu_keluar_akhir  = explode('-', $operational_time['waktu_keluar'])[1];
        $telat = $operational_time['telat'];
        $id_operational_time = $operational_time['id_operational_time'];
        $data = [
            'title' => 'Setting',
            'settings_secret_key' => $this->setting_model->get_setting('name', 'secret_key')[0],
            'operational_time' => [
                'waktu_masuk_awal' => $waktu_masuk_awal,
                'waktu_masuk_akhir' => $waktu_masuk_akhir,
                'waktu_keluar_awal' => $waktu_keluar_awal,
                'waktu_keluar_akhir' => $waktu_keluar_akhir,
                'telat' => $telat,
                'id_operational_time' => $id_operational_time,
            ]
        ];
    
        $this->load->view('dashboard/settings/index', $data);
    }


    public function update()
    {
        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }
        $this->form_validation->set_rules('secret_key', 'Secret Key', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->index();
        } else {
            $this->setting_model->update($this->input->post('id_setting'), [
                'value' => $this->input->post('secret_key'),
            ]);
            $this->session->set_flashdata('success', 'Setting Secret Key Berhasil Diperbarui!');
            redirect('settings');
        }
    }
}
