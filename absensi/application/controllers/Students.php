<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Students extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("form_validation");
        $this->load->model(['student_model', 'class_model']);
        $this->load->helper(['form', 'url', 'encryption']);
        if (!$this->session->userdata('status')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
    }

    public function template()
	{
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
        // set Header
        $sheet->SetCellValue('A1', 'nama')->getColumnDimension('A')->setAutoSize(true);
        $sheet->SetCellValue('B1', 'nis')->getColumnDimension('B')->setAutoSize(true);
        $sheet->SetCellValue('C1', 'rfid')->getColumnDimension('C')->setAutoSize(true);
        $sheet->SetCellValue('D1', 'jenis_kelamin')->getColumnDimension('D')->setAutoSize(true);
        $sheet->SetCellValue('E1', 'tempat_lahir')->getColumnDimension('E')->setAutoSize(true);       
        $sheet->SetCellValue('F1', 'tanggal_lahir')->getColumnDimension('F')->setAutoSize(true);       
        $sheet->SetCellValue('G1', 'alamat')->getColumnDimension('G')->setAutoSize(true);       
        $sheet->SetCellValue('H1', 'no_hp')->getColumnDimension('H')->setAutoSize(true); 

        $sheet->SetCellValue('A2', 'Putra')->getColumnDimension('A')->setAutoSize(true);
        $sheet->SetCellValue('B2', '11111111')->getColumnDimension('B')->setAutoSize(true);
        $sheet->SetCellValue('C2', '1111111')->getColumnDimension('C')->setAutoSize(true);
        $sheet->SetCellValue('D2', 'Laki-laki')->getColumnDimension('D')->setAutoSize(true);
        $sheet->SetCellValue('E2', 'Jakarta')->getColumnDimension('E')->setAutoSize(true);       
        $sheet->SetCellValue('F2', '2022-02-20')->getColumnDimension('F')->setAutoSize(true);       
        $sheet->SetCellValue('G2', 'Jakarta')->getColumnDimension('G')->setAutoSize(true);       
        $sheet->SetCellValue('H2', '08888888')->getColumnDimension('H')->setAutoSize(true); 

        $sheet->SetCellValue('A3', 'Putri')->getColumnDimension('A')->setAutoSize(true);
        $sheet->SetCellValue('B3', '222222222')->getColumnDimension('B')->setAutoSize(true);
        $sheet->SetCellValue('C3', '222222')->getColumnDimension('C')->setAutoSize(true);
        $sheet->SetCellValue('D3', 'Perempuan')->getColumnDimension('D')->setAutoSize(true);
        $sheet->SetCellValue('E3', 'Jakarta')->getColumnDimension('E')->setAutoSize(true);       
        $sheet->SetCellValue('F3', '2022-02-20')->getColumnDimension('F')->setAutoSize(true);       
        $sheet->SetCellValue('G3', 'Jakarta')->getColumnDimension('G')->setAutoSize(true);       
        $sheet->SetCellValue('H3', '088888888')->getColumnDimension('H')->setAutoSize(true);       
        
		$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
		$filename = 'example-siswa';

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
		header('Cache-Control: max-age=0');
		
		$writer->save('php://output'); // download file 

	}


    public function export_excel()
	{
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
        // set Header
        $sheet->SetCellValue('A1', 'NIS')->getColumnDimension('A')->setAutoSize(true);
        $sheet->SetCellValue('B1', 'Nama')->getColumnDimension('B')->setAutoSize(true);
        $sheet->SetCellValue('C1', 'Jenis Kelamin')->getColumnDimension('C')->setAutoSize(true);
        $sheet->SetCellValue('D1', 'Tempat Lahir')->getColumnDimension('D')->setAutoSize(true);
        $sheet->SetCellValue('E1', 'Tanggal Lahir')->getColumnDimension('E')->setAutoSize(true);       
        $sheet->SetCellValue('F1', 'Alamat')->getColumnDimension('E')->setAutoSize(true);       
        $sheet->SetCellValue('G1', 'No HP')->getColumnDimension('F')->setAutoSize(true);       
        // set Row
		$students = $this->student_model->get_students();
        $rowCount = 2;
        foreach ($students as $student) {
            $sheet->SetCellValue('A' . $rowCount, $student['nis'])->getColumnDimension('A')->setAutoSize(true);
            $sheet->SetCellValue('B' . $rowCount, $student['nama'])->getColumnDimension('B')->setAutoSize(true);
            $sheet->SetCellValue('C' . $rowCount, $student['jenis_kelamin'])->getColumnDimension('C')->setAutoSize(true);
            $sheet->SetCellValue('D' . $rowCount, $student['tempat_lahir'])->getColumnDimension('D')->setAutoSize(true);
            $sheet->SetCellValue('E' . $rowCount, $student['tanggal_lahir'])->getColumnDimension('E')->setAutoSize(true);
            $sheet->SetCellValue('F' . $rowCount, $student['alamat'])->getColumnDimension('F')->setAutoSize(true);
            $sheet->SetCellValue('G' . $rowCount, $student['no_hp'])->getColumnDimension('G')->setAutoSize(true);
            $rowCount++;
        }
		
		$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);

		$filename = date('D-M-Y');

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
		header('Cache-Control: max-age=0');
		
		$writer->save('php://output'); // download file 

	}

    // file upload functionality
    public function upload()
    {
        $data = array();
        // Load form validation library
        $this->load->library('form_validation');
        $this->form_validation->set_rules('fileURL', 'Upload File', 'callback_checkFileValidation');
        if ($this->form_validation->run() == false) {

            $this->load->view('spreadsheet/index', $data);
        } else {
            // If file uploaded
            if (!empty($_FILES['fileURL']['name'])) {
                // get file extension
                $extension = pathinfo($_FILES['fileURL']['name'], PATHINFO_EXTENSION);

                if ($extension == 'csv') {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
                } elseif ($extension == 'xlsx') {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                } else {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                }
                // file path
                $spreadsheet = $reader->load($_FILES['fileURL']['tmp_name']);
                $allDataInSheet = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

                // array Count
                $arrayCount = count($allDataInSheet);
                $flag = 0;
                $createArray = ['nama', 'nis', 'rfid', 'jenis_kelamin', 'tanggal_lahir', 'tempat_lahir', 'alamat', 'no_hp'];
                $makeArray = ['nama' => 'nama', 'nis' => 'nis', 'rfid' => 'rfid', 'jenis_kelamin' => 'jenis_kelamin', 'tanggal_lahir' => 'tanggal_lahir', 'tempat_lahir' => 'tempat_lahir', 'alamat' => 'alamat', 'no_hp' => 'no_hp'];
                $SheetDataKey = [];
                foreach ($allDataInSheet as $dataInSheet) {
                    foreach ($dataInSheet as $key => $value) {
                        if (in_array(trim($value), $createArray)) {
                            $value = preg_replace('/\s+/', '', $value);
                            $SheetDataKey[trim($value)] = $key;
                        }
                    }
                }

                $dataDiff = array_diff_key($makeArray, $SheetDataKey);
                if (empty($dataDiff)) {
                    $flag = 1;
                }

                // match excel sheet column
                if ($flag == 1) {
                    for ($i = 2; $i <= $arrayCount; $i++) {
                        $nama = $SheetDataKey['nama'];
                        $nis = $SheetDataKey['nis'];
                        $rfid = $SheetDataKey['rfid'];
                        $jenis_kelamin = $SheetDataKey['jenis_kelamin'];
                        $tempat_lahir = $SheetDataKey['tempat_lahir'];
                        $tanggal_lahir = $SheetDataKey['tanggal_lahir'];
                        $alamat = $SheetDataKey['alamat'];
                        $no_hp = $SheetDataKey['no_hp'];

                        $encryption =  encryption('0', en_iv(), $allDataInSheet[$i][$nis]);

                        $saldo = filter_var(trim($encryption), FILTER_SANITIZE_STRING);
                        $nama = filter_var(trim($allDataInSheet[$i][$nama]), FILTER_SANITIZE_STRING);
                        $nis = filter_var(trim($allDataInSheet[$i][$nis]), FILTER_SANITIZE_STRING);
                        $rfid = filter_var(trim($allDataInSheet[$i][$rfid]), FILTER_SANITIZE_STRING);
                        $pin = filter_var(trim(password_hash('000000', PASSWORD_DEFAULT)), FILTER_SANITIZE_STRING);
                        $jenis_kelamin = filter_var(trim($allDataInSheet[$i][$jenis_kelamin]), FILTER_SANITIZE_STRING);
                        $tempat_lahir = filter_var(trim($allDataInSheet[$i][$tempat_lahir]), FILTER_SANITIZE_STRING);
                        $tanggal_lahir = filter_var(trim($allDataInSheet[$i][$tanggal_lahir]), FILTER_SANITIZE_STRING);
                        $alamat = filter_var(trim($allDataInSheet[$i][$alamat]), FILTER_SANITIZE_STRING);
                        $no_hp = filter_var(trim($allDataInSheet[$i][$no_hp]), FILTER_SANITIZE_STRING);
                        $id_class = filter_var(trim(0), FILTER_SANITIZE_STRING);
                        $deleted = filter_var(trim(0), FILTER_SANITIZE_STRING);
                        $fetchData[] = ['id_class' => $id_class, 'deleted' => $deleted, 'nama' => $nama, 'nis' => $nis, 'rfid' => $rfid, 'saldo' => $saldo, 'pin' => $pin , 'jenis_kelamin' => $jenis_kelamin, 'tempat_lahir' => $tempat_lahir, 'tanggal_lahir' => $tanggal_lahir, 'alamat' => $alamat, 'no_hp' => $no_hp,];
                    }
                    $data['dataInfo'] = $fetchData;
                    $this->student_model->setBatchImport($fetchData);
                    $this->student_model->importData();
                } else {
                    echo "Please import correct file, did not match excel sheet column";
                }
                $this->session->set_flashdata('success', 'Siswa Berhasil Ditambahkan!');
                redirect('dashboard/students');
            }
        }
    }

    // checkFileValidation
    public function checkFileValidation($string)
    {
        $file_mimes = array(
            'text/x-comma-separated-values',
            'text/comma-separated-values',
            'application/octet-stream',
            'application/vnd.ms-excel',
            'application/x-csv',
            'text/x-csv',
            'text/csv',
            'application/csv',
            'application/excel',
            'application/vnd.msexcel',
            'text/plain',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        );
        if (isset($_FILES['fileURL']['name'])) {
            $arr_file = explode('.', $_FILES['fileURL']['name']);
            $extension = end($arr_file);
            if (($extension == 'xlsx' || $extension == 'xls' || $extension == 'csv') && in_array($_FILES['fileURL']['type'], $file_mimes)) {
                return true;
            } else {
                $this->form_validation->set_message('checkFileValidation', 'Please choose correct file.');
                return false;
            }
        } else {
            $this->form_validation->set_message('checkFileValidation', 'Please choose a file.');
            return false;
        }
    }

    public function index()
    {
        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }
        $data = [
            'title' => 'Siswa',
            'students' => $this->student_model->get_students()
        ];

        $this->load->view('dashboard/students/index', $data);
    }

    public function create()
    {
        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }
        $data = [
            'title' => 'Siswa',
            'classes' => $this->class_model->get_classes()
        ];

        $this->load->view('dashboard/students/create', $data);
    }

    public function store()
    {

        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }
        $this->form_validation->set_rules('class', 'Class', 'required|trim');
        $this->form_validation->set_rules('nama', 'Name', 'required|trim');
        $this->form_validation->set_rules('nis', 'NIS', 'required|trim|is_unique[students.nis]');
        $this->form_validation->set_rules('rfid', 'RFID', 'required|trim|is_unique[students.rfid]|is_unique[teacher_staffs.rfid]');
        $this->form_validation->set_rules('jenis_kelamin', 'Gender', 'required|trim');
        $this->form_validation->set_rules('tempat_lahir', 'Place of Birth', 'required|trim');
        $this->form_validation->set_rules('tanggal_lahir', 'Date of Birth', 'required|trim');
        $this->form_validation->set_rules('alamat', 'Address', 'required|trim');
        $this->form_validation->set_rules('no_hp', 'Phone Number', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->create();
        } else {

            $config['upload_path']          = '../assets/img/uploads/students';
            $config['allowed_types']        = 'gif|jpg|png|jpeg|jfif';
            $config['max_size']             = 10000;
            $config['max_width']            = 10000;
            $config['max_height']           = 10000;

            $this->load->library('upload', $config);
            $this->upload->do_upload('foto');

            $encryption =  encryption('0', en_iv(), $this->input->post('nis'));
            $this->student_model->insert([
                'id_class' => $this->input->post('class'),
                'nama' => $this->input->post('nama'),
                'nis' => $this->input->post('nis'),
                'rfid' => $this->input->post('rfid'),
                'jenis_kelamin' => $this->input->post('jenis_kelamin'),
                'saldo' => $encryption,
                'deleted' => 0,
                'pin' => password_hash('000000', PASSWORD_DEFAULT),
                'tempat_lahir' => $this->input->post('tempat_lahir'),
                'tanggal_lahir' => $this->input->post('tanggal_lahir'),
                'alamat' => $this->input->post('alamat'),
                'no_hp' => $this->input->post('no_hp'),
                'foto' => 'students/' . $this->upload->data('file_name'),
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $this->session->set_flashdata('success', 'Siswa Berhasil Ditambahkan!');
            redirect('dashboard/students');
        }
    }

    public function view($id_student)
    {
        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }
        $data = [
            'title' => 'Siswa',
            'student' => $this->student_model->get_student('id_student', $id_student)[0]
        ];

        $this->load->view('dashboard/students/view', $data);
    }

    public function edit($id_student)
    {
        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }
        $data = [
            'title' => 'Siswa',
            'classes' => $this->class_model->get_classes(),
            'student' => $this->student_model->get_student('id_student', $id_student)[0]
        ];

        $this->load->view('dashboard/students/edit', $data);
    }

    public function update()
    {
        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }
        $student = $this->student_model->get_student('id_student', $this->input->post('id_student'))[0];

        if ($this->input->post('nis') == $student['nis'] || $this->input->post('rfid') == $student['rfid']) {
            $nis_rules = 'required|trim';
            $rfid_rules = 'required|trim';
        } else {
            $nis_rules = 'required|trim|is_unique[students.nis]';
            $rfid_rules = 'required|trim|is_unique[students.rfid]|is_unique[teacher_staffs.rfid]';
        }

        $this->form_validation->set_rules('class', 'Class', 'required|trim');
        $this->form_validation->set_rules('nama', 'Name', 'required|trim');
        $this->form_validation->set_rules('nis', 'NIS', $nis_rules);
        $this->form_validation->set_rules('rfid', 'RFID', $rfid_rules);
        $this->form_validation->set_rules('jenis_kelamin', 'Gender', 'required|trim');
        $this->form_validation->set_rules('tempat_lahir', 'Place of Birth', 'required|trim');
        $this->form_validation->set_rules('tanggal_lahir', 'Date of Birth', 'required|trim');
        $this->form_validation->set_rules('alamat', 'Address', 'required|trim');
        $this->form_validation->set_rules('no_hp', 'Phone Number', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->edit($this->input->post('id_student'));
        } else {
            $config['upload_path']          = '../assets/img/uploads/students';
            $config['allowed_types']        = 'gif|jpg|png|jpeg|jfif';
            $config['max_size']             = 10000;
            $config['max_width']            = 10000;
            $config['max_height']           = 10000;

            $this->load->library('upload', $config);
            $this->upload->do_upload('foto');

            if ($this->upload->data('file_name') == '') {
                $foto = $student['foto'];
            } else {
                unlink('../assets/img/uploads/' . $student['foto']);
                $foto = 'students/' . $this->upload->data('file_name');
            }
            $decrypt_saldo =  decryption($student['saldo'], de_iv(), $student['nis']);
            $encrypt_saldo = encryption($decrypt_saldo, en_iv(), $this->input->post('nis'));

            $this->student_model->update($this->input->post('id_student'), [
                'id_class' => $this->input->post('class'),
                'nama' => $this->input->post('nama'),
                'nis' => $this->input->post('nis'),
                'rfid' => $this->input->post('rfid'),
                'jenis_kelamin' => $this->input->post('jenis_kelamin'),
                'saldo' => $encrypt_saldo,
                'pin' => $student['pin'],
                'tempat_lahir' => $this->input->post('tempat_lahir'),
                'tanggal_lahir' => $this->input->post('tanggal_lahir'),
                'alamat' => $this->input->post('alamat'),
                'no_hp' => $this->input->post('no_hp'),
                'foto' => $foto,
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            $this->session->set_flashdata('success', 'Siswa Berhasil Diperbarui!');
            redirect('dashboard/students');
        }
    }

    public function delete($id_student)
    {
        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }
        $student = $this->student_model->get_student('id_student', $id_student)[0];

        $this->student_model->delete($id_student);
        unlink('../assets/img/uploads/' . $student['foto']);
        $this->session->set_flashdata('success', 'Siswa Berhasil Dihapus!');
        redirect('dashboard/students');
    }
}
