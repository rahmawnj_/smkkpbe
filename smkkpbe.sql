-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 08, 2022 at 10:05 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smkkpbe`
--

-- --------------------------------------------------------

--
-- Table structure for table `attandances`
--

CREATE TABLE `attandances` (
  `id_attandance` int(11) NOT NULL,
  `id_device` int(11) NOT NULL,
  `id_student` int(11) NOT NULL,
  `masuk` int(11) NOT NULL,
  `waktu_masuk` varchar(50) NOT NULL,
  `keluar` int(11) DEFAULT NULL,
  `waktu_keluar` varchar(50) DEFAULT NULL,
  `status_hadir` varchar(11) NOT NULL,
  `ket` varchar(100) NOT NULL,
  `edited_by` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attandances`
--

INSERT INTO `attandances` (`id_attandance`, `id_device`, `id_student`, `masuk`, `waktu_masuk`, `keluar`, `waktu_keluar`, `status_hadir`, `ket`, `edited_by`, `date`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '07:10', 1, '11:10', 'Hadir', 'Hadir - Telat', NULL, '2022-11-08', '2022-11-08 07:41:26', '2022-11-08 07:37:13'),
(2, 0, 2, 0, '-', 0, '-', 'Alfa', 'Alfa', NULL, '2022-11-08', '2022-11-08 07:42:36', '2022-11-08 07:42:36');

-- --------------------------------------------------------

--
-- Table structure for table `attandance_devices`
--

CREATE TABLE `attandance_devices` (
  `id_attandance_device` int(11) NOT NULL,
  `device` varchar(100) NOT NULL,
  `lokasi` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `deleted` int(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attandance_devices`
--

INSERT INTO `attandance_devices` (`id_attandance_device`, `device`, `lokasi`, `gambar`, `deleted`, `created_at`, `updated_at`) VALUES
(1, '7878', '6546745', 'attandance_devicesdownload.png', 0, '2022-11-08 07:34:43', '2022-11-08 07:34:43');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id_cart` int(11) NOT NULL,
  `id_merchant` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `jumlah_harga` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id_category` int(11) NOT NULL,
  `kategori` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id_class` int(11) NOT NULL,
  `kelas` varchar(50) NOT NULL,
  `deleted` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id_class`, `kelas`, `deleted`, `created_at`, `updated_at`) VALUES
(1, '57984543', 0, '2022-11-08 07:25:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE `devices` (
  `id_device` int(11) NOT NULL,
  `device` varchar(100) NOT NULL,
  `harga` int(11) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `lokasi` varchar(100) DEFAULT NULL,
  `created_at` text NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `devices`
--

INSERT INTO `devices` (`id_device`, `device`, `harga`, `gambar`, `lokasi`, `created_at`, `updated_at`, `deleted`) VALUES
(1, '', 54764, 'devices/new2.jpeg', '6458645', '2022-11-08 13:21:30', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `id_holiday` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `waktu` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `merchants`
--

CREATE TABLE `merchants` (
  `id_merchant` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `deleted` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `merchants`
--

INSERT INTO `merchants` (`id_merchant`, `nama`, `gambar`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'hjehktr', 'merchants/WhatsApp_Image_2022-11-02_at_11_55_25.jpeg', 1, '2022-11-08 06:30:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `merchant_accounts`
--

CREATE TABLE `merchant_accounts` (
  `id_merchant_account` int(11) NOT NULL,
  `id_merchant` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(20) NOT NULL,
  `deleted` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `merchant_accounts`
--

INSERT INTO `merchant_accounts` (`id_merchant_account`, `id_merchant`, `username`, `password`, `role`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 1, '', '', 'owner', 1, '2022-11-08 06:42:03', '2022-11-08 06:30:59'),
(2, 1, '', '', 'cashier', 1, '2022-11-08 06:42:03', '2022-11-08 06:30:59');

-- --------------------------------------------------------

--
-- Table structure for table `operational_times`
--

CREATE TABLE `operational_times` (
  `id_operational_time` int(11) NOT NULL,
  `waktu_masuk` varchar(20) NOT NULL,
  `telat` varchar(20) NOT NULL,
  `waktu_keluar` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `operational_times`
--

INSERT INTO `operational_times` (`id_operational_time`, `waktu_masuk`, `telat`, `waktu_keluar`) VALUES
(1, '06:00-07:30', '01:05', '11:00-13:00');

-- --------------------------------------------------------

--
-- Table structure for table `order_transactions`
--

CREATE TABLE `order_transactions` (
  `id_order_transaction` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `id_merchant` int(11) NOT NULL,
  `saldo_awal` varchar(50) NOT NULL,
  `saldo_akhir` varchar(50) NOT NULL,
  `harga` int(11) NOT NULL,
  `profit` int(11) NOT NULL,
  `status` varchar(10) DEFAULT NULL,
  `metode` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id_product` int(11) NOT NULL,
  `id_merchant` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `stok` int(11) NOT NULL,
  `harga_modal` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `purchased_items`
--

CREATE TABLE `purchased_items` (
  `id_purchased_item` int(11) NOT NULL,
  `id_order_transaction` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id_role` int(11) NOT NULL,
  `role` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id_role`, `role`) VALUES
(1, 'administrator'),
(2, 'admin_topup'),
(3, 'admin_absensi');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id_setting` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id_setting`, `name`, `value`) VALUES
(1, 'secret_key', 'smkkpbaleendah');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id_student` int(11) NOT NULL,
  `id_class` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nis` varchar(50) NOT NULL,
  `saldo` varchar(255) NOT NULL,
  `rfid` varchar(50) NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `pin` varchar(100) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `deleted` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id_student`, `id_class`, `nama`, `nis`, `saldo`, `rfid`, `jenis_kelamin`, `pin`, `tempat_lahir`, `tanggal_lahir`, `alamat`, `no_hp`, `foto`, `deleted`, `updated_at`, `created_at`) VALUES
(1, 1, 'Rahma', '11111111111', 'Cw==', '22222222222', 'Perempuan', '$2y$10$wrhs75OcOmNyvv8SMRkhhutBfZFmSOMOs1NAdBC2I.savqbF03ryq', 'Bandung', '2022-11-16', 'C843', '86096490654', 'students/WhatsApp_Image_2022-11-02_at_11_57_45.jpeg', 0, NULL, '2022-11-08 07:26:37'),
(2, 1, 'diasji', '347685', 'Lw==', '6549876', 'Perempuan', '$2y$10$FeceBGvCfND4wBzSSkn45e7UwaOOg56yoGVaMqEzVkMP01H6kmrMO', '646445', '2022-11-10', '545445', '65564', 'students/IMG-20221103-WA0030.jpg', 0, NULL, '2022-11-08 07:42:13');

-- --------------------------------------------------------

--
-- Table structure for table `student_parking_transactions`
--

CREATE TABLE `student_parking_transactions` (
  `id_student_parking_transaction` int(11) NOT NULL,
  `id_student` int(11) NOT NULL,
  `id_device` int(11) NOT NULL,
  `saldo_awal` int(11) NOT NULL,
  `saldo_akhir` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `status` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `teacher_staffs`
--

CREATE TABLE `teacher_staffs` (
  `id_teacher_staff` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `foto` varchar(128) NOT NULL,
  `rfid` varchar(128) NOT NULL,
  `jabatan` varchar(128) NOT NULL,
  `deleted` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `teacher_staffs`
--

INSERT INTO `teacher_staffs` (`id_teacher_staff`, `nama`, `foto`, `rfid`, `jabatan`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'rrejkre', 'teacher_staffs/WhatsApp_Image_2022-11-02_at_11_55_25.jpeg', '', 'Staff', 1, '2022-11-08 06:18:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `teacher_staff_parking_transactions`
--

CREATE TABLE `teacher_staff_parking_transactions` (
  `id_teacher_staff_parking_transaction` int(11) NOT NULL,
  `id_teacher_staff` int(11) NOT NULL,
  `id_device` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `top_ups`
--

CREATE TABLE `top_ups` (
  `id_top_up` int(11) NOT NULL,
  `id_student` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nominal` int(11) NOT NULL,
  `saldo_awal` int(11) NOT NULL,
  `saldo_akhir` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `id_role`, `nama`, `foto`, `username`, `password`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 1, 'Administrator', 'users/oga76v6k.png', 'admin', '$2y$10$qsjTQ2GtUFg7EGO9SmmQcuzhg/WJb6S72QKHIysmlPqhE0nUUarS6', NULL, '2022-08-24 13:28:22', 0),
(2, 2, 'Admin Topup', 'users/9081cbefe7c689caed05a251d1ed78721.png', 'topup', '$2y$10$QQpsvEe8NjVEq/6c6dNZhu/j91igbtkP3Q9u60EbPR8GUiaDu3JeC', '2022-07-31 12:01:18', NULL, 0),
(3, 1, 'Admin', 'users/emoney.png', 'administrator', '$2a$12$uYMT8ZfTP25x6b6ZuwjyoeWCUHJYYCfmKPy5zV4JZH9TrItUWWHxW', NULL, '2022-08-31 06:35:05', 0),
(4, 3, 'Admin Absensi', 'users/new2.jpeg', 'absensi', '$2y$10$qsjTQ2GtUFg7EGO9SmmQcuzhg/WJb6S72QKHIysmlPqhE0nUUarS6', NULL, '2022-11-08 07:19:13', 0),
(5, 3, 'ds9038', 'users/', '543859843', '$2y$10$N18Y3KlS/upvr6FYsvbdp.ZZrICuK2J2AaTqNVMbtdgd2LfvasHlS', '2022-11-08 06:10:29', NULL, 1),
(6, 1, 'rewrew543543', 'users/WhatsApp_Image_2022-11-02_at_11_55_25_(1).jpeg', 'admin543435', '$2y$10$dgF7ia0ITNvQnjqInc3ZW.6zFHgoDrXfx9M76OEVp.ss77KAxP8oK', '2022-11-08 06:11:34', '2022-11-08 06:15:22', 1),
(7, 2, '87898', 'users/IMG-20221103-WA0030.jpg', 'adminjhkjhkiu', '$2y$10$e942MvSc.BrOYfSEmegiJeuEdNXfcrGlIzWRlE6FOFjSsIGT5ajnu', '2022-11-08 07:00:13', NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attandances`
--
ALTER TABLE `attandances`
  ADD PRIMARY KEY (`id_attandance`);

--
-- Indexes for table `attandance_devices`
--
ALTER TABLE `attandance_devices`
  ADD PRIMARY KEY (`id_attandance_device`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id_cart`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id_category`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id_class`);

--
-- Indexes for table `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id_device`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`id_holiday`);

--
-- Indexes for table `merchants`
--
ALTER TABLE `merchants`
  ADD PRIMARY KEY (`id_merchant`);

--
-- Indexes for table `merchant_accounts`
--
ALTER TABLE `merchant_accounts`
  ADD PRIMARY KEY (`id_merchant_account`);

--
-- Indexes for table `operational_times`
--
ALTER TABLE `operational_times`
  ADD PRIMARY KEY (`id_operational_time`);

--
-- Indexes for table `order_transactions`
--
ALTER TABLE `order_transactions`
  ADD PRIMARY KEY (`id_order_transaction`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id_product`);

--
-- Indexes for table `purchased_items`
--
ALTER TABLE `purchased_items`
  ADD PRIMARY KEY (`id_purchased_item`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_role`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id_setting`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id_student`);

--
-- Indexes for table `student_parking_transactions`
--
ALTER TABLE `student_parking_transactions`
  ADD PRIMARY KEY (`id_student_parking_transaction`);

--
-- Indexes for table `teacher_staffs`
--
ALTER TABLE `teacher_staffs`
  ADD PRIMARY KEY (`id_teacher_staff`);

--
-- Indexes for table `teacher_staff_parking_transactions`
--
ALTER TABLE `teacher_staff_parking_transactions`
  ADD PRIMARY KEY (`id_teacher_staff_parking_transaction`);

--
-- Indexes for table `top_ups`
--
ALTER TABLE `top_ups`
  ADD PRIMARY KEY (`id_top_up`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attandances`
--
ALTER TABLE `attandances`
  MODIFY `id_attandance` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `attandance_devices`
--
ALTER TABLE `attandance_devices`
  MODIFY `id_attandance_device` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id_cart` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id_category` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id_class` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `devices`
--
ALTER TABLE `devices`
  MODIFY `id_device` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `id_holiday` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `merchants`
--
ALTER TABLE `merchants`
  MODIFY `id_merchant` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `merchant_accounts`
--
ALTER TABLE `merchant_accounts`
  MODIFY `id_merchant_account` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `operational_times`
--
ALTER TABLE `operational_times`
  MODIFY `id_operational_time` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `order_transactions`
--
ALTER TABLE `order_transactions`
  MODIFY `id_order_transaction` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id_product` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchased_items`
--
ALTER TABLE `purchased_items`
  MODIFY `id_purchased_item` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id_setting` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id_student` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `student_parking_transactions`
--
ALTER TABLE `student_parking_transactions`
  MODIFY `id_student_parking_transaction` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teacher_staffs`
--
ALTER TABLE `teacher_staffs`
  MODIFY `id_teacher_staff` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `teacher_staff_parking_transactions`
--
ALTER TABLE `teacher_staff_parking_transactions`
  MODIFY `id_teacher_staff_parking_transaction` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `top_ups`
--
ALTER TABLE `top_ups`
  MODIFY `id_top_up` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
